﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Windows.Forms;
using com.razorsoftware.wuahelper;
using com.razorsoftware.wuahelper.Services;
using com.razorsoftware.wuahelper.WuaBlocking;
using com.razorsoftware.wuatool.ApplicationSettings;
using ServiceBase = com.razorsoftware.wuahelper.Services.ServiceBase;

namespace com.razorsoftware.wuatool
{
    static class Program
    {
        /// <summary>
        /// Application Entry Point
        /// </summary>
        [STAThread]
        static void Main()
        {
            Settings.Load();

            // Check if Windows Remediation Service (SED) Service is present, and if not, disable SED patching entirely
            if (!SedService.IsPresent)
                Settings.Persistent.DisableSedServiceOnExit = false;

            // Setup Application styles
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Launch Bootstrapping window
            Application.Run(new WndBootstrap());

            // Launch WndMain if possible
            if (Settings.Volatile.MainFormReference != null)
                Application.Run(Settings.Volatile.MainFormReference);

            // Stop & disable WUA service if specified on Persistent settings
            if (Settings.Persistent.DisableWuaServiceOnExit)
            {
                WuaService.SetServiceStartMode(ServiceStartMode.Disabled);

                ServiceBase service = new WuaService();
                if (service.Status != ServiceControllerStatus.Stopped)
                    service.Stop();

                // Stop & disable BITS service if present and specified on Persistent settings (this also requires setting DisableWuaServiceOnExit true)
                if (Settings.Persistent.DisableBitsServiceOnExit && BitsService.IsPresent)
                {
                    BitsService.SetServiceStartMode(ServiceStartMode.Disabled);

                    service = new BitsService();
                    if (service.Status != ServiceControllerStatus.Stopped)
                        service.Stop();
                }
            }

            // Stop & disable SED & USO services if specified on Persistent settings
            if (Settings.Persistent.DisableSedServiceOnExit)
            {
                // SED
                if (SedService.IsPresent)
                {
                    SedService.SetServiceStartMode(ServiceStartMode.Disabled);

                    ServiceBase service = new SedService();
                    if (service.Status != ServiceControllerStatus.Stopped)
                        service.Stop();

                    if (!service.IsPathPatched)
                        service.ApplyPathPatch();
                }
                
                // USO
                if (UsoService.IsPresent)
                {
                    UsoService.SetServiceStartMode(ServiceStartMode.Disabled);

                    ServiceBase service = new UsoService();
                    if (service.Status != ServiceControllerStatus.Stopped)
                        service.Stop();

                    if (!service.IsPathPatched)
                        service.ApplyPathPatch();
                }
            }
            else
            {
                // SED
                if (SedService.IsPresent)
                {
                    ServiceBase service = new SedService();
                    if (service.IsPathPatched)
                        service.RemovePathPatch();

                    if (SedService.GetServiceStartMode() != ServiceStartMode.Automatic)
                        SedService.SetServiceStartMode(ServiceStartMode.Automatic);
                }

                // USO
                if (UsoService.IsPresent)
                {
                    ServiceBase service = new UsoService();
                    if (service.IsPathPatched)
                        service.RemovePathPatch();

                    if (UsoService.GetServiceStartMode() != ServiceStartMode.Automatic)
                        UsoService.SetServiceStartMode(ServiceStartMode.Automatic);
                }
            }
            

            // Apply WUA restrictions via GPO if defined on Persistent settings
            if (Settings.Persistent.EnableWuaServicePolicyRestrictions)
            {
                try
                {
                    PolicyBlocking blocker = new PolicyBlocking();
                    blocker.Block();
                }
                catch (Exception)
                {
                    // Ignore. Probably not a good idea
                }
            }

            // Apply WUA restrictions via Registry if defined on Persistent settings
            if (Settings.Persistent.EnableWuaServiceRegistryRestrictions)
            {
                try
                {
                    RegistryBlocking.Block();
                }
                catch (Exception)
                {
                    // Ignore. Probably not a good idea
                }
            }

            // Set all connections to metered if defined on Persistent settings
            if (Settings.Persistent.SetAllConnectionsToMetered)
            {
                try
                {
                    //wuahelper.WuaBlocking.MeteredBlocking.Block();
                }
                catch (Exception)
                {
                    // Ignore. Probably not a good idea
                }
            }

            // Destroy WU service if set on Volatile settings
            if (Settings.Volatile.DestroyWuaInstallationOnExit)
            {
                wuahelper.WuaBlocking.Unsafe.WuapiRemoval remover = new wuahelper.WuaBlocking.Unsafe.WuapiRemoval();
                remover.RemoveApi();
                remover.InstallPlaceholder();
            }

            Settings.Save();
        }
    }
}
