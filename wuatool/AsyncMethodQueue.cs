﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace com.razorsoftware.wuatool
{
    /// <summary>
    /// Provides a Queue for executing a batch of background methods in a controlled manner, with optional UI Thread callback support
    /// </summary>
    /// <remarks>
    /// Each task is run directly inside each Thread on the Thread Pool synchronously, no additional threads are created to run the tasks. Exceptions are wrapped inside a Try...Catch block and exposed as events.
    /// The active Thread on the ThreadPool is the actual invocator of any events fired by the background Task.
    /// NOTE: Fired callback events for Tasks or any handler attached to <see cref="BackgroundException"/> MUST NOT access the parent <see cref="AsyncMethodWrapper"/> instance or the AsyncMethodQueue itself to prevent thread DeadLocking
    /// </remarks>
    internal class AsyncMethodQueue : IDisposable
    {
        // Fields
        protected readonly Thread[] ThreadPool;
        protected volatile AsyncTaskStatus[] ThreadPoolStatus;
        protected readonly Queue<AsyncTaskSynchronizer<AsyncMethodWrapper>> MethodQueue;

        /// <summary>
        /// Back-end field for <see cref="GlobalCallbackMethod"/> property.
        /// </summary>
        protected volatile Action GlobalCallbackMethodField;
        /// <summary>
        /// Back-end field for <see cref="GlobalInvocationParameters"/> property.
        /// </summary>
        protected volatile object[] GlobalInvocationParametersField;
        /// <summary>
        /// Back-end field for <see cref="GlobalInvocationOwner"/> property.
        /// </summary>
        protected volatile Control GlobalInvocationOwnerField;
        /// <summary>
        /// Indicates if the Background Thread should abort their task loop
        /// </summary>
        protected volatile bool AbortThreads;

        // Constructor
        /// <summary>
        /// Creates a new instance of this class by specifying the amount of working threads that will be used. (Size of ThreadPool)
        /// </summary>
        /// <param name="poolCount">Size of the ThreadPool to use</param>
        public AsyncMethodQueue(int poolCount)
        {
            MethodQueue = new Queue<AsyncTaskSynchronizer<AsyncMethodWrapper>>();
            ThreadPool = new Thread[poolCount];
            ThreadPoolStatus = new AsyncTaskStatus[poolCount];

            for (int i = 0; i < ThreadPool.Length; i++)
            {
                ThreadPool[i] = MakeThreadWithIndex(i);
            }
        }

        // Events
        public event Action<AsyncMethodWrapper, Exception> BackgroundException;

        // Properties
        /// <summary>
        /// Stores the amount of Threads on the ThreadPool used to invoke tasks in parallel
        /// </summary>
        public int PoolSize { get { return ThreadPool.Length; } }
        /// <summary>
        /// Stores the amount of Tasks that are pending for execution
        /// </summary>
        public int Count { get { return MethodQueue.Count; } }

        /// <summary>
        /// Callback Method to invoke when a background task execution ends.
        /// This value will be used when the internal <see cref="AsyncMethodWrapper.CallbackMethod"/> of a Task is null
        /// </summary>
        public Action GlobalCallbackMethod => GlobalCallbackMethodField;

        /// <summary>
        /// Parameters for the invocation of all background delegates.
        /// This value will be used when the internal <see cref="AsyncMethodWrapper.InvocationParameters"/> of a Task is null
        /// </summary>
        public object[] GlobalInvocationParameters => GlobalInvocationParametersField;

        /// <summary>
        /// (Optional) Control object where the callback will be invoked after a background execution ends
        /// This value will be used when the internal <see cref="AsyncMethodWrapper.InvocationOwner"/> of a Task is null
        /// </summary>
        public Control GlobalInvocationOwner => GlobalInvocationOwnerField;

        // Methods
        protected void ThreadLoop(int index)
        {
            while (!AbortThreads)
            {
                ThreadPoolStatus[index] = AsyncTaskStatus.Looping;
                AsyncTaskSynchronizer<AsyncMethodWrapper> task = null;

                lock (MethodQueue)
                {
                    if (MethodQueue.Count > 0)
                        task = MethodQueue.Dequeue();
                }

                if (task == null)
                    break; // No more tasks available

                if (AbortThreads) break; // Additional check for thread abortion

                if (!task.Lock()) continue; // The task has been polled by another thread and it locked the object first

                // Apply global settings to the task
                if (task.Data.CallbackMethod == null)
                    task.Data.CallbackMethod = GlobalCallbackMethodField;
                if (task.Data.InvocationOwner == null)
                    task.Data.InvocationOwner = GlobalInvocationOwnerField;
                if (task.Data.InvocationParameters == null)
                    task.Data.InvocationParameters = GlobalInvocationParametersField;

                ThreadPoolStatus[index] = AsyncTaskStatus.Invoking;
                try
                {
                    task.Data.InternalInvoke(); // Run the task Synchronously on this thread
                }
                catch (Exception ex)
                {
                    ThreadPoolStatus[index] = AsyncTaskStatus.RaisingException;
                    if (AbortThreads) break; // Additional check for thread abortion

                    // Handle background exception
                    if (task.Data.InvocationOwner != null && task.Data.InvocationOwner.InvokeRequired)
                        task.Data.InvocationOwner.Invoke(
                            new Action<AsyncMethodWrapper, Exception>(OnBackgroundException), task.Data, ex);
                    else
                        OnBackgroundException(task.Data, ex);
                }
            }

            ThreadPoolStatus[index] = AsyncTaskStatus.Ended;
        }
        /// <summary>
        /// Returns the Background thread at the specified index, used for invoking Task delegates
        /// </summary>
        public Thread GetBackgroundThreadAt(int index)
        {
            return ThreadPool[index];
        }
        /// <summary>
        /// Waits until all background tasks are completed and the background threads stops looping.
        /// WARNING: Do NOT call this method from the UI Thread if any background task or this queue is configured to fire events on the UI thread. This will result in a massive DeadLock and application hang
        /// </summary>
        public void JoinAll()
        {
            for (int i = 0; i < ThreadPool.Length; i++)
            {
                if (ThreadPool[i].IsAlive)
                    ThreadPool[i].Join();
            }
        }
        /// <summary>
        /// Schedules an specified method for invocation by one of the Threads in the Pool
        /// </summary>
        /// <param name="method">Method that will be scheduled for invocation</param>
        /// <returns>The amount of tasks that are in front of the inserted one in the queue</returns>
        public int Enqueue(AsyncMethodWrapper method)
        {
            AsyncTaskSynchronizer<AsyncMethodWrapper> syncObj = new AsyncTaskSynchronizer<AsyncMethodWrapper>(method);
            int newCount = 0;

            lock (MethodQueue)
            {
                MethodQueue.Enqueue(syncObj);
                newCount = MethodQueue.Count;
            }

            StartThreads();

            return newCount;
        }
        /// <summary>
        /// Dynamically starts the Pool Threads based on the amount of pending tasks on the queue
        /// </summary>
        protected void StartThreads()
        {
            for (int i = 0; i < ThreadPool.Length; i++)
            {
                if (MethodQueue.Count < 1)
                    break;

                if (!ThreadPool[i].IsAlive)
                {
                    if (!ThreadPool[i].ThreadState.HasFlag(ThreadState.Unstarted))
                        ThreadPool[i] = MakeThreadWithIndex(i);
                }
                else
                    continue;
                    

                ThreadPool[i].Start();
            }
        }

        protected virtual void OnBackgroundException(AsyncMethodWrapper arg1, Exception arg2)
        {
            BackgroundException?.Invoke(arg1, arg2);
        }

        protected Thread MakeThreadWithIndex(int index)
        {
            Thread t = new Thread((ThreadStart)delegate { ThreadLoop(index); });
            t.IsBackground = true;
            t.Priority = ThreadPriority.Lowest;
            t.Name = "TreadPool #" + (index + 1);

            return t;
        }

        void IDisposable.Dispose()
        {
            AbortThreads = true;
        }
    }
}
