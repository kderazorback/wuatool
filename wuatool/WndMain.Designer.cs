﻿namespace com.razorsoftware.wuatool
{
    partial class WndMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndMain));
            this.dgvUpdates = new System.Windows.Forms.DataGridView();
            this.colSelected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.colDownload = new System.Windows.Forms.DataGridViewImageColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colKbArticle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelOverlay = new System.Windows.Forms.Panel();
            this.panelProgress = new System.Windows.Forms.Panel();
            this.lblOperationProgressText = new System.Windows.Forms.Label();
            this.pbOperationProgress = new System.Windows.Forms.ProgressBar();
            this.lblOperationDetailText = new System.Windows.Forms.Label();
            this.lblOperationText = new System.Windows.Forms.Label();
            this.picSpinner = new System.Windows.Forms.PictureBox();
            this.panelTaskErrors = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.picTaskErrorsFound = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOperationTitle = new System.Windows.Forms.Label();
            this.toolTips = new System.Windows.Forms.ToolTip(this.components);
            this.panelHeadControls = new System.Windows.Forms.Panel();
            this.cmdOpenSettings = new System.Windows.Forms.LinkLabel();
            this.picOnlineMode = new System.Windows.Forms.PictureBox();
            this.chkSearchOnline = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSystemBuildIdentifier = new System.Windows.Forms.Label();
            this.lblSystemEdition = new System.Windows.Forms.Label();
            this.lblSystemName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chkShowSoftware = new System.Windows.Forms.CheckBox();
            this.chkShowDrivers = new System.Windows.Forms.CheckBox();
            this.chkShowHidden = new System.Windows.Forms.CheckBox();
            this.chkShowNotInstalled = new System.Windows.Forms.CheckBox();
            this.chkShowInstalled = new System.Windows.Forms.CheckBox();
            this.cmdRefreshUpdates = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.mnuAdvancedDownload = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuItemDownloadToDirectory = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemCacheApp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuItemCacheWua = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuItemGenDownloadLinks = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdInstallSelected = new System.Windows.Forms.Button();
            this.cmdDownloadSelected = new System.Windows.Forms.Button();
            this.lblOutdatedCacheText = new System.Windows.Forms.Label();
            this.panelSplitDownloads = new System.Windows.Forms.SplitContainer();
            this.cmdDownloadAdvanced = new System.Windows.Forms.Button();
            this.panelSettings = new System.Windows.Forms.Panel();
            this.lblDisableSedReminder = new System.Windows.Forms.Label();
            this.chkDisableSedOnExit = new System.Windows.Forms.CheckBox();
            this.cmdWuaRegistryBlocked = new System.Windows.Forms.LinkLabel();
            this.lblWuaRegistryBlocked = new System.Windows.Forms.Label();
            this.cmdWuaPolicyUnblock = new System.Windows.Forms.LinkLabel();
            this.lblWuaPolicyEnforced = new System.Windows.Forms.Label();
            this.chkWuaSetConnectionsMetered = new System.Windows.Forms.CheckBox();
            this.chkWuaSetRegistryRestrictions = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkWuaEnforceGpo = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkDisableBitsOnExit = new System.Windows.Forms.CheckBox();
            this.chkDisableWuaOnExit = new System.Windows.Forms.CheckBox();
            this.cmdSettingsRestore = new System.Windows.Forms.Button();
            this.cmdSettingsDiscard = new System.Windows.Forms.Button();
            this.cmdSettingsApply = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdates)).BeginInit();
            this.panelOverlay.SuspendLayout();
            this.panelProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSpinner)).BeginInit();
            this.panelTaskErrors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTaskErrorsFound)).BeginInit();
            this.panelHeadControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picOnlineMode)).BeginInit();
            this.mnuAdvancedDownload.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelSplitDownloads)).BeginInit();
            this.panelSplitDownloads.Panel1.SuspendLayout();
            this.panelSplitDownloads.Panel2.SuspendLayout();
            this.panelSplitDownloads.SuspendLayout();
            this.panelSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvUpdates
            // 
            this.dgvUpdates.AllowUserToAddRows = false;
            this.dgvUpdates.AllowUserToDeleteRows = false;
            this.dgvUpdates.AllowUserToResizeRows = false;
            this.dgvUpdates.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUpdates.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvUpdates.BackgroundColor = System.Drawing.Color.White;
            this.dgvUpdates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUpdates.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSelected,
            this.colIcon,
            this.colDownload,
            this.colName,
            this.colSize,
            this.colId,
            this.colKbArticle});
            this.dgvUpdates.Location = new System.Drawing.Point(17, 124);
            this.dgvUpdates.Name = "dgvUpdates";
            this.dgvUpdates.RowHeadersVisible = false;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(177)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvUpdates.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvUpdates.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUpdates.Size = new System.Drawing.Size(1057, 60);
            this.dgvUpdates.TabIndex = 8;
            this.dgvUpdates.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUpdates_CellClick);
            this.dgvUpdates.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUpdates_CellDoubleClick);
            // 
            // colSelected
            // 
            this.colSelected.Frozen = true;
            this.colSelected.HeaderText = "";
            this.colSelected.MinimumWidth = 30;
            this.colSelected.Name = "colSelected";
            this.colSelected.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colSelected.Width = 30;
            // 
            // colIcon
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colIcon.DefaultCellStyle = dataGridViewCellStyle1;
            this.colIcon.Frozen = true;
            this.colIcon.HeaderText = "";
            this.colIcon.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.colIcon.MinimumWidth = 20;
            this.colIcon.Name = "colIcon";
            this.colIcon.ReadOnly = true;
            this.colIcon.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colIcon.Width = 20;
            // 
            // colDownload
            // 
            this.colDownload.Frozen = true;
            this.colDownload.HeaderText = "";
            this.colDownload.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.colDownload.MinimumWidth = 20;
            this.colDownload.Name = "colDownload";
            this.colDownload.ReadOnly = true;
            this.colDownload.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colDownload.Width = 20;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.HeaderText = "Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            // 
            // colSize
            // 
            this.colSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colSize.HeaderText = "Size";
            this.colSize.Name = "colSize";
            this.colSize.ReadOnly = true;
            this.colSize.Width = 52;
            // 
            // colId
            // 
            this.colId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Width = 42;
            // 
            // colKbArticle
            // 
            this.colKbArticle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colKbArticle.HeaderText = "KB Article";
            this.colKbArticle.Name = "colKbArticle";
            this.colKbArticle.ReadOnly = true;
            this.colKbArticle.Width = 80;
            // 
            // panelOverlay
            // 
            this.panelOverlay.BackColor = System.Drawing.Color.White;
            this.panelOverlay.Controls.Add(this.panelProgress);
            this.panelOverlay.Controls.Add(this.picSpinner);
            this.panelOverlay.Controls.Add(this.panelTaskErrors);
            this.panelOverlay.Controls.Add(this.label3);
            this.panelOverlay.Controls.Add(this.lblOperationTitle);
            this.panelOverlay.Location = new System.Drawing.Point(5, 195);
            this.panelOverlay.Name = "panelOverlay";
            this.panelOverlay.Size = new System.Drawing.Size(487, 358);
            this.panelOverlay.TabIndex = 9;
            this.panelOverlay.Visible = false;
            // 
            // panelProgress
            // 
            this.panelProgress.Controls.Add(this.lblOperationProgressText);
            this.panelProgress.Controls.Add(this.pbOperationProgress);
            this.panelProgress.Controls.Add(this.lblOperationDetailText);
            this.panelProgress.Controls.Add(this.lblOperationText);
            this.panelProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelProgress.Location = new System.Drawing.Point(0, 276);
            this.panelProgress.Name = "panelProgress";
            this.panelProgress.Size = new System.Drawing.Size(487, 82);
            this.panelProgress.TabIndex = 3;
            this.panelProgress.Visible = false;
            // 
            // lblOperationProgressText
            // 
            this.lblOperationProgressText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOperationProgressText.AutoEllipsis = true;
            this.lblOperationProgressText.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperationProgressText.Location = new System.Drawing.Point(324, 0);
            this.lblOperationProgressText.Name = "lblOperationProgressText";
            this.lblOperationProgressText.Size = new System.Drawing.Size(160, 19);
            this.lblOperationProgressText.TabIndex = 3;
            this.lblOperationProgressText.Text = "<counter>";
            this.lblOperationProgressText.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pbOperationProgress
            // 
            this.pbOperationProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbOperationProgress.Location = new System.Drawing.Point(3, 56);
            this.pbOperationProgress.Name = "pbOperationProgress";
            this.pbOperationProgress.Size = new System.Drawing.Size(481, 23);
            this.pbOperationProgress.TabIndex = 2;
            // 
            // lblOperationDetailText
            // 
            this.lblOperationDetailText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOperationDetailText.AutoEllipsis = true;
            this.lblOperationDetailText.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperationDetailText.Location = new System.Drawing.Point(7, 35);
            this.lblOperationDetailText.Name = "lblOperationDetailText";
            this.lblOperationDetailText.Size = new System.Drawing.Size(477, 19);
            this.lblOperationDetailText.TabIndex = 1;
            this.lblOperationDetailText.Text = "<detail>";
            // 
            // lblOperationText
            // 
            this.lblOperationText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOperationText.AutoEllipsis = true;
            this.lblOperationText.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperationText.Location = new System.Drawing.Point(7, 16);
            this.lblOperationText.Name = "lblOperationText";
            this.lblOperationText.Size = new System.Drawing.Size(477, 19);
            this.lblOperationText.TabIndex = 0;
            this.lblOperationText.Text = "<operation>";
            // 
            // picSpinner
            // 
            this.picSpinner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picSpinner.Enabled = false;
            this.picSpinner.Image = global::com.razorsoftware.wuatool.GraphicalResources.Spinner;
            this.picSpinner.Location = new System.Drawing.Point(195, 171);
            this.picSpinner.Name = "picSpinner";
            this.picSpinner.Size = new System.Drawing.Size(96, 96);
            this.picSpinner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSpinner.TabIndex = 2;
            this.picSpinner.TabStop = false;
            // 
            // panelTaskErrors
            // 
            this.panelTaskErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTaskErrors.Controls.Add(this.label4);
            this.panelTaskErrors.Controls.Add(this.picTaskErrorsFound);
            this.panelTaskErrors.Location = new System.Drawing.Point(250, 9);
            this.panelTaskErrors.Name = "panelTaskErrors";
            this.panelTaskErrors.Size = new System.Drawing.Size(234, 34);
            this.panelTaskErrors.TabIndex = 5;
            this.panelTaskErrors.Visible = false;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(3, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 24);
            this.label4.TabIndex = 5;
            this.label4.Text = "Some errors occurred";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // picTaskErrorsFound
            // 
            this.picTaskErrorsFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picTaskErrorsFound.Enabled = false;
            this.picTaskErrorsFound.Image = global::com.razorsoftware.wuatool.GraphicalResources.Alert;
            this.picTaskErrorsFound.Location = new System.Drawing.Point(207, 3);
            this.picTaskErrorsFound.Name = "picTaskErrorsFound";
            this.picTaskErrorsFound.Size = new System.Drawing.Size(24, 24);
            this.picTaskErrorsFound.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picTaskErrorsFound.TabIndex = 4;
            this.picTaskErrorsFound.TabStop = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(481, 26);
            this.label3.TabIndex = 1;
            this.label3.Text = "... Please wait ...";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOperationTitle
            // 
            this.lblOperationTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOperationTitle.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperationTitle.Location = new System.Drawing.Point(3, 60);
            this.lblOperationTitle.Name = "lblOperationTitle";
            this.lblOperationTitle.Size = new System.Drawing.Size(481, 82);
            this.lblOperationTitle.TabIndex = 0;
            this.lblOperationTitle.Text = "<title>";
            this.lblOperationTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // toolTips
            // 
            this.toolTips.IsBalloon = true;
            this.toolTips.ToolTipTitle = "Information";
            // 
            // panelHeadControls
            // 
            this.panelHeadControls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHeadControls.Controls.Add(this.cmdOpenSettings);
            this.panelHeadControls.Controls.Add(this.picOnlineMode);
            this.panelHeadControls.Controls.Add(this.chkSearchOnline);
            this.panelHeadControls.Controls.Add(this.label6);
            this.panelHeadControls.Controls.Add(this.lblSystemBuildIdentifier);
            this.panelHeadControls.Controls.Add(this.lblSystemEdition);
            this.panelHeadControls.Controls.Add(this.lblSystemName);
            this.panelHeadControls.Controls.Add(this.label2);
            this.panelHeadControls.Controls.Add(this.chkShowSoftware);
            this.panelHeadControls.Controls.Add(this.chkShowDrivers);
            this.panelHeadControls.Controls.Add(this.chkShowHidden);
            this.panelHeadControls.Controls.Add(this.chkShowNotInstalled);
            this.panelHeadControls.Controls.Add(this.chkShowInstalled);
            this.panelHeadControls.Controls.Add(this.cmdRefreshUpdates);
            this.panelHeadControls.Controls.Add(this.label1);
            this.panelHeadControls.Location = new System.Drawing.Point(3, 1);
            this.panelHeadControls.Name = "panelHeadControls";
            this.panelHeadControls.Size = new System.Drawing.Size(1084, 117);
            this.panelHeadControls.TabIndex = 10;
            // 
            // cmdOpenSettings
            // 
            this.cmdOpenSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOpenSettings.AutoSize = true;
            this.cmdOpenSettings.Location = new System.Drawing.Point(935, 9);
            this.cmdOpenSettings.Name = "cmdOpenSettings";
            this.cmdOpenSettings.Size = new System.Drawing.Size(73, 13);
            this.cmdOpenSettings.TabIndex = 29;
            this.cmdOpenSettings.TabStop = true;
            this.cmdOpenSettings.Text = "App Settings";
            this.cmdOpenSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.cmdOpenSettings_LinkClicked);
            // 
            // picOnlineMode
            // 
            this.picOnlineMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picOnlineMode.Location = new System.Drawing.Point(1023, 8);
            this.picOnlineMode.Name = "picOnlineMode";
            this.picOnlineMode.Size = new System.Drawing.Size(48, 48);
            this.picOnlineMode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picOnlineMode.TabIndex = 28;
            this.picOnlineMode.TabStop = false;
            this.picOnlineMode.Click += new System.EventHandler(this.picOnlineMode_Click);
            // 
            // chkSearchOnline
            // 
            this.chkSearchOnline.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkSearchOnline.AutoSize = true;
            this.chkSearchOnline.Checked = true;
            this.chkSearchOnline.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSearchOnline.Location = new System.Drawing.Point(1020, 62);
            this.chkSearchOnline.Name = "chkSearchOnline";
            this.chkSearchOnline.Size = new System.Drawing.Size(61, 17);
            this.chkSearchOnline.TabIndex = 27;
            this.chkSearchOnline.Text = "Online";
            this.chkSearchOnline.UseVisualStyleBackColor = true;
            this.chkSearchOnline.CheckedChanged += new System.EventHandler(this.chkSearchOnline_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(494, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 17);
            this.label6.TabIndex = 26;
            this.label6.Text = "Build:";
            // 
            // lblSystemBuildIdentifier
            // 
            this.lblSystemBuildIdentifier.AutoSize = true;
            this.lblSystemBuildIdentifier.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSystemBuildIdentifier.Location = new System.Drawing.Point(539, 78);
            this.lblSystemBuildIdentifier.Name = "lblSystemBuildIdentifier";
            this.lblSystemBuildIdentifier.Size = new System.Drawing.Size(123, 20);
            this.lblSystemBuildIdentifier.TabIndex = 25;
            this.lblSystemBuildIdentifier.Text = "<BuildIdentifier>";
            // 
            // lblSystemEdition
            // 
            this.lblSystemEdition.AutoSize = true;
            this.lblSystemEdition.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSystemEdition.Location = new System.Drawing.Point(486, 58);
            this.lblSystemEdition.Name = "lblSystemEdition";
            this.lblSystemEdition.Size = new System.Drawing.Size(98, 15);
            this.lblSystemEdition.TabIndex = 24;
            this.lblSystemEdition.Text = "<SystemEdition>";
            // 
            // lblSystemName
            // 
            this.lblSystemName.AutoSize = true;
            this.lblSystemName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSystemName.Location = new System.Drawing.Point(461, 37);
            this.lblSystemName.Name = "lblSystemName";
            this.lblSystemName.Size = new System.Drawing.Size(125, 21);
            this.lblSystemName.TabIndex = 23;
            this.lblSystemName.Text = "<SystemName>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(430, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 21);
            this.label2.TabIndex = 22;
            this.label2.Text = "System Information";
            // 
            // chkShowSoftware
            // 
            this.chkShowSoftware.AutoSize = true;
            this.chkShowSoftware.Checked = true;
            this.chkShowSoftware.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowSoftware.Location = new System.Drawing.Point(213, 76);
            this.chkShowSoftware.Name = "chkShowSoftware";
            this.chkShowSoftware.Size = new System.Drawing.Size(191, 17);
            this.chkShowSoftware.TabIndex = 21;
            this.chkShowSoftware.Text = "Show Software and OS Updates";
            this.chkShowSoftware.UseVisualStyleBackColor = true;
            // 
            // chkShowDrivers
            // 
            this.chkShowDrivers.AutoSize = true;
            this.chkShowDrivers.Location = new System.Drawing.Point(64, 76);
            this.chkShowDrivers.Name = "chkShowDrivers";
            this.chkShowDrivers.Size = new System.Drawing.Size(134, 17);
            this.chkShowDrivers.TabIndex = 20;
            this.chkShowDrivers.Text = "Show Driver Updates";
            this.chkShowDrivers.UseVisualStyleBackColor = true;
            // 
            // chkShowHidden
            // 
            this.chkShowHidden.AutoSize = true;
            this.chkShowHidden.Location = new System.Drawing.Point(295, 44);
            this.chkShowHidden.Name = "chkShowHidden";
            this.chkShowHidden.Size = new System.Drawing.Size(96, 17);
            this.chkShowHidden.TabIndex = 19;
            this.chkShowHidden.Text = "Show Hidden";
            this.chkShowHidden.UseVisualStyleBackColor = true;
            // 
            // chkShowNotInstalled
            // 
            this.chkShowNotInstalled.AutoSize = true;
            this.chkShowNotInstalled.Checked = true;
            this.chkShowNotInstalled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowNotInstalled.Location = new System.Drawing.Point(172, 44);
            this.chkShowNotInstalled.Name = "chkShowNotInstalled";
            this.chkShowNotInstalled.Size = new System.Drawing.Size(104, 17);
            this.chkShowNotInstalled.TabIndex = 18;
            this.chkShowNotInstalled.Text = "Show Available";
            this.chkShowNotInstalled.UseVisualStyleBackColor = true;
            // 
            // chkShowInstalled
            // 
            this.chkShowInstalled.AutoSize = true;
            this.chkShowInstalled.Checked = true;
            this.chkShowInstalled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowInstalled.Location = new System.Drawing.Point(42, 44);
            this.chkShowInstalled.Name = "chkShowInstalled";
            this.chkShowInstalled.Size = new System.Drawing.Size(102, 17);
            this.chkShowInstalled.TabIndex = 17;
            this.chkShowInstalled.Text = "Show Installed";
            this.chkShowInstalled.UseVisualStyleBackColor = true;
            // 
            // cmdRefreshUpdates
            // 
            this.cmdRefreshUpdates.AutoSize = true;
            this.cmdRefreshUpdates.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRefreshUpdates.Location = new System.Drawing.Point(168, 9);
            this.cmdRefreshUpdates.Name = "cmdRefreshUpdates";
            this.cmdRefreshUpdates.Size = new System.Drawing.Size(138, 20);
            this.cmdRefreshUpdates.TabIndex = 16;
            this.cmdRefreshUpdates.TabStop = true;
            this.cmdRefreshUpdates.Text = "Refresh updates list";
            this.cmdRefreshUpdates.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.cmdRefreshUpdates_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 21);
            this.label1.TabIndex = 15;
            this.label1.Text = "Windows Updates";
            // 
            // mnuAdvancedDownload
            // 
            this.mnuAdvancedDownload.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuItemDownloadToDirectory,
            this.toolStripSeparator1,
            this.mnuItemCacheApp,
            this.mnuItemCacheWua,
            this.toolStripSeparator2,
            this.mnuItemGenDownloadLinks});
            this.mnuAdvancedDownload.Name = "mnuUpdateRow";
            this.mnuAdvancedDownload.Size = new System.Drawing.Size(274, 104);
            // 
            // mnuItemDownloadToDirectory
            // 
            this.mnuItemDownloadToDirectory.Image = global::com.razorsoftware.wuatool.GraphicalResources.DownloadCustomDirectory;
            this.mnuItemDownloadToDirectory.Name = "mnuItemDownloadToDirectory";
            this.mnuItemDownloadToDirectory.Size = new System.Drawing.Size(273, 22);
            this.mnuItemDownloadToDirectory.Text = "Download to custom directory";
            this.mnuItemDownloadToDirectory.Click += new System.EventHandler(this.mnuItemDownloadToDirectory_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(270, 6);
            // 
            // mnuItemCacheApp
            // 
            this.mnuItemCacheApp.Image = global::com.razorsoftware.wuatool.GraphicalResources.DownloadAppCache;
            this.mnuItemCacheApp.Name = "mnuItemCacheApp";
            this.mnuItemCacheApp.Size = new System.Drawing.Size(273, 22);
            this.mnuItemCacheApp.Text = "Download to Application Cache";
            // 
            // mnuItemCacheWua
            // 
            this.mnuItemCacheWua.Image = global::com.razorsoftware.wuatool.GraphicalResources.Download;
            this.mnuItemCacheWua.Name = "mnuItemCacheWua";
            this.mnuItemCacheWua.Size = new System.Drawing.Size(273, 22);
            this.mnuItemCacheWua.Text = "Download to WUA Cache";
            this.mnuItemCacheWua.Click += new System.EventHandler(this.mnuItemCacheWua_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(270, 6);
            // 
            // mnuItemGenDownloadLinks
            // 
            this.mnuItemGenDownloadLinks.Image = global::com.razorsoftware.wuatool.GraphicalResources.DownloadList;
            this.mnuItemGenDownloadLinks.Name = "mnuItemGenDownloadLinks";
            this.mnuItemGenDownloadLinks.Size = new System.Drawing.Size(273, 22);
            this.mnuItemGenDownloadLinks.Text = "Generate text file with download URLs";
            // 
            // cmdInstallSelected
            // 
            this.cmdInstallSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdInstallSelected.Image = global::com.razorsoftware.wuatool.GraphicalResources.Ok;
            this.cmdInstallSelected.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdInstallSelected.Location = new System.Drawing.Point(17, 570);
            this.cmdInstallSelected.Name = "cmdInstallSelected";
            this.cmdInstallSelected.Size = new System.Drawing.Size(130, 35);
            this.cmdInstallSelected.TabIndex = 12;
            this.cmdInstallSelected.Text = "Install Selected";
            this.cmdInstallSelected.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdInstallSelected.UseVisualStyleBackColor = true;
            this.cmdInstallSelected.Click += new System.EventHandler(this.cmdInstallSelected_Click);
            // 
            // cmdDownloadSelected
            // 
            this.cmdDownloadSelected.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdDownloadSelected.Image = global::com.razorsoftware.wuatool.GraphicalResources.Download;
            this.cmdDownloadSelected.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdDownloadSelected.Location = new System.Drawing.Point(0, 0);
            this.cmdDownloadSelected.Name = "cmdDownloadSelected";
            this.cmdDownloadSelected.Size = new System.Drawing.Size(154, 34);
            this.cmdDownloadSelected.TabIndex = 13;
            this.cmdDownloadSelected.Text = "Download Selected";
            this.cmdDownloadSelected.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdDownloadSelected.UseVisualStyleBackColor = true;
            this.cmdDownloadSelected.Click += new System.EventHandler(this.cmdDownloadSelected_Click);
            // 
            // lblOutdatedCacheText
            // 
            this.lblOutdatedCacheText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOutdatedCacheText.AutoSize = true;
            this.lblOutdatedCacheText.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutdatedCacheText.ForeColor = System.Drawing.Color.DarkRed;
            this.lblOutdatedCacheText.Location = new System.Drawing.Point(625, 591);
            this.lblOutdatedCacheText.Name = "lblOutdatedCacheText";
            this.lblOutdatedCacheText.Size = new System.Drawing.Size(449, 17);
            this.lblOutdatedCacheText.TabIndex = 14;
            this.lblOutdatedCacheText.Text = "Package list is outdated. Click \"Refresh\" to get the latest Package state.";
            this.lblOutdatedCacheText.Visible = false;
            // 
            // panelSplitDownloads
            // 
            this.panelSplitDownloads.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelSplitDownloads.Location = new System.Drawing.Point(153, 570);
            this.panelSplitDownloads.Name = "panelSplitDownloads";
            // 
            // panelSplitDownloads.Panel1
            // 
            this.panelSplitDownloads.Panel1.Controls.Add(this.cmdDownloadSelected);
            // 
            // panelSplitDownloads.Panel2
            // 
            this.panelSplitDownloads.Panel2.Controls.Add(this.cmdDownloadAdvanced);
            this.panelSplitDownloads.Panel2MinSize = 20;
            this.panelSplitDownloads.Size = new System.Drawing.Size(183, 34);
            this.panelSplitDownloads.SplitterDistance = 154;
            this.panelSplitDownloads.SplitterWidth = 1;
            this.panelSplitDownloads.TabIndex = 15;
            // 
            // cmdDownloadAdvanced
            // 
            this.cmdDownloadAdvanced.ContextMenuStrip = this.mnuAdvancedDownload;
            this.cmdDownloadAdvanced.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdDownloadAdvanced.Enabled = false;
            this.cmdDownloadAdvanced.Image = global::com.razorsoftware.wuatool.GraphicalResources.DropdownWidget;
            this.cmdDownloadAdvanced.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdDownloadAdvanced.Location = new System.Drawing.Point(0, 0);
            this.cmdDownloadAdvanced.Name = "cmdDownloadAdvanced";
            this.cmdDownloadAdvanced.Size = new System.Drawing.Size(28, 34);
            this.cmdDownloadAdvanced.TabIndex = 0;
            this.cmdDownloadAdvanced.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdDownloadAdvanced.UseVisualStyleBackColor = true;
            this.cmdDownloadAdvanced.Click += new System.EventHandler(this.cmdDownloadAdvanced_Click);
            // 
            // panelSettings
            // 
            this.panelSettings.Controls.Add(this.lblDisableSedReminder);
            this.panelSettings.Controls.Add(this.chkDisableSedOnExit);
            this.panelSettings.Controls.Add(this.cmdWuaRegistryBlocked);
            this.panelSettings.Controls.Add(this.lblWuaRegistryBlocked);
            this.panelSettings.Controls.Add(this.cmdWuaPolicyUnblock);
            this.panelSettings.Controls.Add(this.lblWuaPolicyEnforced);
            this.panelSettings.Controls.Add(this.chkWuaSetConnectionsMetered);
            this.panelSettings.Controls.Add(this.chkWuaSetRegistryRestrictions);
            this.panelSettings.Controls.Add(this.label9);
            this.panelSettings.Controls.Add(this.chkWuaEnforceGpo);
            this.panelSettings.Controls.Add(this.label7);
            this.panelSettings.Controls.Add(this.chkDisableBitsOnExit);
            this.panelSettings.Controls.Add(this.chkDisableWuaOnExit);
            this.panelSettings.Controls.Add(this.cmdSettingsRestore);
            this.panelSettings.Controls.Add(this.cmdSettingsDiscard);
            this.panelSettings.Controls.Add(this.cmdSettingsApply);
            this.panelSettings.Controls.Add(this.label5);
            this.panelSettings.Location = new System.Drawing.Point(388, 190);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(692, 358);
            this.panelSettings.TabIndex = 16;
            this.panelSettings.Visible = false;
            // 
            // lblDisableSedReminder
            // 
            this.lblDisableSedReminder.AutoSize = true;
            this.lblDisableSedReminder.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisableSedReminder.ForeColor = System.Drawing.Color.Gray;
            this.lblDisableSedReminder.Location = new System.Drawing.Point(101, 150);
            this.lblDisableSedReminder.Name = "lblDisableSedReminder";
            this.lblDisableSedReminder.Size = new System.Drawing.Size(458, 13);
            this.lblDisableSedReminder.TabIndex = 34;
            this.lblDisableSedReminder.Text = "Unticking this option will revert the modifications applied to the Service when t" +
    "he tool is Closed";
            this.lblDisableSedReminder.Visible = false;
            // 
            // chkDisableSedOnExit
            // 
            this.chkDisableSedOnExit.AutoSize = true;
            this.chkDisableSedOnExit.Location = new System.Drawing.Point(68, 130);
            this.chkDisableSedOnExit.Name = "chkDisableSedOnExit";
            this.chkDisableSedOnExit.Size = new System.Drawing.Size(465, 17);
            this.chkDisableSedOnExit.TabIndex = 33;
            this.chkDisableSedOnExit.Text = "On Exit, Disable and Prevent Windows Remediation Service (SED) from Running again" +
    "";
            this.chkDisableSedOnExit.UseVisualStyleBackColor = true;
            this.chkDisableSedOnExit.Visible = false;
            this.chkDisableSedOnExit.CheckedChanged += new System.EventHandler(this.chkDisableSedOnExit_CheckedChanged);
            // 
            // cmdWuaRegistryBlocked
            // 
            this.cmdWuaRegistryBlocked.AutoSize = true;
            this.cmdWuaRegistryBlocked.Location = new System.Drawing.Point(377, 250);
            this.cmdWuaRegistryBlocked.Name = "cmdWuaRegistryBlocked";
            this.cmdWuaRegistryBlocked.Size = new System.Drawing.Size(50, 13);
            this.cmdWuaRegistryBlocked.TabIndex = 32;
            this.cmdWuaRegistryBlocked.TabStop = true;
            this.cmdWuaRegistryBlocked.Text = "Unblock";
            this.cmdWuaRegistryBlocked.Visible = false;
            this.cmdWuaRegistryBlocked.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.cmdWuaRegistryBlocked_LinkClicked);
            // 
            // lblWuaRegistryBlocked
            // 
            this.lblWuaRegistryBlocked.AutoSize = true;
            this.lblWuaRegistryBlocked.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWuaRegistryBlocked.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblWuaRegistryBlocked.Location = new System.Drawing.Point(281, 251);
            this.lblWuaRegistryBlocked.Name = "lblWuaRegistryBlocked";
            this.lblWuaRegistryBlocked.Size = new System.Drawing.Size(90, 13);
            this.lblWuaRegistryBlocked.TabIndex = 31;
            this.lblWuaRegistryBlocked.Text = "Currently blocked";
            this.lblWuaRegistryBlocked.Visible = false;
            // 
            // cmdWuaPolicyUnblock
            // 
            this.cmdWuaPolicyUnblock.AutoSize = true;
            this.cmdWuaPolicyUnblock.Location = new System.Drawing.Point(377, 228);
            this.cmdWuaPolicyUnblock.Name = "cmdWuaPolicyUnblock";
            this.cmdWuaPolicyUnblock.Size = new System.Drawing.Size(50, 13);
            this.cmdWuaPolicyUnblock.TabIndex = 30;
            this.cmdWuaPolicyUnblock.TabStop = true;
            this.cmdWuaPolicyUnblock.Text = "Unblock";
            this.cmdWuaPolicyUnblock.Visible = false;
            this.cmdWuaPolicyUnblock.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.cmdWuaPolicyUnblock_LinkClicked);
            // 
            // lblWuaPolicyEnforced
            // 
            this.lblWuaPolicyEnforced.AutoSize = true;
            this.lblWuaPolicyEnforced.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWuaPolicyEnforced.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblWuaPolicyEnforced.Location = new System.Drawing.Point(281, 229);
            this.lblWuaPolicyEnforced.Name = "lblWuaPolicyEnforced";
            this.lblWuaPolicyEnforced.Size = new System.Drawing.Size(90, 13);
            this.lblWuaPolicyEnforced.TabIndex = 12;
            this.lblWuaPolicyEnforced.Text = "Currently blocked";
            this.lblWuaPolicyEnforced.Visible = false;
            // 
            // chkWuaSetConnectionsMetered
            // 
            this.chkWuaSetConnectionsMetered.AutoSize = true;
            this.chkWuaSetConnectionsMetered.Location = new System.Drawing.Point(70, 274);
            this.chkWuaSetConnectionsMetered.Name = "chkWuaSetConnectionsMetered";
            this.chkWuaSetConnectionsMetered.Size = new System.Drawing.Size(191, 17);
            this.chkWuaSetConnectionsMetered.TabIndex = 11;
            this.chkWuaSetConnectionsMetered.Text = "Set all connections to \"Metered\"";
            this.chkWuaSetConnectionsMetered.UseVisualStyleBackColor = true;
            this.chkWuaSetConnectionsMetered.CheckedChanged += new System.EventHandler(this.chkWuaSetConnectionsMetered_CheckedChanged);
            // 
            // chkWuaSetRegistryRestrictions
            // 
            this.chkWuaSetRegistryRestrictions.AutoSize = true;
            this.chkWuaSetRegistryRestrictions.Location = new System.Drawing.Point(70, 251);
            this.chkWuaSetRegistryRestrictions.Name = "chkWuaSetRegistryRestrictions";
            this.chkWuaSetRegistryRestrictions.Size = new System.Drawing.Size(159, 17);
            this.chkWuaSetRegistryRestrictions.TabIndex = 10;
            this.chkWuaSetRegistryRestrictions.Text = "Apply Registry restrictions";
            this.chkWuaSetRegistryRestrictions.UseVisualStyleBackColor = true;
            this.chkWuaSetRegistryRestrictions.CheckedChanged += new System.EventHandler(this.chkWuaSetRegistryRestrictions_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label9.Location = new System.Drawing.Point(46, 187);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(177, 17);
            this.label9.TabIndex = 9;
            this.label9.Text = "Automatic Update Blocking";
            // 
            // chkWuaEnforceGpo
            // 
            this.chkWuaEnforceGpo.AutoSize = true;
            this.chkWuaEnforceGpo.Location = new System.Drawing.Point(70, 228);
            this.chkWuaEnforceGpo.Name = "chkWuaEnforceGpo";
            this.chkWuaEnforceGpo.Size = new System.Drawing.Size(193, 17);
            this.chkWuaEnforceGpo.TabIndex = 8;
            this.chkWuaEnforceGpo.Text = "Enforce Group Policy restrictions";
            this.chkWuaEnforceGpo.UseVisualStyleBackColor = true;
            this.chkWuaEnforceGpo.CheckedChanged += new System.EventHandler(this.chkWuaEnforceGpo_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label7.Location = new System.Drawing.Point(46, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Services";
            // 
            // chkDisableBitsOnExit
            // 
            this.chkDisableBitsOnExit.AutoSize = true;
            this.chkDisableBitsOnExit.Location = new System.Drawing.Point(68, 107);
            this.chkDisableBitsOnExit.Name = "chkDisableBitsOnExit";
            this.chkDisableBitsOnExit.Size = new System.Drawing.Size(342, 17);
            this.chkDisableBitsOnExit.TabIndex = 5;
            this.chkDisableBitsOnExit.Text = "Disable Background \"Intelligent\" Transfer service (BITS) on Exit";
            this.chkDisableBitsOnExit.UseVisualStyleBackColor = true;
            this.chkDisableBitsOnExit.CheckedChanged += new System.EventHandler(this.chkDisableBitsOnExit_CheckedChanged);
            // 
            // chkDisableWuaOnExit
            // 
            this.chkDisableWuaOnExit.AutoSize = true;
            this.chkDisableWuaOnExit.Location = new System.Drawing.Point(68, 84);
            this.chkDisableWuaOnExit.Name = "chkDisableWuaOnExit";
            this.chkDisableWuaOnExit.Size = new System.Drawing.Size(232, 17);
            this.chkDisableWuaOnExit.TabIndex = 4;
            this.chkDisableWuaOnExit.Text = "Disable Windows Update service on Exit";
            this.chkDisableWuaOnExit.UseVisualStyleBackColor = true;
            this.chkDisableWuaOnExit.CheckedChanged += new System.EventHandler(this.chkDisableWuaOnExit_CheckedChanged);
            // 
            // cmdSettingsRestore
            // 
            this.cmdSettingsRestore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdSettingsRestore.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdSettingsRestore.Location = new System.Drawing.Point(20, 311);
            this.cmdSettingsRestore.Name = "cmdSettingsRestore";
            this.cmdSettingsRestore.Size = new System.Drawing.Size(165, 35);
            this.cmdSettingsRestore.TabIndex = 3;
            this.cmdSettingsRestore.Text = "Restore Defaults";
            this.cmdSettingsRestore.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.cmdSettingsRestore.UseVisualStyleBackColor = true;
            this.cmdSettingsRestore.Click += new System.EventHandler(this.cmdSettingsRestore_Click);
            // 
            // cmdSettingsDiscard
            // 
            this.cmdSettingsDiscard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSettingsDiscard.Image = global::com.razorsoftware.wuatool.GraphicalResources.Alert;
            this.cmdSettingsDiscard.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdSettingsDiscard.Location = new System.Drawing.Point(434, 311);
            this.cmdSettingsDiscard.Name = "cmdSettingsDiscard";
            this.cmdSettingsDiscard.Size = new System.Drawing.Size(138, 35);
            this.cmdSettingsDiscard.TabIndex = 2;
            this.cmdSettingsDiscard.Text = "Discard Changes";
            this.cmdSettingsDiscard.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.cmdSettingsDiscard.UseVisualStyleBackColor = true;
            this.cmdSettingsDiscard.Click += new System.EventHandler(this.cmdSettingsDiscard_Click);
            // 
            // cmdSettingsApply
            // 
            this.cmdSettingsApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSettingsApply.Image = global::com.razorsoftware.wuatool.GraphicalResources.Ok;
            this.cmdSettingsApply.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdSettingsApply.Location = new System.Drawing.Point(578, 311);
            this.cmdSettingsApply.Name = "cmdSettingsApply";
            this.cmdSettingsApply.Size = new System.Drawing.Size(99, 35);
            this.cmdSettingsApply.TabIndex = 1;
            this.cmdSettingsApply.Text = "Save";
            this.cmdSettingsApply.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.cmdSettingsApply.UseVisualStyleBackColor = true;
            this.cmdSettingsApply.Click += new System.EventHandler(this.cmdSettingsApply_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label5.Location = new System.Drawing.Point(20, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(165, 21);
            this.label5.TabIndex = 0;
            this.label5.Text = "Application Settings";
            // 
            // WndMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1086, 617);
            this.Controls.Add(this.panelSettings);
            this.Controls.Add(this.panelOverlay);
            this.Controls.Add(this.panelSplitDownloads);
            this.Controls.Add(this.lblOutdatedCacheText);
            this.Controls.Add(this.cmdInstallSelected);
            this.Controls.Add(this.panelHeadControls);
            this.Controls.Add(this.dgvUpdates);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(790, 600);
            this.Name = "WndMain";
            this.Tag = "";
            this.Text = "WUA Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WndMain_FormClosing);
            this.Load += new System.EventHandler(this.WndMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdates)).EndInit();
            this.panelOverlay.ResumeLayout(false);
            this.panelProgress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picSpinner)).EndInit();
            this.panelTaskErrors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picTaskErrorsFound)).EndInit();
            this.panelHeadControls.ResumeLayout(false);
            this.panelHeadControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picOnlineMode)).EndInit();
            this.mnuAdvancedDownload.ResumeLayout(false);
            this.panelSplitDownloads.Panel1.ResumeLayout(false);
            this.panelSplitDownloads.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelSplitDownloads)).EndInit();
            this.panelSplitDownloads.ResumeLayout(false);
            this.panelSettings.ResumeLayout(false);
            this.panelSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvUpdates;
        private System.Windows.Forms.Panel panelOverlay;
        private System.Windows.Forms.PictureBox picSpinner;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblOperationTitle;
        private System.Windows.Forms.ToolTip toolTips;
        private System.Windows.Forms.Panel panelHeadControls;
        private System.Windows.Forms.CheckBox chkShowSoftware;
        private System.Windows.Forms.CheckBox chkShowDrivers;
        private System.Windows.Forms.CheckBox chkShowHidden;
        private System.Windows.Forms.CheckBox chkShowNotInstalled;
        private System.Windows.Forms.CheckBox chkShowInstalled;
        private System.Windows.Forms.LinkLabel cmdRefreshUpdates;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colSelected;
        private System.Windows.Forms.DataGridViewImageColumn colIcon;
        private System.Windows.Forms.DataGridViewImageColumn colDownload;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colKbArticle;
        private System.Windows.Forms.ContextMenuStrip mnuAdvancedDownload;
        private System.Windows.Forms.ToolStripMenuItem mnuItemDownloadToDirectory;
        private System.Windows.Forms.Button cmdInstallSelected;
        private System.Windows.Forms.Button cmdDownloadSelected;
        private System.Windows.Forms.Panel panelProgress;
        private System.Windows.Forms.ProgressBar pbOperationProgress;
        private System.Windows.Forms.Label lblOperationDetailText;
        private System.Windows.Forms.Label lblOperationText;
        private System.Windows.Forms.Label lblOperationProgressText;
        private System.Windows.Forms.Label lblOutdatedCacheText;
        private System.Windows.Forms.Label lblSystemEdition;
        private System.Windows.Forms.Label lblSystemName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblSystemBuildIdentifier;
        private System.Windows.Forms.CheckBox chkSearchOnline;
        private System.Windows.Forms.PictureBox picOnlineMode;
        private System.Windows.Forms.SplitContainer panelSplitDownloads;
        private System.Windows.Forms.Button cmdDownloadAdvanced;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCacheApp;
        private System.Windows.Forms.ToolStripMenuItem mnuItemCacheWua;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuItemGenDownloadLinks;
        private System.Windows.Forms.PictureBox picTaskErrorsFound;
        private System.Windows.Forms.Panel panelTaskErrors;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelSettings;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button cmdSettingsRestore;
        private System.Windows.Forms.Button cmdSettingsDiscard;
        private System.Windows.Forms.Button cmdSettingsApply;
        private System.Windows.Forms.CheckBox chkDisableWuaOnExit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkDisableBitsOnExit;
        private System.Windows.Forms.CheckBox chkWuaSetRegistryRestrictions;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkWuaEnforceGpo;
        private System.Windows.Forms.CheckBox chkWuaSetConnectionsMetered;
        private System.Windows.Forms.LinkLabel cmdOpenSettings;
        private System.Windows.Forms.LinkLabel cmdWuaPolicyUnblock;
        private System.Windows.Forms.Label lblWuaPolicyEnforced;
        private System.Windows.Forms.LinkLabel cmdWuaRegistryBlocked;
        private System.Windows.Forms.Label lblWuaRegistryBlocked;
        private System.Windows.Forms.CheckBox chkDisableSedOnExit;
        private System.Windows.Forms.Label lblDisableSedReminder;
    }
}

