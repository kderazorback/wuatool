﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuatool
{
    /// <summary>
    /// Defines various status for a Background Task Thread inside <see cref="AsyncMethodQueue"/>
    /// </summary>
    internal enum AsyncTaskStatus : byte
    {
        /// <summary>
        /// The background thread status is Unknown or it has not been started yet
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// The background thread is currently looping through the Task Queue
        /// </summary>
        Looping,
        /// <summary>
        /// The background thread is currently Invoking its payload
        /// </summary>
        Invoking,
        /// <summary>
        /// The background thread has detected an unhandled exception on its payload and is handling it
        /// </summary>
        RaisingException,
        /// <summary>
        /// The background thread has ended its task loop and is not running anymore
        /// </summary>
        Ended,
    }
}
