﻿
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace com.razorsoftware.wuatool.ApplicationSettings
{
    /// <summary>
    /// Stores Application settings (both volatile and persistent) and manages configuration files
    /// </summary>
    internal static class Settings
    {
        // Static settings
        public static string SettingsFilename = ".\\settings.xml";

        // Fields
        private static PersistentSet _backingFieldPersistentSet;
        private static VolatileSet _backingFieldVolatileSet;
        private static PersistentSet _backingFieldDefaultSet;


        // Properties
        /// <summary>
        /// Returns an object used to store Persistent settings that are saved to the configuration file on disk
        /// </summary>
        public static PersistentSet Persistent
        {
            get
            {
                if (_backingFieldPersistentSet == null)
                    _backingFieldPersistentSet = new PersistentSet(); // Load default settings if a custom PersistentSet is not available
                
                return _backingFieldPersistentSet;   
            }
            set { _backingFieldPersistentSet = value; }
        }
        /// <summary>
        /// Returns an object used to store Default Persistent settings for the Application
        /// </summary>
        public static PersistentSet Defaults
        {
            get
            {
                if (_backingFieldDefaultSet == null)
                    _backingFieldDefaultSet = new PersistentSet(); // Always Load Default settings

                return _backingFieldDefaultSet;
            }
        }

        /// <summary>
        /// Stores volatile (runtime only) variables and settings for the current application.
        /// </summary>
        public static VolatileSet Volatile
        {
            get
            {
                if (_backingFieldVolatileSet == null)
                    _backingFieldVolatileSet = new VolatileSet();

                return _backingFieldVolatileSet;
            }
        }


        // Static methods
        public static void Load()
        {
            try
            {
                _backingFieldPersistentSet = PersistentSet.Deserialize(SettingsFilename);
            }
            catch (Exception)
            {
                _backingFieldPersistentSet = new PersistentSet();
            }
        }

        public static void Save()
        {
            try
            {
                PersistentSet.Serialize(SettingsFilename, Persistent);
            }
            catch (Exception)
            {
                // Ignore
            }
        }
    }
}
