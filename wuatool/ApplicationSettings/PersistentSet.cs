﻿using System.IO;
using System.Linq;
using System.Runtime.Serialization;

namespace com.razorsoftware.wuatool.ApplicationSettings
{
    /// <summary>
    /// Defines a set of settings that can be saved and loaded to/from disk using XML Serialization
    /// </summary>
    public class PersistentSet
    {
        // Back-end Fields
        private string _backendDumpUpdateXmlTreesDirectory = "update_xml";
        private int _backendNetworkThreadPoolSize = 6;
        private bool _backendDangerousRedirectWuaServiceEntryPointOnExit = false;


        // Properties
        /// <summary>
        /// Specifies if the Windows Update Agent service will be stopped and disabled when the application quits
        /// </summary>
        [DataMember(Name="DisableWuaSvcOnExit")]
        public bool DisableWuaServiceOnExit { get; set; } = true;
        /// <summary>
        /// Specifies if the Background Intelligent Transfer Service (BITS) will be stopped and disabled when the application quits
        /// </summary>
        [DataMember(Name = "DisableBitsSvcOnExit")]
        public bool DisableBitsServiceOnExit { get; set; } = true;
        /// <summary>
        /// Specified if the Windows Remediation Service (SED) will be stopped and disabled on Exit
        /// </summary>
        public bool DisableSedServiceOnExit { get; set; } = true;

        /// <summary>
        /// Defines the amount of Threads that will be used to execute Network operations in the Background. The Value is clamped between 1 and 10.
        /// </summary>
        [DataMember(Name = "Http_ThreadPoolSize")]
        public int NetworkThreadPoolSize
        {
            get { return _backendNetworkThreadPoolSize; }
            set
            {
                if (value < 1) value = 1;
                if (value > 10) value = 10;

                _backendNetworkThreadPoolSize = value;
            }
        }
        /// <summary>
        /// Defines if the application should dump each queried update XML information to disk. This is intended for debugging purposes only
        /// </summary>
        [DataMember(Name="DumpUpdateInfoXml")]
        public bool DumpUpdateXmlTrees { get; set; } = true;
        /// <summary>
        /// Stores the directory (relative to the Application Working Path) where each Update XML information will be dumped on disk, when the <see cref="DumpUpdateXmlTrees"/> option is set
        /// </summary>
        /// <remarks>This option cant store path separators. The path is sanitized on the Get method and if any Path separator is found, the default path will be used.</remarks>
        [DataMember(Name="DumpUpdateInfoXmlDirectory")]
        public string DumpUpdateXmlTreesDirectory
        {
            get
            {
                return _backendDumpUpdateXmlTreesDirectory;
            }
            set
            {
                char[] separators = Path.GetInvalidFileNameChars();
                for (int i = 0; i < separators.Length; i++)
                {
                    if (value.Contains(separators[i]))
                        return; // Don't throw exception because it can break the XMlSerializer when loading settings. Just return instead and use the previous value
                }

                _backendDumpUpdateXmlTreesDirectory = value;
            }
        }

        /// <summary>
        /// Stores a value indicating if the application will enforce Group Policy restrictions on the WUA Service preventing it from downloading updates automatically
        /// </summary>
        [DataMember(Name="EnforceWuaSvcPolicyRestrictions")]
        public bool EnableWuaServicePolicyRestrictions { get; set; } = false;

        /// <summary>
        /// Stores a value indicating if the application will set registry restrictions for the WUA Service, preventing it from downloading updates automatically
        /// </summary>
        [DataMember(Name = "EnforceWuaRegistryRestrictions")]
        public bool EnableWuaServiceRegistryRestrictions { get; set; } = false;

        /// <summary>
        /// Stores a value indicating if all connection types should be set to METERED for preventing the WUA and BITS services from dowloading content automatically
        /// </summary>
        [DataMember(Name = "SetConnectionsToMetered")]
        public bool SetAllConnectionsToMetered { get; set; } = false;

        /// <summary>
        /// (Dangerous) Defines if the Windows Update Agent (WUA) Service entry point dll should be removed from the Windows Registry Hive on exit.
        /// This prevents the Windows Service Host (svchost) process from locating and launching the WUA service even when the service itself has been
        /// re-enabled on the Service Database. This is NOT RECOMMENDED.
        /// </summary>
        public bool DangerousRedirectWuaServiceEntryPointOnExit { get { return false; } }


        // Static Serializer/Deserializer
        public static void Serialize(string filename, PersistentSet obj)
        {
            DataContractSerializer ser = new DataContractSerializer(typeof(PersistentSet));

            FileInfo fi = new FileInfo(filename);
            if (fi.Directory != null && !fi.Directory.Exists)
                fi.Directory.Create();

            using (FileStream fs = new FileStream(fi.FullName, FileMode.Create, FileAccess.Write, FileShare.None))
                ser.WriteObject(fs, obj);
        }

        public static PersistentSet Deserialize(string filename)
        {
            DataContractSerializer ser = new DataContractSerializer(typeof(PersistentSet));
            FileInfo fi = new FileInfo(filename);

            if (!fi.Exists)
                throw new FileNotFoundException(fi.FullName);

            PersistentSet obj;

            using (FileStream fs = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read, FileShare.Read))
                obj = (PersistentSet)ser.ReadObject(fs);

            return obj;
        }
    }
}