﻿using com.razorsoftware.wuahelper;
using com.razorsoftware.wuahelper.Downloads;
using com.razorsoftware.wuahelper.WuaCom;

namespace com.razorsoftware.wuatool.ApplicationSettings
{
    /// <summary>
    /// Defines a set of settings that are kept in memory while the application is running.
    /// This class depends on settings stored on the active <see cref="PersistentSet"/> instance.
    /// </summary>
    internal class VolatileSet
    {
        // Fields
        private WuaTask _backendFieldWuaTask;
        private DownloadList _backendFieldDownloadList;
        /// <summary>
        /// Stores a list of updates packages returned by the last search operation submitted to the Windows Update Agent (WUA)
        /// </summary>
        public WuaUpdateWrapper[] Updates;
        /// <summary>
        /// Queue used for executing Network tasks in a background ThreadPool
        /// </summary>
        public AsyncMethodQueue NetworkTaskQueue = new AsyncMethodQueue(Settings.Persistent.NetworkThreadPoolSize);

        // Properties
        /// <summary>
        /// Stores a reference to the active <see cref="WndMain"/> window
        /// </summary>
        public WndMain MainFormReference { get; set; }

        /// <summary>
        /// Stores the background <see cref="WuaTask"/> used by the application as a connection point to the Windows Update Agent (WUA)
        /// </summary>
        public WuaTask BackgroundTask
        {
            get
            {
                if (_backendFieldWuaTask == null)
                    _backendFieldWuaTask = WuaTask.CreateForUpdates();

                return _backendFieldWuaTask;
            }
        }

        /// <summary>
        /// Stores the object that will be used to download updates locally
        /// </summary>
        public DownloadList BackgroundDownloadList
        {
            get
            {
                if (_backendFieldDownloadList == null)
                    _backendFieldDownloadList = new DownloadList();

                return _backendFieldDownloadList;
            }
            set { _backendFieldDownloadList = value; }
        }


        /// <summary>
        /// Indicates if <see cref="WuaTask"/> is searching for update packages
        /// </summary>
        public bool IsSearching { get; set; }

        /// <summary>
        /// Indicates if <see cref="WuaTask"/> is downloading packages
        /// </summary>
        public bool IsDownloading { get; set; }

        /// <summary>
        /// Indicates if the <see cref="WuaTask"/> is installing update packages
        /// </summary>
        public bool IsInstalling { get; set; }

        /// <summary>
        /// Returns a value indicating if the las Update information is outdated.
        /// </summary>
        public bool UpdateInfoObsolete { get; set; }

        /// <summary>
        /// Returns a value indicating if the app <see cref="WuaTask"/> is currently active
        /// </summary>
        public bool IsActive
        {
            get { return IsSearching || IsDownloading || IsInstalling; }
        }

        public bool DestroyWuaInstallationOnExit { get; set; } = false;
    }
}
