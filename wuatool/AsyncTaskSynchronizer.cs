﻿using System.Text;
using System.Threading;

namespace com.razorsoftware.wuatool
{
    /// <summary>
    /// Provides a custom-made Thread Synchronization primitive around <see cref="AsyncMethodWrapper"/> objects
    /// </summary>
    /// <typeparam name="T">Data type for the contained object</typeparam>
    internal sealed class AsyncTaskSynchronizer<T> where T: AsyncMethodWrapper
    {
        // Fields
        private volatile Thread _ownerThread;
        private readonly T _data;

        // Constructor
        /// <summary>
        /// Creates a new instance of this class by specifying its contained data
        /// </summary>
        /// <param name="data">Data that will be stored for Synchronized access</param>
        public AsyncTaskSynchronizer (T data)
        {
            _data = data;
        }

        // Properties
        /// <summary>
        /// Returns a value indicating if this Task is bound to the calling thread
        /// </summary>
        public bool IsCallingBound
        {
            get { return _ownerThread == Thread.CurrentThread; }
        }
        /// <summary>
        /// Indicates if the contained object is currently bound
        /// </summary>
        public bool IsBound { get { return _ownerThread != null && _ownerThread.IsAlive; } }
        /// <summary>
        /// Returns the wrapped object
        /// </summary>
        public T Data { get { return _data; } }

        // Methods
        /// <summary>
        /// Tries to locks the background object to the calling Thread, returning a value indicating if the lock operation succeeded.
        /// </summary>
        /// <returns>True if the lock operation succeeded and the Task is bound to the calling thread, otherwise false</returns>
        public bool Lock()
        {
            if (_ownerThread != null)
            {
                if (_ownerThread.IsAlive)
                    return false;
            }

            _ownerThread = Thread.CurrentThread;

            return (_ownerThread == Thread.CurrentThread);
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();

            str.Append((IsBound ? "Locked->" + _ownerThread.ManagedThreadId : "Free"));
            str.Append(" { ");
            str.Append(_data);
            str.Append(" }");

            return str.ToString();
        }
    }
}
