﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.razorsoftware.wuahelper;
using com.razorsoftware.wuahelper.Services;
using com.razorsoftware.wuahelper.WuaCom;

namespace com.razorsoftware.wuatool
{
    internal partial class WndBootstrap : Form
    {
        // Private fields
        private string _serviceOperationTimeoutMessage = 
@"The Windows Update Agent service (WUA) could not be started.
The Application isn't having administrator rights, or the WUA service itself is damaged.

Do you want to retry enabling and starting the service?";

        // Public fields
        /// <summary>
        /// Specifies if this window can be closed
        /// </summary>
        public bool CanClose = false;
        /// <summary>
        /// Stores a Background Service Manager that can be used to Start/Stop the service
        /// </summary>
        public AsyncMethodWrapper<ServiceControllerStatus> BackgroundServiceManager;

        public WndBootstrap()
        {
            InitializeComponent();
        }

        private void WndBootstrap_Load(object sender, EventArgs e)
        {
            picIcon.Image = GraphicalResources.Logo;

            lblAppVersion.Text = "Version: " + Application.ProductVersion.ToString()
#if DEBUG 
                + @"-DEBUG" 
#endif
                ;

            SystemInfo.OsDetails osInfo = SystemInfo.GetSystemInfo();

            lblWinVersion.Text = osInfo.ToString();

            BackgroundServiceManager = AsyncMethodWrapper<ServiceControllerStatus>.WrapAround(new Func<ServiceControllerStatus>(StartWuaService), WuaServiceStartComplete, this);

            // Check WUA service
            ServiceStartMode startMode = WuaService.GetServiceStartMode();
            if (startMode == ServiceStartMode.Disabled)
            {
                SwitchToError(true);
                return;
            }

            cmdServiceRetry_Click(null, null);
        }

        /// <summary>
        /// Starts the Windows Update Agent service if possible
        /// </summary>
        /// <returns>The new status of the Windows Update Agent service</returns>
        public ServiceControllerStatus StartWuaService()
        {
            System.Threading.Thread.Sleep(500);

            /* CODE: DISABLED FOR SECURITY REASONS> REQUIRES A LOT MORE TESTING
            if (string.IsNullOrWhiteSpace(WuaService.GetWuaBinaryPath()))
            {
                WuaService.SetWuaBinaryPath(null); // This should restore the default path
                System.Threading.Thread.Sleep(1000);
            }
            */

            ServiceControllerStatus finalStatus = ServiceControllerStatus.Stopped;

            // Try to enable Service
            if (!WuaService.SetServiceStartMode(ServiceStartMode.Manual))
                return ServiceControllerStatus.Stopped;

            try
            {
                WuaService service = WuaService.StartNew();
                finalStatus = service.Status;
            }
            catch (Exception ex)
            {
                // Swallow (for now)
                System.Diagnostics.Debugger.Log(1, "Exception", "[" + ex.GetType().Name + "] " + ex.Message);
            }

            return finalStatus;
        }

        private void SwitchToError(bool enable)
        {
            panelWuaConnectError.Visible = enable;
            lblStatus.Visible = !enable;
            CanClose = enable;

            if (enable)
                picIcon.Image = GraphicalResources.AlertBig;
            else
                picIcon.Image = GraphicalResources.Logo;
        }

        private void WuaServiceStartComplete(ServiceControllerStatus newStatus)
        {
            if (newStatus != ServiceControllerStatus.Running)
            {
                SwitchToError(true);
                return;
            }

            WuaVersionInformation infoObj = new WuaVersionInformation();
            infoObj.ProgressUpdated += WuaServiceCheckProgressUpdated;
            infoObj.BackgroundOperationCompleted += WuaServiceCheckComplete;
            infoObj.GetLocalInfoAsync();
        }

        private void WuaServiceCheckProgressUpdated(WuaVersionInformation sender, WuaVersionInformationEventArgs args)
        {
            if (InvokeRequired)
            {
                WuaVersionInformation.ProgressUpdatedDelegate d = new WuaVersionInformation.ProgressUpdatedDelegate(WuaServiceCheckProgressUpdated);
                Invoke(d, sender, args);
                return;
            }

            lblStatus.Text = args.Message;
        }

        private void WuaServiceCheckComplete(WuaVersionInformation obj, WuaVersionInformationEventArgs args)
        {
            if (InvokeRequired)
            {
                WuaVersionInformation.ProgressUpdatedDelegate d = new WuaVersionInformation.ProgressUpdatedDelegate(WuaServiceCheckComplete);
                Invoke(d, obj, args);
                return;
            }

            if (!obj.IsWuaAvailable)
            {
                
            }

            if (obj.IsUpdateAvailable)
            {
                
            }

           Thread.Sleep(1000);

            Hide();
            CanClose = true;
            WndMain main = new WndMain();
            ApplicationSettings.Settings.Volatile.MainFormReference = main;

            Close();
        }

        private void WndBootstrap_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!CanClose && e.CloseReason == CloseReason.UserClosing)
                e.Cancel = true;
        }

        private void cmdServiceRetry_Click(object sender, EventArgs e)
        {
            lblServiceErrorMessage.Text = _serviceOperationTimeoutMessage;
            cmdServiceRetry.Text = "Retry";

            SwitchToError(false);
            BackgroundServiceManager.Invoke();
        }

        private void cmdServiceCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
