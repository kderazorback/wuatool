﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace com.razorsoftware.wuatool
{
    /// <summary>
    /// Provides a Wrapper for executing Methods in background Threads, with optional UI Thread callback support
    /// </summary>
    internal class AsyncMethodWrapper
    {
        // Instance Fields
        protected internal Thread ThisThread;
        protected internal Delegate TargetDelegate;
        /// <summary>
        /// Callback Method to invoke when the background execution ends
        /// </summary>
        protected internal virtual Action CallbackMethod { get; set; }
        /// <summary>
        /// Parameters for the invocation of the background delegate
        /// </summary>
        protected internal object[] InvocationParameters;
        /// <summary>
        /// (Optional) Control object where the callback will be invoked after the background execution ends
        /// </summary>
        protected internal Control InvocationOwner;


        // Instance Properties
        /// <summary>
        /// Stores the Background thread used for invoking the specified Delegate
        /// </summary>
        public Thread BackgroundThread { get { return ThisThread; } }


        // Static Methods
        /// <summary>
        /// Creates a new instance of this class wrapping around the specified Delegate
        /// </summary>
        /// <param name="d">Delegate to call asynchronously</param>
        /// <param name="callback">Callback method that will be fired when the target delegate returns</param>
        /// <param name="invokeOwner">An optional <see cref="Control"/> object used to relay the callback method invoke to the UI Thread</param>
        /// <param name="arguments">Arguments passed to the target Delegate</param>
        /// <returns>A new instance of this class that can be used to asynchronously call the specified Delegate</returns>
        public static AsyncMethodWrapper WrapAround(Delegate d, Action callback, Control invokeOwner, params object[] arguments)
        {
            AsyncMethodWrapper instance = new AsyncMethodWrapper();
            instance.ThisThread = new Thread(instance.InternalInvoke);
            instance.ThisThread.IsBackground = true;
            instance.ThisThread.Priority = ThreadPriority.Lowest;

            instance.TargetDelegate = d;
            instance.CallbackMethod = callback;
            instance.InvocationParameters = arguments;
            instance.InvocationOwner = invokeOwner;

            return instance;
        }


        // Instance Methods
        /// <summary>
        /// Internally invokes the target delegate in the background
        /// </summary>
        protected internal virtual void InternalInvoke()
        {
            TargetDelegate.DynamicInvoke(InvocationParameters);

            if (CallbackMethod == null)
                return;

            try
            {
                if (InvocationOwner != null)
                {
                    InvocationOwner.Invoke(CallbackMethod);
                }
                else
                    CallbackMethod.Invoke();
            }
            catch (Exception)
            {
                // Swallow
            }
            
        }

        /// <summary>
        /// Invokes the wrapped Delegate asynchronously
        /// </summary>
        public void Invoke()
        {
            if (!ThisThread.ThreadState.HasFlag(ThreadState.Running))
                throw new InvalidOperationException("The specified thread is already running.");

            ThisThread.Start();
        }

        /// <summary>
        /// Locks the calling thread until the background delegate execution is complete
        /// </summary>
        /// <param name="millisecondsTimeout">Optional timeout value for the operation</param>
        /// <returns>True if the thread has ended before the timeout, otherwise false</returns>
        public bool Join(int millisecondsTimeout)
        {
            return ThisThread.Join(millisecondsTimeout);
        }

        /// <summary>
        /// Terminates the background thread abruptly. This doesnt make the object invoke the callback method.
        /// </summary>
        public void Abort()
        {
            ThisThread.Abort();
        }
    }



    /// <summary>
    /// Provides a Wrapper for executing Methods that return a value in background Threads, with optional UI Thread callback support
    /// </summary>
    /// <typeparam name="T">The return type of the wrapped method</typeparam>
    internal class AsyncMethodWrapper<T> : AsyncMethodWrapper
    {
        // Instance Fields
        /// <summary>
        /// Callback Method to invoke when the background execution ends
        /// </summary>
        protected new Action<T> CallbackMethod { get; set; }

        // Static Methods
        /// <summary>
        /// Creates a new instance of this class wrapping around the specified Delegate
        /// </summary>
        /// <param name="d">Delegate to call asynchronously</param>
        /// <param name="callback">Callback method that will be fired when the target delegate returns</param>
        /// <param name="invokeOwner">An optional <see cref="Control"/> object used to relay the callback method invoke to the UI Thread</param>
        /// <param name="arguments">Arguments passed to the target Delegate</param>
        /// <returns>A new instance of this class that can be used to asynchronously call the specified Delegate</returns>
        public static AsyncMethodWrapper<T> WrapAround(Delegate d, Action<T> callback, Control invokeOwner, params object[] arguments)
        {
            AsyncMethodWrapper<T> instance = new AsyncMethodWrapper<T>();
            instance.ThisThread = new Thread(instance.InternalInvoke);
            instance.ThisThread.IsBackground = true;
            instance.ThisThread.Priority = ThreadPriority.Lowest;

            instance.TargetDelegate = d;
            instance.CallbackMethod = callback;
            instance.InvocationParameters = arguments;
            instance.InvocationOwner = invokeOwner;

            return instance;
        }

        // Instance Methods
        /// <summary>
        /// Internally invokes the target delegate in the background
        /// </summary>
        protected internal override sealed void InternalInvoke()
        {
            T result = (T)TargetDelegate.DynamicInvoke(InvocationParameters);

            if (CallbackMethod == null)
                return;

            try
            {
                if (InvocationOwner != null)
                {
                    InvocationOwner.Invoke(CallbackMethod, result);
                }
                else
                    CallbackMethod.Invoke(result);
            }
            catch (Exception)
            {
                // Swallow
            }
        }
    }
}
