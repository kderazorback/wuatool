﻿using Microsoft.Win32;

namespace com.razorsoftware.wuatool
{
    internal class SystemInfo
    {
        public struct OsDetails
        {
            public string FriendlyName;
            public string RegisteredOwner;
            public string BuildNumber;
            public string InstallationType;
            public string Edition;

            public override string ToString()
            {
                return FriendlyName + " (" + Edition + "). Build " + BuildNumber;
            }
        }

        public static OsDetails GetSystemInfo()
        {
            OsDetails result = new OsDetails();
            string path = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion";

            result.FriendlyName = GetValueFromRegistryOrDefault(path, "ProductName", "Unknown Windows Version");
            string buildNumber = GetValueFromRegistryOrDefault(path, "ReleaseId", "");

            if (string.IsNullOrWhiteSpace(buildNumber))
                buildNumber = GetValueFromRegistryOrDefault(path, "CurrentBuild", "0000");

            result.BuildNumber = buildNumber;
            result.Edition = GetValueFromRegistryOrDefault(path, "EditionID", "");
            result.InstallationType = GetValueFromRegistryOrDefault(path, "InstallationType", "Client");
            result.RegisteredOwner = GetValueFromRegistryOrDefault(path, "RegisteredOwner", "Owner");

            return result;
        }

        private static string GetValueFromRegistryOrDefault(string path, string key, string defaultValue)
        {
            string val = Registry.GetValue(path, key, null) as string;

            if (string.IsNullOrEmpty(val))
                return defaultValue;

            return val;
        }
    }
}
