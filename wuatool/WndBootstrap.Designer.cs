﻿namespace com.razorsoftware.wuatool
{
    partial class WndBootstrap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WndBootstrap));
            this.picIcon = new System.Windows.Forms.PictureBox();
            this.lblAppTitle = new System.Windows.Forms.Label();
            this.lblAppVersion = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblWinVersion = new System.Windows.Forms.Label();
            this.panelWuaConnectError = new System.Windows.Forms.Panel();
            this.cmdServiceCancel = new System.Windows.Forms.Button();
            this.cmdServiceRetry = new System.Windows.Forms.Button();
            this.lblServiceErrorMessage = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).BeginInit();
            this.panelWuaConnectError.SuspendLayout();
            this.SuspendLayout();
            // 
            // picIcon
            // 
            this.picIcon.Location = new System.Drawing.Point(422, 12);
            this.picIcon.Name = "picIcon";
            this.picIcon.Size = new System.Drawing.Size(128, 128);
            this.picIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picIcon.TabIndex = 0;
            this.picIcon.TabStop = false;
            // 
            // lblAppTitle
            // 
            this.lblAppTitle.AutoSize = true;
            this.lblAppTitle.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAppTitle.Location = new System.Drawing.Point(36, 32);
            this.lblAppTitle.Name = "lblAppTitle";
            this.lblAppTitle.Size = new System.Drawing.Size(128, 32);
            this.lblAppTitle.TabIndex = 1;
            this.lblAppTitle.Text = "WUA Tool";
            // 
            // lblAppVersion
            // 
            this.lblAppVersion.AutoSize = true;
            this.lblAppVersion.Location = new System.Drawing.Point(65, 64);
            this.lblAppVersion.Name = "lblAppVersion";
            this.lblAppVersion.Size = new System.Drawing.Size(60, 13);
            this.lblAppVersion.TabIndex = 2;
            this.lblAppVersion.Text = "<version>";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Location = new System.Drawing.Point(13, 253);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(70, 13);
            this.lblStatus.TabIndex = 3;
            this.lblStatus.Text = "Initializing...";
            // 
            // lblWinVersion
            // 
            this.lblWinVersion.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWinVersion.Location = new System.Drawing.Point(146, 253);
            this.lblWinVersion.Name = "lblWinVersion";
            this.lblWinVersion.Size = new System.Drawing.Size(401, 13);
            this.lblWinVersion.TabIndex = 4;
            this.lblWinVersion.Text = "<winver>";
            this.lblWinVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelWuaConnectError
            // 
            this.panelWuaConnectError.BackColor = System.Drawing.Color.Transparent;
            this.panelWuaConnectError.Controls.Add(this.cmdServiceCancel);
            this.panelWuaConnectError.Controls.Add(this.cmdServiceRetry);
            this.panelWuaConnectError.Controls.Add(this.lblServiceErrorMessage);
            this.panelWuaConnectError.Controls.Add(this.label1);
            this.panelWuaConnectError.Location = new System.Drawing.Point(2, 80);
            this.panelWuaConnectError.Name = "panelWuaConnectError";
            this.panelWuaConnectError.Size = new System.Drawing.Size(414, 158);
            this.panelWuaConnectError.TabIndex = 5;
            this.panelWuaConnectError.Visible = false;
            // 
            // cmdServiceCancel
            // 
            this.cmdServiceCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdServiceCancel.Location = new System.Drawing.Point(245, 115);
            this.cmdServiceCancel.Name = "cmdServiceCancel";
            this.cmdServiceCancel.Size = new System.Drawing.Size(122, 31);
            this.cmdServiceCancel.TabIndex = 3;
            this.cmdServiceCancel.Text = "Cancel && Quit";
            this.cmdServiceCancel.UseVisualStyleBackColor = true;
            this.cmdServiceCancel.Click += new System.EventHandler(this.cmdServiceCancel_Click);
            // 
            // cmdServiceRetry
            // 
            this.cmdServiceRetry.Location = new System.Drawing.Point(40, 115);
            this.cmdServiceRetry.Name = "cmdServiceRetry";
            this.cmdServiceRetry.Size = new System.Drawing.Size(122, 31);
            this.cmdServiceRetry.TabIndex = 2;
            this.cmdServiceRetry.Text = "Enable Service";
            this.cmdServiceRetry.UseVisualStyleBackColor = true;
            this.cmdServiceRetry.Click += new System.EventHandler(this.cmdServiceRetry_Click);
            // 
            // lblServiceErrorMessage
            // 
            this.lblServiceErrorMessage.AutoEllipsis = true;
            this.lblServiceErrorMessage.Location = new System.Drawing.Point(40, 50);
            this.lblServiceErrorMessage.Name = "lblServiceErrorMessage";
            this.lblServiceErrorMessage.Size = new System.Drawing.Size(347, 62);
            this.lblServiceErrorMessage.TabIndex = 1;
            this.lblServiceErrorMessage.Text = "The Windows Update Agent service (WUA) is currently Disabled.\r\nThis application r" +
    "equires that the service is enabled and started.\r\n\r\nEnable the service and start" +
    " it now? ";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Could not connect to Windows Update Agent";
            // 
            // WndBootstrap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(562, 278);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblAppVersion);
            this.Controls.Add(this.lblAppTitle);
            this.Controls.Add(this.picIcon);
            this.Controls.Add(this.panelWuaConnectError);
            this.Controls.Add(this.lblWinVersion);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WndBootstrap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WUA Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WndBootstrap_FormClosing);
            this.Load += new System.EventHandler(this.WndBootstrap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).EndInit();
            this.panelWuaConnectError.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picIcon;
        private System.Windows.Forms.Label lblAppTitle;
        private System.Windows.Forms.Label lblAppVersion;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblWinVersion;
        private System.Windows.Forms.Panel panelWuaConnectError;
        private System.Windows.Forms.Label lblServiceErrorMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdServiceCancel;
        private System.Windows.Forms.Button cmdServiceRetry;
    }
}