﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.razorsoftware.wuahelper;
using com.razorsoftware.wuahelper.Services;
using com.razorsoftware.wuahelper.WuaBlocking;
using com.razorsoftware.wuahelper.WuaCom;
using com.razorsoftware.wuatool.ApplicationSettings;

namespace com.razorsoftware.wuatool
{
    internal partial class WndMain : Form
    {
        // Ivars
        private PersistentSet _localNewSettings;
        private int disableWuaCheckCount = 0;


        // Constructor
        public WndMain()
        {
            InitializeComponent();
        }


        // Event Handlers
        private void WndMain_Load(object sender, EventArgs e)
        {
            panelOverlay.Location = Point.Empty;
            panelOverlay.Dock = DockStyle.Fill;
            dgvUpdates.Height = cmdInstallSelected.Top - dgvUpdates.Top - 6;

            Settings.Volatile.MainFormReference = this;
            // Settings.BackgroundTask = WuaTask.CreateForUpdates(); Defer task creation to the Getter of the property
            Settings.Volatile.BackgroundTask.Invocator = this;
            Settings.Volatile.BackgroundTask.SearchComplete += TaskOnSearchComplete;
            Settings.Volatile.BackgroundTask.DownloadProgressUpdated += TaskOnDownloadProgressUpdated;
            Settings.Volatile.BackgroundTask.DownloadComplete += TaskOnDownloadComplete;
            Settings.Volatile.BackgroundTask.InstallProgressUpdated += TaskOnInstallProgressUpdated;
            Settings.Volatile.BackgroundTask.InstallationComplete += TaskOnInstallationComplete;
            Settings.Volatile.BackgroundDownloadList.DownloadStopped += BackgroundDownloadList_DownloadStopped;
            Settings.Volatile.BackgroundDownloadList.DownloadException += BackgroundDownloadList_DownloadException;
            Settings.Volatile.BackgroundDownloadList.DownloadProgressUpdated += BackgroundDownloadList_DownloadProgressUpdated;

            chkSearchOnline_CheckedChanged(null, null); // Update cloud icon based on the checkbox Checked property

            UpdateSystemInfo();
        }

        private void BackgroundDownloadList_DownloadProgressUpdated(wuahelper.Downloads.DownloadList sender, wuahelper.Downloads.DownloadBase download)
        {
            if (InvokeRequired)
            {
                Delegate d = new Action<wuahelper.Downloads.DownloadList, wuahelper.Downloads.DownloadBase>(BackgroundDownloadList_DownloadProgressUpdated);
                this.Invoke(d, new object[] {sender, download});
                return;
            }

            panelProgress.Visible = true;
            lblOperationProgressText.Text = string.Format("Downloading update {0} of {1}...", sender.DownloadPointer + 1, sender.Count);
            lblOperationText.Text = string.Format("Downloading update \"{0}\"...", sender.DownloadPointer + 1);
            lblOperationDetailText.Text = string.Format("{0} downloaded of {1}...",
                GetPrettySize(sender.TotalByteCount), GetPrettySize(sender.DownloadedBytes));
            pbOperationProgress.Value = (int)sender.PercentComplete;
            lblOperationTitle.Text = @"Downloading Update packages...";
        }

        private void BackgroundDownloadList_DownloadException(wuahelper.Downloads.DownloadList sender, wuahelper.Downloads.DownloadBase download, Exception ex)
        {
            if (InvokeRequired)
            {
                Delegate d = new Action<wuahelper.Downloads.DownloadList, wuahelper.Downloads.DownloadBase, Exception>(BackgroundDownloadList_DownloadException);
                this.Invoke(d, new object[] { sender, download, ex });
                return;
            }

            string message = toolTips.GetToolTip(picTaskErrorsFound);
            message += Environment.NewLine + download.Name +
                       " failed to download because an unexpected exception was found. " + ex.GetType().Name + ": " + ex.Message;
            toolTips.SetToolTip(picTaskErrorsFound, message);
            panelTaskErrors.Visible = true;
        }

        private void BackgroundDownloadList_DownloadStopped(wuahelper.Downloads.DownloadList sender, wuahelper.Downloads.DownloadBase download)
        {
            if (InvokeRequired)
            {
                Delegate d = new Action<wuahelper.Downloads.DownloadList, wuahelper.Downloads.DownloadBase>(BackgroundDownloadList_DownloadStopped);
                this.Invoke(d, new object[] { sender, download });
                return;
            }

            if (download.RemainingBytes > 0)
            {
                string message = toolTips.GetToolTip(picTaskErrorsFound);
                message += Environment.NewLine + download.Name +
                           " were partially downloaded due to a network error.";
                toolTips.SetToolTip(picTaskErrorsFound, message);
                panelTaskErrors.Visible = true;
            }

            if (sender.IsDownloading)
                return; // Still downloading

            if (!panelTaskErrors.Visible)
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine("Some updates failed to be downloaded by the Windows Update Service.");
                message.AppendLine();

                message.AppendLine(toolTips.GetToolTip(picTaskErrorsFound));

                MessageBox.Show(message.ToString(), @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            Settings.Volatile.IsDownloading = false;
            Settings.Volatile.UpdateInfoObsolete = true;
            Settings.Volatile.IsInstalling = false;
            UpdateUi();
        }

        public void ShowPanel(bool visible)
        {
            panelHeadControls.Enabled = !visible;

            if (visible)
            {
                panelOverlay.Visible = true;
                panelProgress.Visible = false;
                picSpinner.Enabled = true;
                picSpinner.Select();
                panelOverlay.BringToFront();
                toolTips.SetToolTip(picTaskErrorsFound, "");
                panelTaskErrors.Visible = false;
            }
            else
            {
                panelOverlay.Visible = false;
                picSpinner.Enabled = false;
                dgvUpdates.Focus();
                dgvUpdates.Refresh();
            }
        }

        private void cmdRefreshUpdates_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WuaUpdateSearchFlags flags = 0;

            if (chkShowDrivers.Checked)
                flags |= WuaUpdateSearchFlags.Driver;

            if (chkShowHidden.Checked)
                flags |= WuaUpdateSearchFlags.Hidden;

            if (chkShowInstalled.Checked)
                flags |= WuaUpdateSearchFlags.Installed;

            if (chkShowNotInstalled.Checked)
                flags |= WuaUpdateSearchFlags.NotInstalled;

            if (chkShowSoftware.Checked)
                flags |= WuaUpdateSearchFlags.Software;

            ShowPanel(true);

            /*AsyncMethodWrapper<WuaUpdateWrapper[]>.WrapAround(
                new Func<WuaUpdateSearchFlags, WuaUpdateWrapper[]>(WuaSearch.GetUpdates), GetUpdatesCallback, this,
                flags).Invoke();*/

            Settings.Volatile.IsSearching = true;
            Settings.Volatile.IsDownloading = false;
            Settings.Volatile.IsInstalling = false;
            lblOperationTitle.Text = "Refreshing Update information...";
            Settings.Volatile.BackgroundTask.SelectedUids = new string[] {};
            Settings.Volatile.BackgroundTask.Online = chkSearchOnline.Checked;
            Settings.Volatile.BackgroundTask.SolveUpdatesAsync(flags);
        }

        public void UpdateUi()
        {
            if (panelOverlay.InvokeRequired)
                throw new AccessViolationException("A Thread invocation is required since the call has not been made on the UI Thread.");

            ShowPanel(true);
            string[] selectedUpdates = GetSelectedUpdateIds();
            dgvUpdates.Rows.Clear();
            foreach (WuaUpdateWrapper update in Settings.Volatile.Updates)
            {
                Image icon = GraphicalResources.Available;
                string tooltip = null;

                if (update.IsCritical)
                {
                    icon = GraphicalResources.Alert;
                    tooltip = "This update is Critical";
                }

                if (update.IsDownloaded)
                {
                    icon = GraphicalResources.Downloaded;
                    tooltip = "Downloaded";

                    if (update.IsCritical)
                    {
                        icon = GraphicalResources.DownloadedCritical;
                        tooltip = "This update is Critical, and is already Downloaded";
                    }
                }

                if (update.IsInstalled)
                {
                    icon = GraphicalResources.Ok;
                    tooltip = "Already Installed";
                }

                int index = dgvUpdates.Rows.Add(false, icon, GraphicalResources.SearchDisabled, update.Name, GetPrettySize(update.Size), update.Id,
                    (update.HasKbArticle ? "KB" + update.KbArticle : ""));

                dgvUpdates.Rows[index].Cells[colIcon.Index].ToolTipText = tooltip;
                if (update.HasKbArticle)
                {
                    dgvUpdates.Rows[index].Cells[colKbArticle.Index].ToolTipText =
                        "Double click to open the KB Article in a browser." + Environment.NewLine + Environment.NewLine +
                        update.KbArticleUrl;
                    dgvUpdates.Rows[index].Cells[colKbArticle.Index].Tag = update.KbArticleUrl;
                }
                if (update.HasId)
                {
                    dgvUpdates.Rows[index].Cells[colId.Index].ToolTipText =
                        "Double click to open the Update ID on the Windows Update Catalog site." + Environment.NewLine +
                        Environment.NewLine + update.InformationUrl;
                    dgvUpdates.Rows[index].Cells[colId.Index].Tag = update.InformationUrl;
                }

                Settings.Volatile.NetworkTaskQueue.Enqueue(
                    AsyncMethodWrapper.WrapAround(new Action<int>(GetDownloadLinkFor), null, this, index));
            }

            Debug.Assert(!Settings.Volatile.IsActive);
            lblOutdatedCacheText.Visible = Settings.Volatile.UpdateInfoObsolete;

            UpdateSystemInfo();

            SetSelectedUpdateIds(selectedUpdates);

            ShowPanel(false);
        }

        private void UpdateSystemInfo()
        {
            // Get System Info
            SystemInfo.OsDetails osinfo = SystemInfo.GetSystemInfo();
            lblSystemName.Text = osinfo.FriendlyName;
            lblSystemEdition.Text = osinfo.Edition;
            lblSystemBuildIdentifier.Text = osinfo.BuildNumber;
        }

        private void GetDownloadLinkFor(int index)
        {
            if (dgvUpdates.RowCount <= index)
                return;

            dgvUpdates.Rows[index].Cells[colDownload.Index].Value = GraphicalResources.Search;

            string str;
            try
            {
                str = Settings.Volatile.Updates[index].GetDownloadUrl();
            }
            catch (Exception ex)
            {
                dgvUpdates.Rows[index].Cells[colDownload.Index].ToolTipText =
                "Cannot retrieve the download link for this download." +
                Environment.NewLine + Environment.NewLine + "[" + ex.GetType().Name + "]" + Environment.NewLine + ex.Message;
                dgvUpdates.Rows[index].Cells[colDownload.Index].Value = GraphicalResources.DownloadNotAvailable;
                return;
            }

            if (string.IsNullOrWhiteSpace(str))
            {
                dgvUpdates.Rows[index].Cells[colDownload.Index].ToolTipText =
                "There is no package download available for this update.";
                dgvUpdates.Rows[index].Cells[colDownload.Index].Value = GraphicalResources.DownloadDisabled;
                return;
            }

            dgvUpdates.Rows[index].Cells[colDownload.Index].Value = GraphicalResources.Download;
            dgvUpdates.Rows[index].Cells[colDownload.Index].ToolTipText =
                "Click to open the Download URL in a browser and start downloading the update file." +
                Environment.NewLine + Environment.NewLine + str;
            dgvUpdates.Rows[index].Cells[colDownload.Index].Tag = str;

        }

        public static string GetPrettySize(long value)
        {
            string[] sizes = {" B", " KB", " MB", " GB", " TB"};
            float fValue = value;

            int index = 0;
            while (index < sizes.Length)
            {
                if (fValue < 1024.0f)
                    break;

                fValue /= 1024;
                index++;
            }

            return fValue.ToString("N1") + sizes[index];
        }

        public string[] GetSelectedUpdateIds()
        {
            if (dgvUpdates.Rows.Count < 1)
                return new string[0];

            List<string> output = new List<string>(dgvUpdates.Rows.Count);

            for (int i = 0; i < dgvUpdates.Rows.Count; i++)
            {
                if ((bool)dgvUpdates.Rows[i].Cells[colSelected.Index].Value)
                    output.Add(dgvUpdates.Rows[i].Cells[colId.Index].Value.ToString());
            }

            return output.ToArray();
        }

        public void SetSelectedUpdateIds(string[] ids)
        {
            dgvUpdates.SuspendLayout();

            if (ids == null)
                ids = new string[0];

            if (dgvUpdates.Rows.Count < 1)
            {
                dgvUpdates.ResumeLayout();
                return;
            }

            for (int i = 0; i < dgvUpdates.Rows.Count; i++)
            {
                string rowId = dgvUpdates.Rows[i].Cells[colId.Index].Value.ToString();

                bool value = false;
                for (int x = 0; x < ids.Length; x++)
                {
                    if (string.Equals(rowId, ids[x], StringComparison.OrdinalIgnoreCase))
                    {
                        value = true;
                        break;
                    }
                }

                dgvUpdates.Rows[i].Cells[colSelected.Index].Value = value;
            }

            dgvUpdates.ResumeLayout();
        }

        private void dgvUpdates_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            object data = dgvUpdates.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag;

            if (data == null) return;

            string dataStr = data as string;

            if (string.IsNullOrWhiteSpace(dataStr)) return;

            Process.Start(dataStr);
        }

        private void dgvUpdates_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != colDownload.Index)
                return;

            dgvUpdates_CellDoubleClick(sender, e);
        }

        private void cmdInstallSelected_Click(object sender, EventArgs e)
        {
            List<string> selectedUids = new List<string>();
            foreach (DataGridViewRow row in dgvUpdates.Rows)
            {
                if ((bool)row.Cells[colSelected.Index].Value)
                    selectedUids.Add(Settings.Volatile.Updates[row.Index].Id);
            }

            if (selectedUids.Count < 1)
            {
                MessageBox.Show("You must check one or more updates first.", "Cant install Updates", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ShowPanel(true);

            Settings.Volatile.IsSearching = true;
            Settings.Volatile.IsDownloading = true;
            Settings.Volatile.IsInstalling = true;
            Settings.Volatile.BackgroundTask.Online = chkSearchOnline.Checked;
            Settings.Volatile.BackgroundTask.SelectedUids = selectedUids.ToArray();
            Settings.Volatile.BackgroundTask.SolveUpdatesAsync();
        }

        private void TaskOnSearchComplete(WuaUpdateWrapper[] updates, string[] missingUpdates, XmlTypeSerializer result)
        {
            Settings.Volatile.Updates = updates;
            bool downloadToo = Settings.Volatile.IsDownloading;

            if (Settings.Persistent.DumpUpdateXmlTrees)
            {
                // Dump XML files to disk
                List<String> failedDumps = new List<string>();
                foreach (WuaUpdateWrapper update in updates)
                {
                    try
                    {
                        string xml = update.ToXml();
                        if (string.IsNullOrWhiteSpace(xml))
                            throw new NullReferenceException("The XML tree returned by the API wrapper is empty.");

                        DirectoryInfo di = new DirectoryInfo(Path.Combine(".\\", Settings.Persistent.DumpUpdateXmlTreesDirectory));
                        if (!di.Exists)
                            di.Create();

                        FileInfo fi = new FileInfo(Path.Combine(di.FullName, update.Id + ".xml"));

                        using (
                            FileStream fs = new FileStream(fi.FullName, FileMode.Create, FileAccess.Write,
                                FileShare.None))
                        {
                            byte[] buffer =
                                System.Text.Encoding.UTF8.GetBytes("<?xml version=\"1.0\" encoding=\"utf - 8\"?>");
                            fs.Write(buffer, 0, buffer.Length);
                            buffer = System.Text.Encoding.UTF8.GetBytes(xml);
                            fs.Write(buffer, 0, buffer.Length);
                        }
                    }
                    catch (Exception ex)
                    {
                        failedDumps.Add("[" + update.Id + "] " + ex.GetType().Name + ": " + ex.Message);
                    }
                }

                if (failedDumps.Count > 0)
                {
                    StringBuilder message = new StringBuilder();
                    message.AppendLine(
                        "The application is configured to dump XML information to disk for every update queried. But some updated failed to be dumped.");
                    message.AppendLine();

                    for (int i = 0; i < failedDumps.Count; i++)
                    {
                        if (i > 7)
                        {
                            message.AppendLine("... Continues for " + failedDumps.Count.ToString("N0") + " more errors.");
                            break;
                        }
                        message.AppendLine(failedDumps[i]);
                    }

                    MessageBox.Show(message.ToString(), "XML Dump failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            if (missingUpdates != null && missingUpdates.Length > 0)
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine("Some updates failed to be resolved by the Windows Update Service.");
                message.AppendLine();

                for (int i = 0; i < missingUpdates.Length; i++)
                    message.AppendLine(string.Format("[{0}]: {1}", i, missingUpdates[i]));

                MessageBox.Show(message.ToString(), @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                downloadToo = false;
            }

            Settings.Volatile.IsSearching = false;

            if (downloadToo)
            {
                Settings.Volatile.IsDownloading = false;
                Settings.Volatile.BackgroundTask.DownloadUpdatesAsync();
            }
            else
            {
                Settings.Volatile.UpdateInfoObsolete = false;
                Settings.Volatile.IsDownloading = false;
                Settings.Volatile.IsInstalling = false;
                UpdateUi();
            }
        }

        private void TaskOnDownloadProgressUpdated(WuaDownloadProgress progress)
        {
            panelProgress.Visible = true;
            lblOperationProgressText.Text = string.Format("Downloading update {0} of {1}...", progress.UpdateIndex + 1, progress.Updates.Length);
            lblOperationText.Text = string.Format("Downloading update \"{0}\"...", progress.CurrentUpdate.Id);
            lblOperationDetailText.Text = string.Format("{0} downloaded of {1}...",
                GetPrettySize(progress.UpdateBytesDownloaded), GetPrettySize(progress.UpdateTotalBytes));
            pbOperationProgress.Value = (int)progress.PercentComplete;
            lblOperationTitle.Text = @"Downloading Update packages...";
        }

        private void TaskOnInstallProgressUpdated(WuaInstallProgress progress)
        {
            panelProgress.Visible = true;
            lblOperationProgressText.Text = string.Format("Installing update {0} of {1}...", progress.UpdateIndex + 1, progress.Updates.Length);
            lblOperationText.Text = string.Format("Installing update \"{0}\"...", progress.CurrentUpdate.Id);
            lblOperationDetailText.Visible = false;
            pbOperationProgress.Value = (int)progress.PercentComplete;
            lblOperationTitle.Text = @"Applying Updates...";
        }

        private void TaskOnDownloadComplete(KeyValuePair<WuaUpdateWrapper, string>[] results, bool succeedded)
        {
            bool installToo = Settings.Volatile.IsInstalling;

            if (!succeedded)
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine("Some updates failed to be downloaded by the Windows Update Service.");
                message.AppendLine();

                for (int i = 0; i < results.Length; i++)
                    message.AppendLine(string.Format("[{0}]: {1}={2}", i, results[i].Key.Id, results[i].Value));

                MessageBox.Show(message.ToString(), @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                installToo = false;
            }

            Settings.Volatile.IsDownloading = false;

            Settings.Volatile.UpdateInfoObsolete = true;

            if (installToo)
            {
                Settings.Volatile.IsInstalling = false;
                Settings.Volatile.BackgroundTask.InstallUpdatesAsync();
                lblOperationTitle.Text = @"Applying Updates...";
                panelProgress.Visible = false;

            }
            else
            {
                Settings.Volatile.IsInstalling = false;
                UpdateUi();
            }
        }

        private void TaskOnInstallationComplete(KeyValuePair<WuaUpdateWrapper, string>[] results, bool succeedded)
        {
            if (!succeedded)
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine("Some updates failed to be installed by the Windows Update Service.");
                message.AppendLine();

                for (int i = 0; i < results.Length; i++)
                    message.AppendLine(string.Format("[{0}]: {1}={2}", i, results[i].Key.Id, results[i].Value));

                MessageBox.Show(message.ToString(), @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            Settings.Volatile.UpdateInfoObsolete = true;
            UpdateUi();
        }

        private void WndMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Volatile.MainFormReference = null;
            Settings.Volatile.BackgroundTask.Dispose();
            Settings.Volatile.Updates = null;
        }

        private void cmdDownloadSelected_Click(object sender, EventArgs e)
        {
            List<string> selectedUids = new List<string>();
            foreach (DataGridViewRow row in dgvUpdates.Rows)
            {
                if ((bool)row.Cells[colSelected.Index].Value)
                    selectedUids.Add(Settings.Volatile.Updates[row.Index].Id);
            }

            if (selectedUids.Count < 1)
            {
                MessageBox.Show("You must check one or more updates first.", "Cant download Updates", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ShowPanel(true);

            Settings.Volatile.IsSearching = true;
            Settings.Volatile.IsDownloading = true;
            Settings.Volatile.IsInstalling = false;
            Settings.Volatile.BackgroundTask.Online = chkSearchOnline.Checked;
            Settings.Volatile.BackgroundTask.SelectedUids = selectedUids.ToArray();
            Settings.Volatile.BackgroundTask.SolveUpdatesAsync();
        }

        private void chkSearchOnline_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSearchOnline.Checked)
            {
                picOnlineMode.Image = GraphicalResources.CloudOnline;
                toolTips.SetToolTip(picOnlineMode, "Windows Update Agent (WUA) will go Online and retrieve information from the configured Windows Server Update Services (WSUS)");
            }
            else
            {
                picOnlineMode.Image = GraphicalResources.CloudOffline;
                toolTips.SetToolTip(picOnlineMode, "Windows Update Agent (WUA) will work Offline and retrieve information from its cache only.");
            }
        }

        private void cmdDownloadAdvanced_Click(object sender, EventArgs e)
        {
            if (cmdDownloadAdvanced.ContextMenuStrip.Visible)
                cmdDownloadAdvanced.ContextMenuStrip.Hide();
            else
                cmdDownloadAdvanced.ContextMenuStrip.Show(cmdDownloadAdvanced, new Point(cmdDownloadAdvanced.Width, 0));
        }

        private void picOnlineMode_Click(object sender, EventArgs e)
        {
            chkSearchOnline.Checked = !chkSearchOnline.Checked;
        }

        private void mnuItemDownloadToDirectory_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, string>> selectedupdates = new List<KeyValuePair<string, string>>();
            foreach (DataGridViewRow row in dgvUpdates.Rows)
            {
                if ((bool) row.Cells[colSelected.Index].Value)
                {
                    if (string.IsNullOrWhiteSpace(Settings.Volatile.Updates[row.Index].DownloadUrl))
                    {
                        MessageBox.Show(
                            string.Format("The update \"{0}\" cannot be downloaded from the cloud because its not available via Microsoft Update Catalog.", Settings.Volatile.Updates[row.Index].Name),
                            "Cant download update", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    selectedupdates.Add(new KeyValuePair<string, string>(Settings.Volatile.Updates[row.Index].Name,
                        Settings.Volatile.Updates[row.Index].DownloadUrl));
                }
            }

            if (selectedupdates.Count < 1)
            {
                MessageBox.Show("You must check one or more updates first.", "Cant download Updates", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Select destination directory for downloaded files.";
            if (fbd.ShowDialog() != DialogResult.OK)
                return;

            DirectoryInfo di = new DirectoryInfo(fbd.SelectedPath);
            if (!di.Exists)
                di.Create();


            ShowPanel(true);

            Settings.Volatile.IsSearching = false;
            Settings.Volatile.IsDownloading = true;
            Settings.Volatile.IsInstalling = false;
            Settings.Volatile.BackgroundDownloadList.Clear();

            Parallel.ForEach(selectedupdates, (upd) =>
            {
                wuahelper.Downloads.HttpDownload download = new wuahelper.Downloads.HttpDownload(upd.Key, upd.Value,
                    Path.Combine(di.FullName,
                        upd.Key + "." +
                        upd.Value.Split(new string[] {"."}, StringSplitOptions.RemoveEmptyEntries).Last()));
                download.GetInfo();
                Settings.Volatile.BackgroundDownloadList.Add(download);
            });

            Settings.Volatile.BackgroundDownloadList.Start();
        }

        private void mnuItemCacheWua_Click(object sender, EventArgs e)
        {

        }

        private void cmdSettingsRestore_Click(object sender, EventArgs e)
        {
            _localNewSettings = new PersistentSet();

            Settings.Volatile.DestroyWuaInstallationOnExit = false;

            UpdateSettingsView();
        }

        private void cmdSettingsDiscard_Click(object sender, EventArgs e)
        {
            _localNewSettings = Settings.Persistent;

            UpdateSettingsView();
            panelSettings.Visible = false;
        }

        private void cmdSettingsApply_Click(object sender, EventArgs e)
        {
            Settings.Persistent = _localNewSettings;

            panelSettings.Visible = false;
        }


        // Methods
        public void UpdateSettingsView()
        {
            chkDisableWuaOnExit.Checked = _localNewSettings.DisableWuaServiceOnExit;
            chkDisableBitsOnExit.Checked = _localNewSettings.DisableBitsServiceOnExit;
            chkDisableSedOnExit.Checked = _localNewSettings.DisableSedServiceOnExit;
            chkWuaEnforceGpo.Checked = _localNewSettings.EnableWuaServicePolicyRestrictions;
            chkWuaSetRegistryRestrictions.Checked = _localNewSettings.EnableWuaServiceRegistryRestrictions;
            chkWuaSetConnectionsMetered.Checked = _localNewSettings.SetAllConnectionsToMetered;

            PolicyBlocking policyBlocker = new PolicyBlocking();
            bool policyBlocked = policyBlocker.GetBlockStatus();
            bool registryBlocked = RegistryBlocking.IsBlocked;

            cmdWuaPolicyUnblock.Visible = policyBlocked;
            lblWuaPolicyEnforced.Visible = policyBlocked;

            cmdWuaRegistryBlocked.Visible = registryBlocked;
            lblWuaRegistryBlocked.Visible = registryBlocked;
        }

        private void chkDisableWuaOnExit_CheckedChanged(object sender, EventArgs e)
        {
            _localNewSettings.DisableWuaServiceOnExit = chkDisableWuaOnExit.Checked;
            disableWuaCheckCount++;

            if (disableWuaCheckCount > 15 && !Settings.Volatile.DestroyWuaInstallationOnExit)
            {
                if (MessageBox.Show("Are you sure you want to remove (and potentially destroy) the Windows Update service on this system?\n\nWhen doing so, a placeholder tool will be installed to prevent automatic reinstallations of the Windows Update service components. Take in mind that doing this will prevent running this tool again, and can only be undone by launching the wuaplaceholder tool or by performing a system repair with \"SFC /scannow\" as Administrator from the Command Line.\nAre you sure you want to continue?", "WUATool destructive operation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    Settings.Volatile.DestroyWuaInstallationOnExit = true;
                    MessageBox.Show("WUA Service will be removed when this application quits. To cancel it, reset settings to the default values using the button on the lower left corner.");
                }
                else
                {
                    disableWuaCheckCount = 0;
                }
            }
        }

        private void chkDisableBitsOnExit_CheckedChanged(object sender, EventArgs e)
        {
            _localNewSettings.DisableBitsServiceOnExit = chkDisableBitsOnExit.Checked;
        }

        private void chkWuaEnforceGpo_CheckedChanged(object sender, EventArgs e)
        {
            _localNewSettings.EnableWuaServicePolicyRestrictions = chkWuaEnforceGpo.Checked;
        }

        private void chkWuaSetRegistryRestrictions_CheckedChanged(object sender, EventArgs e)
        {
            _localNewSettings.EnableWuaServiceRegistryRestrictions = chkWuaSetRegistryRestrictions.Checked;
        }

        private void chkWuaSetConnectionsMetered_CheckedChanged(object sender, EventArgs e)
        {
            _localNewSettings.SetAllConnectionsToMetered = chkWuaSetConnectionsMetered.Checked;
        }

        private void cmdOpenSettings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            disableWuaCheckCount = 0;

            panelSettings.Location = Point.Empty;
            panelSettings.Dock = DockStyle.Fill;
            panelSettings.Visible = true;
            panelSettings.BringToFront();
            panelSettings.Focus();

            _localNewSettings = new PersistentSet();

            PropertyInfo[] props = typeof(PersistentSet).GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (PropertyInfo p in props)
            {
                if (p.CanRead && p.CanWrite)
                    p.SetValue(_localNewSettings, p.GetValue(Settings.Persistent)); // Obj copy
            }

            if (SedService.IsPresent)
            {
                if (UsoService.IsPresent)
                {
                    chkDisableSedOnExit.Text = @"On Exit, Disable and Prevent WU Remediation and Orchestation services from Running again";
                    chkDisableSedOnExit.Visible = true;
                    lblDisableSedReminder.Visible = true;
                    chkDisableSedOnExit.Enabled = true;
                }
                else
                {
                    chkDisableSedOnExit.Text = @"On Exit, Disable and Prevent Windows Remediation Service (SED) from Running again";
                    chkDisableSedOnExit.Visible = true;
                    lblDisableSedReminder.Visible = true;
                    chkDisableSedOnExit.Enabled = true;
                }
            }
            else if (UsoService.IsPresent)
            {
                chkDisableSedOnExit.Text = @"On Exit, Disable and Prevent Windows Update Orchestator Service from Running again";
                chkDisableSedOnExit.Visible = true;
                lblDisableSedReminder.Visible = true;
                chkDisableSedOnExit.Enabled = true;
            }
            else
            {
                chkDisableSedOnExit.Text = @"Option disabled because no WU Remediation and/or Orchestation services are present on the system.";
                chkDisableSedOnExit.Visible = false;
                lblDisableSedReminder.Visible = false;
                chkDisableSedOnExit.Enabled = false;
            }

            UpdateSettingsView();
        }

        private void cmdWuaPolicyUnblock_LinkClicked(object sender, LinkLabelLinkClickedEventArgs eventArgs)
        {
            try
            {
                PolicyBlocking blocker = new PolicyBlocking();
                blocker.Unblock();
            }
            catch (Exception e)
            {
                MessageBox.Show("Cannot unblock policy restrictions for the Windows Update service.\n" + e.Message,
                    "Cannot unblock", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            UpdateSettingsView();
        }

        private void cmdWuaRegistryBlocked_LinkClicked(object sender, LinkLabelLinkClickedEventArgs eventArgs)
        {
            try
            {
                RegistryBlocking.Unblock();
            }
            catch (Exception e)
            {
                MessageBox.Show("Cannot unblock Windows Update service from the registry.\n" + e.Message,
                    "Cannot unblock", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            UpdateSettingsView();
        }

        private void chkDisableSedOnExit_CheckedChanged(object sender, EventArgs e)
        {
            _localNewSettings.DisableSedServiceOnExit = chkDisableSedOnExit.Checked;
        }
    }
}
