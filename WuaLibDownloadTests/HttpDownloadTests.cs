﻿using System.IO;
using com.razorsoftware.wuahelper.Downloads;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WuaLibDownloadTests
{
    [TestClass]
    public class HttpDownloadTests
    {
        private const string Url = @"http://download.windowsupdate.com/c/msdownload/update/software/updt/2017/03/windows10.0-kb3150513-x64_48c5fb66ffab675af4790462a53d866203012c19.msu";
        private const string OutPath = @"X:\_tmp_wuaUnitTests\downloadedFiles\windows10.0-kb3150513-x64_48c5fb66ffab675af4790462a53d866203012c19.msu";

        [TestMethod]
        public void GetInfoFile1()
        {
            HttpDownload downloader = new HttpDownload("Test", Url, OutPath);

            bool result = downloader.GetInfo();

            Assert.IsTrue(result, "The returned status code is False.");
            downloader.Dispose();
        }

        [TestMethod]
        public void GetInfoFile2()
        {
            HttpDownload downloader = new HttpDownload("Test", Url, OutPath);

            bool result = downloader.GetInfo();

            Assert.IsTrue(result, "The returned status code is False.");

            Assert.IsTrue(downloader.Downloadable, "Not download-able");
            Assert.IsTrue(downloader.Resumable, "Not resumable");
            Assert.AreEqual(2636312, downloader.ContentLength, "Size mismatch");
            Assert.AreEqual(0, downloader.DownloadedBytes, "DownloadedBytes should be zero");
            Assert.AreEqual(downloader.ContentLength, downloader.RemainingBytes, "RemainingBytes should be the total file size");
            Assert.IsNotNull(downloader.LastResponseHeader);

            downloader.Dispose();
        }

        [TestMethod]
        public void DownloadTest1()
        {
            HttpDownload downloader = new HttpDownload("Test", Url, OutPath);

            bool result = downloader.GetInfo();

            Assert.IsTrue(result, "The returned status code is False.");

            downloader.WebTimeoutMs = 60000;
            Assert.AreEqual(downloader.ContentLength, downloader.Start(), "Downloaded size doesn't match content length");

            downloader.Dispose();
        }

        [TestMethod]
        public void DownloadChunkTest1()
        {
            HttpDownload downloader = new HttpDownload("Test", Url, OutPath + "-DowloadChunkTest1.part");

            bool result = downloader.GetInfo();

            Assert.IsTrue(result, "The returned status code is False.");

            Assert.AreEqual(0x345, downloader.Start(0x345), "Downloaded size doesn't match content length");

            downloader.Dispose();
        }

        [TestMethod]
        public void DowloadChunkedTest1()
        {
            int breakOffset = 0x345;
            FileInfo fi = new FileInfo(OutPath + "-DowloadChunkedTest1.part");
            if (fi.Exists)
                fi.Delete();
            HttpDownload downloader = new HttpDownload("Test", Url, fi.FullName);
            downloader.TruncateStream = false; // Disable stream trimming

            bool result = downloader.GetInfo();

            Assert.IsTrue(result, "The returned status code is False.");

            Assert.AreEqual(breakOffset, downloader.Resume(breakOffset, breakOffset), "Second Chunk Downloaded size doesn't match content length");
            Assert.AreEqual(breakOffset, downloader.Start(breakOffset), "First Chunk Downloaded size doesn't match content length");
            Assert.AreEqual(downloader.ContentLength - breakOffset - breakOffset, downloader.Resume(breakOffset + breakOffset), "Last Chunk Downloaded size doesn't match content length");


            downloader.Dispose();
        }
    }
}
