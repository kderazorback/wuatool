﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Windows.Forms;

namespace WuaPlaceholderTray
{
    public partial class frmMain : Form
    {
        private string RegistryKeyPath => "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
        private string RegistryKeyName => "WuaToolNotify";

        private string AutomaticRemovalScriptPath = "%TEMP%\\wuatool_remove_script.bat";

        private string AppExeName = "wuauclt.exe";

        private string LibName1 = "wuaueng.dll";

        bool startup = true;

        Queue<FileStream> LockedFiles = new Queue<FileStream>();
        bool lockError = false;

        public frmMain()
        {
            InitializeComponent();

            try
            {
                LockedFiles.Enqueue(new FileStream(System.IO.Path.Combine(Environment.ExpandEnvironmentVariables("%SystemRoot%\\system32"), LibName1), FileMode.Create, FileAccess.Write, FileShare.None));
            }
            catch (Exception ex)
            {
                //MessageBox.Show(String.Format("WUATool Cannot lock target Windows Update dependency {0}. It may be in use by Windows Update or you dont have enough access rights to modify the specified file.\n\nException: {1}", LibName1, ex.Message), "WUATool Windows Update Lock error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lockError = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            trayIcon.Visible = true;
            this.Visible = false;

            if (!IsRunningAsAdmin())
            {
                tabManual.Enabled = false;
                tabAutomaticRestore.Enabled = false;
                lblSettingRestricted1.Visible = true;
                lblSettingRestricted2.Visible = true;
            }
            else
                tabsRestore.TabPages.Remove(tabRunAsAdmin);

            Assembly assy = Assembly.GetEntryAssembly();
            string appPath = assy.Location;
            string statusString = "Unknown";

            string expectedPath = System.IO.Path.Combine(Environment.ExpandEnvironmentVariables("%SystemRoot%\\system32"), AppExeName);
            if (string.Equals(expectedPath, appPath, StringComparison.OrdinalIgnoreCase))
            {
                statusIcon.Image = GraphicalResources.checkmark;
                lblStatus.Text = "Windows Update services were replaced on this System.";
                lblDescription.Text = "This tool is running from the expected path, and is preventing Windows Update services from running.";

                statusString = "Partially blocked";

                if (!lockError)
                {
                    lblWuaPresent.Text = "WUATool is working as expected.";
                    lblWuaPresent.ForeColor = Color.DarkGreen;
                    statusString = "Blocked";
                }
                else
                    lblWuaPresent.Text = "WUATool appears to be working but failed to lock some Windows Update files.";
            }
            else
            {
                trayIcon.ShowBalloonTip(3000, "WuaTool Windows Update Blocker", "Windows Update services on this system are not currently being blocked by WuaTool. Click here for more details.", ToolTipIcon.Warning);

                if (File.Exists(expectedPath))
                {
                    lblWuaPresent.Text = "Windows Update is currently present on the system. Run WUATool again to remove it.";
                    lblWuaPresent.ForeColor = Color.DarkSalmon;
                    statusString = "Unmodified";
                }
                else
                {
                    lblWuaPresent.Text = "Windows Update is not present on the system. But a remediation service can restore this functionality.";
                    lblWuaPresent.ForeColor = Color.DarkOrange;
                    statusString = "Temporarily modified";
                }
            }

            lblFilePath.Text = expectedPath;

            lblOverallStatus.Text = String.Format("Status: {0} - Running as {1}", statusString, IsRunningAsAdmin() ? "Administrator" : "Standard user");
        }

        private void AutoRemoveWuatool()
        {
            string script = GraphicalResources.RemovalScript;
            script = script.Replace("$APPEXENAME;", AppExeName);
            script = script.Replace("$APPEXEPATH;", System.IO.Path.Combine(Environment.ExpandEnvironmentVariables("%SystemRoot%\\system32"), AppExeName));
            script = script.Replace("$APPLIB1PATH;", System.IO.Path.Combine(Environment.ExpandEnvironmentVariables("%SystemRoot%\\system32"), LibName1));

            using (FileStream fs = new FileStream(Environment.ExpandEnvironmentVariables(AutomaticRemovalScriptPath), FileMode.Create, FileAccess.Write, FileShare.None))
            {
                byte[] buffer = Encoding.ASCII.GetBytes(script);
                fs.Write(buffer, 0, buffer.Length);
            }

            RegistryKey key = Registry.LocalMachine.OpenSubKey(RegistryKeyPath, true);
            key.DeleteValue(RegistryKeyName, false);
            key.Dispose();

            while (LockedFiles.Count > 0)
                LockedFiles.Dequeue().Dispose();

            Process.Start("cmd.exe", String.Format("/C \"{0}\"", AutomaticRemovalScriptPath.Replace("\"", "")));
            Application.Exit();
        }

        private void CmdRemoveRegistrySettings_Click(object sender, EventArgs e)
        {
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(RegistryKeyPath, true);
                key.DeleteValue(RegistryKeyName, false);
                key.Dispose();

                MessageBox.Show("Registry options were deleted successfully.", "WUATool", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Cannot remove registry settings for this App.\nPlease remove the following entry manually:\tHKLM\\{0}\\{1}\n\nException: {2}", RegistryKeyPath, RegistryKeyName, ex.Message), "Cannot remove registry configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CmdCloseApp_Click(object sender, EventArgs e)
        {
            while (LockedFiles.Count > 0)
                LockedFiles.Dequeue().Dispose();

            Application.Exit();
        }

        private void CmdCopyFilePath_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lblFilePath.Text);
        }

        private void CmdCopyCommand_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lblSfcCommand.Text);
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            while (LockedFiles.Count > 0)
                LockedFiles.Dequeue().Dispose();
        }

        private void CmdAutomaticallyRestore_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("A reboot will be required to complete this operation.\nClose all applications before clicking YES\n\nAre you sure you want to continue?", "Reboot required", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
                AutoRemoveWuatool();
        }

        private void FrmMain_StyleChanged(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
                Visible = false;
        }

        private void TrayIcon_Click(object sender, EventArgs e)
        {
            Visible = true;
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void FrmMain_Shown(object sender, EventArgs e)
        {
            if (startup)
            {
                startup = false;
                Hide();
            }
        }

        private bool IsRunningAsAdmin()
        {
            WindowsPrincipal principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private void RelaunchAsAdmin()
        {
            ProcessStartInfo elevated = new ProcessStartInfo(System.Reflection.Assembly.GetEntryAssembly().Location);
            elevated.UseShellExecute = true;
            elevated.Verb = "runas";

            while (LockedFiles.Count > 0)
                LockedFiles.Dequeue().Dispose();

            Process.Start(elevated);
            Application.Exit();
        }

        private void CmdRelaunchAdmin_Click(object sender, EventArgs e)
        {
            RelaunchAsAdmin();
        }
    }
}
