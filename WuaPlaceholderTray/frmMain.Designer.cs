﻿namespace WuaPlaceholderTray
{
    partial class frmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.lblStatus = new System.Windows.Forms.Label();
            this.statusIcon = new System.Windows.Forms.PictureBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblWuaPresent = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabsRestore = new System.Windows.Forms.TabControl();
            this.tabAutomaticRestore = new System.Windows.Forms.TabPage();
            this.lblSettingRestricted1 = new System.Windows.Forms.Label();
            this.cmdAutomaticallyRestore = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tabManual = new System.Windows.Forms.TabPage();
            this.lblSettingRestricted2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmdCopyCommand = new System.Windows.Forms.Button();
            this.lblSfcCommand = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblFilePath = new System.Windows.Forms.Label();
            this.cmdCopyFilePath = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cmdCloseApp = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmdRemoveRegistrySettings = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabRunAsAdmin = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmdRelaunchAdmin = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblOverallStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.statusIcon)).BeginInit();
            this.tabsRestore.SuspendLayout();
            this.tabAutomaticRestore.SuspendLayout();
            this.tabManual.SuspendLayout();
            this.tabRunAsAdmin.SuspendLayout();
            this.SuspendLayout();
            // 
            // trayIcon
            // 
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "Windows Update is currently blocked.";
            this.trayIcon.Visible = true;
            this.trayIcon.BalloonTipClicked += new System.EventHandler(this.TrayIcon_Click);
            this.trayIcon.Click += new System.EventHandler(this.TrayIcon_Click);
            this.trayIcon.DoubleClick += new System.EventHandler(this.TrayIcon_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(76, 23);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(385, 17);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "Windows Update Services are not being blocked on this system.";
            // 
            // statusIcon
            // 
            this.statusIcon.Image = global::WuaPlaceholderTray.GraphicalResources.invalid;
            this.statusIcon.Location = new System.Drawing.Point(12, 6);
            this.statusIcon.Name = "statusIcon";
            this.statusIcon.Size = new System.Drawing.Size(56, 56);
            this.statusIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.statusIcon.TabIndex = 1;
            this.statusIcon.TabStop = false;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoEllipsis = true;
            this.lblDescription.Location = new System.Drawing.Point(12, 72);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(604, 20);
            this.lblDescription.TabIndex = 2;
            this.lblDescription.Text = "WUATool cannot block Windows Updates on this system because its not running from " +
    "the expected installation path.";
            // 
            // lblWuaPresent
            // 
            this.lblWuaPresent.AutoEllipsis = true;
            this.lblWuaPresent.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblWuaPresent.Location = new System.Drawing.Point(12, 92);
            this.lblWuaPresent.Name = "lblWuaPresent";
            this.lblWuaPresent.Size = new System.Drawing.Size(604, 20);
            this.lblWuaPresent.TabIndex = 3;
            this.lblWuaPresent.Text = "Windows Update current status is unknown. Please run WUATool to reapply the desir" +
    "ed configuration.";
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.Location = new System.Drawing.Point(12, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(604, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Use the options below if you wish to restore Windows Update functionality on this" +
    " System";
            // 
            // tabsRestore
            // 
            this.tabsRestore.Controls.Add(this.tabAutomaticRestore);
            this.tabsRestore.Controls.Add(this.tabManual);
            this.tabsRestore.Controls.Add(this.tabRunAsAdmin);
            this.tabsRestore.Location = new System.Drawing.Point(12, 162);
            this.tabsRestore.Name = "tabsRestore";
            this.tabsRestore.SelectedIndex = 0;
            this.tabsRestore.Size = new System.Drawing.Size(604, 276);
            this.tabsRestore.TabIndex = 5;
            // 
            // tabAutomaticRestore
            // 
            this.tabAutomaticRestore.Controls.Add(this.lblSettingRestricted1);
            this.tabAutomaticRestore.Controls.Add(this.cmdAutomaticallyRestore);
            this.tabAutomaticRestore.Controls.Add(this.label8);
            this.tabAutomaticRestore.Location = new System.Drawing.Point(4, 22);
            this.tabAutomaticRestore.Name = "tabAutomaticRestore";
            this.tabAutomaticRestore.Padding = new System.Windows.Forms.Padding(3);
            this.tabAutomaticRestore.Size = new System.Drawing.Size(596, 250);
            this.tabAutomaticRestore.TabIndex = 0;
            this.tabAutomaticRestore.Text = "Automatic";
            this.tabAutomaticRestore.UseVisualStyleBackColor = true;
            // 
            // lblSettingRestricted1
            // 
            this.lblSettingRestricted1.AutoSize = true;
            this.lblSettingRestricted1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSettingRestricted1.ForeColor = System.Drawing.Color.Maroon;
            this.lblSettingRestricted1.Location = new System.Drawing.Point(123, 222);
            this.lblSettingRestricted1.Name = "lblSettingRestricted1";
            this.lblSettingRestricted1.Size = new System.Drawing.Size(350, 15);
            this.lblSettingRestricted1.TabIndex = 12;
            this.lblSettingRestricted1.Text = "* You need to run this tool as Administrator to make changes *";
            this.lblSettingRestricted1.Visible = false;
            // 
            // cmdAutomaticallyRestore
            // 
            this.cmdAutomaticallyRestore.Location = new System.Drawing.Point(198, 175);
            this.cmdAutomaticallyRestore.Name = "cmdAutomaticallyRestore";
            this.cmdAutomaticallyRestore.Size = new System.Drawing.Size(201, 37);
            this.cmdAutomaticallyRestore.TabIndex = 1;
            this.cmdAutomaticallyRestore.Text = "Automatically try to restore Windows Update functionality";
            this.cmdAutomaticallyRestore.UseVisualStyleBackColor = true;
            this.cmdAutomaticallyRestore.Click += new System.EventHandler(this.CmdAutomaticallyRestore_Click);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(25, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(548, 48);
            this.label8.TabIndex = 0;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // tabManual
            // 
            this.tabManual.Controls.Add(this.lblSettingRestricted2);
            this.tabManual.Controls.Add(this.label6);
            this.tabManual.Controls.Add(this.cmdCopyCommand);
            this.tabManual.Controls.Add(this.lblSfcCommand);
            this.tabManual.Controls.Add(this.label5);
            this.tabManual.Controls.Add(this.lblFilePath);
            this.tabManual.Controls.Add(this.cmdCopyFilePath);
            this.tabManual.Controls.Add(this.label4);
            this.tabManual.Controls.Add(this.cmdCloseApp);
            this.tabManual.Controls.Add(this.label3);
            this.tabManual.Controls.Add(this.cmdRemoveRegistrySettings);
            this.tabManual.Controls.Add(this.label2);
            this.tabManual.Location = new System.Drawing.Point(4, 22);
            this.tabManual.Name = "tabManual";
            this.tabManual.Padding = new System.Windows.Forms.Padding(3);
            this.tabManual.Size = new System.Drawing.Size(596, 250);
            this.tabManual.TabIndex = 1;
            this.tabManual.Text = "Manual";
            this.tabManual.UseVisualStyleBackColor = true;
            // 
            // lblSettingRestricted2
            // 
            this.lblSettingRestricted2.AutoSize = true;
            this.lblSettingRestricted2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSettingRestricted2.ForeColor = System.Drawing.Color.Maroon;
            this.lblSettingRestricted2.Location = new System.Drawing.Point(123, 222);
            this.lblSettingRestricted2.Name = "lblSettingRestricted2";
            this.lblSettingRestricted2.Size = new System.Drawing.Size(350, 15);
            this.lblSettingRestricted2.TabIndex = 11;
            this.lblSettingRestricted2.Text = "* You need to run this tool as Administrator to make changes *";
            this.lblSettingRestricted2.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "5. Reboot";
            // 
            // cmdCopyCommand
            // 
            this.cmdCopyCommand.Location = new System.Drawing.Point(463, 132);
            this.cmdCopyCommand.Name = "cmdCopyCommand";
            this.cmdCopyCommand.Size = new System.Drawing.Size(116, 23);
            this.cmdCopyCommand.TabIndex = 9;
            this.cmdCopyCommand.Text = "Copy command";
            this.cmdCopyCommand.UseVisualStyleBackColor = true;
            this.cmdCopyCommand.Click += new System.EventHandler(this.CmdCopyCommand_Click);
            // 
            // lblSfcCommand
            // 
            this.lblSfcCommand.AutoEllipsis = true;
            this.lblSfcCommand.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSfcCommand.Location = new System.Drawing.Point(35, 162);
            this.lblSfcCommand.Name = "lblSfcCommand";
            this.lblSfcCommand.Size = new System.Drawing.Size(544, 13);
            this.lblSfcCommand.TabIndex = 8;
            this.lblSfcCommand.Text = "sfc /scannow";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(402, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "4. Run the following command from a command line window as administrator";
            // 
            // lblFilePath
            // 
            this.lblFilePath.AutoEllipsis = true;
            this.lblFilePath.Font = new System.Drawing.Font("Segoe UI", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblFilePath.Location = new System.Drawing.Point(35, 116);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(544, 13);
            this.lblFilePath.TabIndex = 6;
            this.lblFilePath.Text = "<...>";
            // 
            // cmdCopyFilePath
            // 
            this.cmdCopyFilePath.Location = new System.Drawing.Point(463, 89);
            this.cmdCopyFilePath.Name = "cmdCopyFilePath";
            this.cmdCopyFilePath.Size = new System.Drawing.Size(116, 23);
            this.cmdCopyFilePath.TabIndex = 5;
            this.cmdCopyFilePath.Text = "Copy path";
            this.cmdCopyFilePath.UseVisualStyleBackColor = true;
            this.cmdCopyFilePath.Click += new System.EventHandler(this.CmdCopyFilePath_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "3. Delete the following file:";
            // 
            // cmdCloseApp
            // 
            this.cmdCloseApp.Location = new System.Drawing.Point(463, 57);
            this.cmdCloseApp.Name = "cmdCloseApp";
            this.cmdCloseApp.Size = new System.Drawing.Size(116, 23);
            this.cmdCloseApp.TabIndex = 3;
            this.cmdCloseApp.Text = "Close";
            this.cmdCloseApp.UseVisualStyleBackColor = true;
            this.cmdCloseApp.Click += new System.EventHandler(this.CmdCloseApp_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(387, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "2. Close this app by clicking the X on the toolbar or by pressing this button";
            // 
            // cmdRemoveRegistrySettings
            // 
            this.cmdRemoveRegistrySettings.Location = new System.Drawing.Point(463, 27);
            this.cmdRemoveRegistrySettings.Name = "cmdRemoveRegistrySettings";
            this.cmdRemoveRegistrySettings.Size = new System.Drawing.Size(116, 23);
            this.cmdRemoveRegistrySettings.TabIndex = 1;
            this.cmdRemoveRegistrySettings.Text = "Remove";
            this.cmdRemoveRegistrySettings.UseVisualStyleBackColor = true;
            this.cmdRemoveRegistrySettings.Click += new System.EventHandler(this.CmdRemoveRegistrySettings_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(439, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "1. Click the following button to remove WUATool AutoStart options from the Regist" +
    "ry";
            // 
            // tabRunAsAdmin
            // 
            this.tabRunAsAdmin.Controls.Add(this.label12);
            this.tabRunAsAdmin.Controls.Add(this.label11);
            this.tabRunAsAdmin.Controls.Add(this.cmdRelaunchAdmin);
            this.tabRunAsAdmin.Location = new System.Drawing.Point(4, 22);
            this.tabRunAsAdmin.Name = "tabRunAsAdmin";
            this.tabRunAsAdmin.Padding = new System.Windows.Forms.Padding(3);
            this.tabRunAsAdmin.Size = new System.Drawing.Size(596, 250);
            this.tabRunAsAdmin.TabIndex = 2;
            this.tabRunAsAdmin.Text = "Run as Administrator";
            this.tabRunAsAdmin.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(101, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(394, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Press the following button to relaunch this tool with Administrator privileges";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(73, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(450, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "You need to run this tool as Administrator in order to make changes to its config" +
    "uration";
            // 
            // cmdRelaunchAdmin
            // 
            this.cmdRelaunchAdmin.Location = new System.Drawing.Point(172, 156);
            this.cmdRelaunchAdmin.Name = "cmdRelaunchAdmin";
            this.cmdRelaunchAdmin.Size = new System.Drawing.Size(252, 42);
            this.cmdRelaunchAdmin.TabIndex = 0;
            this.cmdRelaunchAdmin.Text = "Relaunch with Administrator privileges";
            this.cmdRelaunchAdmin.UseVisualStyleBackColor = true;
            this.cmdRelaunchAdmin.Click += new System.EventHandler(this.CmdRelaunchAdmin_Click);
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.Location = new System.Drawing.Point(12, 119);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(604, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "This tool will always minimize to Tray. If closed, the app will not prevent WU se" +
    "rvices from being restored.";
            // 
            // lblOverallStatus
            // 
            this.lblOverallStatus.AutoSize = true;
            this.lblOverallStatus.ForeColor = System.Drawing.Color.Silver;
            this.lblOverallStatus.Location = new System.Drawing.Point(12, 445);
            this.lblOverallStatus.Name = "lblOverallStatus";
            this.lblOverallStatus.Size = new System.Drawing.Size(53, 13);
            this.lblOverallStatus.TabIndex = 7;
            this.lblOverallStatus.Text = "<status>";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(628, 464);
            this.Controls.Add(this.lblOverallStatus);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tabsRestore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblWuaPresent);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.statusIcon);
            this.Controls.Add(this.lblStatus);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "WUATool Windows Update Blocker";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.FrmMain_Shown);
            this.Resize += new System.EventHandler(this.FrmMain_StyleChanged);
            this.StyleChanged += new System.EventHandler(this.FrmMain_StyleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.statusIcon)).EndInit();
            this.tabsRestore.ResumeLayout(false);
            this.tabAutomaticRestore.ResumeLayout(false);
            this.tabAutomaticRestore.PerformLayout();
            this.tabManual.ResumeLayout(false);
            this.tabManual.PerformLayout();
            this.tabRunAsAdmin.ResumeLayout(false);
            this.tabRunAsAdmin.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.PictureBox statusIcon;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblWuaPresent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabsRestore;
        private System.Windows.Forms.TabPage tabAutomaticRestore;
        private System.Windows.Forms.TabPage tabManual;
        private System.Windows.Forms.Button cmdRemoveRegistrySettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cmdCloseApp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.Button cmdCopyFilePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button cmdCopyCommand;
        private System.Windows.Forms.Label lblSfcCommand;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button cmdAutomaticallyRestore;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblSettingRestricted1;
        private System.Windows.Forms.Label lblSettingRestricted2;
        private System.Windows.Forms.TabPage tabRunAsAdmin;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button cmdRelaunchAdmin;
        private System.Windows.Forms.Label lblOverallStatus;
    }
}

