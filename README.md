# WUATool #

An small tool for managing Windows Update Agent (WUA) and its downloads, and also allows downloading updates for offline use and manual installation, prevent auto-updates, and more.

## Getting Started ##

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.  
See deployment for notes on how to deploy the project on a live system.

### Prerequisites ###

* Latest WUA version (_or at least a version that are still allowed to connect to Microsoft servers_) See: [KB949104](http://support.microsoft.com/kb/949104) for details
* .NET framework 4.5
* This development build is currently tested only on these builds: 
    * Windows 10 Version 1903 OS Build 18362
    * -- All Windows 10 versions in-between --
    * Windows 10 Version 1511 OS Build 10586
    * Windows 8.1 Build 9600
    * Windows 7 Build 7601
    * _More builds to come_
    _This application was not tested on every build, but can work theoretically as long as you have a working WUA and .NET Framework_
* An internet connection

### Installing ###

_For development and testing_

1. Install and setup Visual Studio (2015 or newer recommended).
2. Checkout the latest GIT source from
> https://bitbucket.org/kderazorback/wuatool.git
3. Copy the contents of
> .\PublicResources\
to
> .\wuatool\Resources  
_You can use any other set of resources you like by copying those files instead to the **Resources** directory_  
4. Open the solution file
> .\wuatool.sln
_Remember this application requires Administrator Rights to run, therefore, to debug under Visual Studio, it must be opened as Administrator_


## Deployment ##

_To a live system_

1. Download any BETA release from the [Downloads](https://bitbucket.org/kderazorback/wuatool/downloads/) page and extract it to an empty directory
2. Run Wuatool.exe with Administrator privileges


## Authors ##

* Fabian R - _main developer_ - [kderazorback][contactAdmin].


## License ##

This project is licensed under the MIT License - see the [LICENSE.md][licenseMd] file for details


[contactAdmin]: https://bitbucket.org/kderazorback/
[licenseMd]: https://bitbucket.org/kderazorback/wuatool/src/master/LICENSE.md