using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceProcess;
using Microsoft.Win32;

namespace com.razorsoftware.wuahelper.Services
{
    /// <summary>
    /// Provides additional functionality for managing any installed Service process on the System
    /// </summary>
    public class ServiceBase : IDisposable
    {
        // Constants
        public const string PathPatchToken = ";?[WUATOOL]";


        // Static Properties
        /// <summary>
        /// Returns the location on the Windows Registry where Service information is stored
        /// </summary>
        public static string ServiceHiveRegistryPath
        {
            get { return "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\"; }
        }
        /// <summary>
        /// Returns the Key where a Service start mode is specified inside the <see cref="ServiceHiveRegistryPath"/> location.
        /// </summary>
        public static string ServiceStartKey
        {
            get { return "Start"; }
        }


        // Constructor
        /// <summary>
        /// Creates a new Instance of the ServiceBase class with the specified Service Name
        /// </summary>
        /// <param name="serviceName">Name of the service that will be handled by this instance</param>
        protected ServiceBase(string serviceName)
        {
            BackingServiceName = serviceName;
            Service = new ServiceController(serviceName);
        }


        // Instance Fields
        protected string BackingServiceName;


        // Instance Properties
        /// <summary>
        /// Stores the service name for the current instance
        /// </summary>
        public virtual string ServiceName { get { return BackingServiceName; } }
        /// <summary>
        /// Stores the ServiceController instance used by this Class
        /// </summary>
        public ServiceController Service { get; protected set; }
        /// <summary>
        /// Returns the current status of the Service (auto-updated on call)
        /// </summary>
        public ServiceControllerStatus Status
        {
            get
            {
                Service.Refresh();
                return Service.Status;
            }
        }
        /// <summary>
        /// Indicates if the Service will be automatically stopped when this instance is Disposed
        /// </summary>
        public bool AutoStop { get; set; } = false;

        /// <summary>
        /// Starts the Service and waits until its fully initialized
        /// </summary>
        public void Start()
        {
            switch (Service.Status)
            {
                case ServiceControllerStatus.Paused:
                case ServiceControllerStatus.PausePending:
                    Service.WaitForStatus(ServiceControllerStatus.Paused);
                    Service.Continue();
                    break;
                case ServiceControllerStatus.StartPending:
                case ServiceControllerStatus.Running:
                case ServiceControllerStatus.ContinuePending:
                    break;
                case ServiceControllerStatus.Stopped:
                    Service.Start();
                    break;
                case ServiceControllerStatus.StopPending:
                    Service.WaitForStatus(ServiceControllerStatus.Stopped);
                    Service.Start();
                    break;
            }

            // Wait until the service is running
            Service.Refresh();
            Service.WaitForStatus(ServiceControllerStatus.Running);
            Service.Refresh();
        }

        /// <summary>
        /// Stops the Service if running
        /// </summary>
        public void Stop()
        {
            switch (Service.Status)
            {
                case ServiceControllerStatus.Paused:
                case ServiceControllerStatus.PausePending:
                    Service.WaitForStatus(ServiceControllerStatus.Paused);
                    Service.Continue();
                    Service.WaitForStatus(ServiceControllerStatus.Running);
                    Service.Stop();
                    break;
                case ServiceControllerStatus.StartPending:
                case ServiceControllerStatus.Running:
                case ServiceControllerStatus.ContinuePending:
                    Service.WaitForStatus(ServiceControllerStatus.Running);
                    Service.Stop();
                    break;
                case ServiceControllerStatus.Stopped:
                    break;
                case ServiceControllerStatus.StopPending:
                    Service.WaitForStatus(ServiceControllerStatus.Stopped);
                    break;
            }

            Service.Refresh();
            Service.WaitForStatus(ServiceControllerStatus.Stopped);
            Service.Refresh();
        }

        public void Dispose()
        {
            if (AutoStop)
            {
                // Shutdown the service
                Service.Refresh();
                try
                {
                    Service.Stop();
                }
                catch (Exception)
                {
                    // So Nasty much code
                }
            }

            Service.Dispose();
            Service = null;
        }

        public bool IsPathPatched
        {
            get
            {
                object dllName = Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName + "\\Parameters", "ServiceDll", null);
                dllName = dllName ?? Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName,
                    "ImagePath", null);

                if (dllName == null) return false;

                return dllName.ToString().Trim().StartsWith(PathPatchToken);
            }
        }

        public void ApplyPathPatch()
        {
            object dllName = Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName + "\\Parameters", "ServiceDll", null);
            string value;
            if (dllName != null)
            {
                value = dllName.ToString().Trim();
                if (value.StartsWith(PathPatchToken)) return; // Already patched
                if (string.IsNullOrWhiteSpace(value))
                {
                    Debug.Print("Service {0} appears to have a ServiceDll associated to it but the key-value is set to null.", ServiceName);   
                    // Fallback to ImagePath patching
                }
                else
                {
                    // Apply ServiceDll Patch
                    Registry.SetValue(
                        "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName + "\\Parameters",
                        "ServiceDll", PathPatchToken + value, RegistryValueKind.ExpandString);

                    return; // Skip ImagePath patching
                }
            }

            // Fallback to ImagePath patch
            dllName = Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName,
                "ImagePath", null);
            value = dllName.ToString().Trim();
            if (value.StartsWith(PathPatchToken)) return; // Already patched
            if (string.IsNullOrWhiteSpace(value))
                throw new KeyNotFoundException(
                    "The target service appears to be damaged since no ImagePath is defined.");

            Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName,
                "ImagePath", PathPatchToken + value, RegistryValueKind.ExpandString);
        }

        public void RemovePathPatch()
        {
            object dllName = Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName + "\\Parameters", "ServiceDll", null);
            string value;
            if (dllName != null)
            {
                value = dllName.ToString().Trim();
                if (value.StartsWith(PathPatchToken))
                {
                    // Remove ServiceDll patch
                    Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName + "\\Parameters",
                        "ServiceDll", value.Substring(PathPatchToken.Length), RegistryValueKind.ExpandString);
                }
            }

            dllName = Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName,
                "ImagePath", null);
            value = dllName.ToString().Trim();
            if (value.StartsWith(PathPatchToken))
            {
                // Remove ImagePath patch
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + ServiceName,
                    "ImagePath", value.Substring(PathPatchToken.Length), RegistryValueKind.ExpandString);
            }
        }


        // Static Methods
        /// <summary>
        /// Queries the Windows Service Database and returns the Start Mode value for the specified Service name
        /// </summary>
        /// <param name="serviceName">Name of the Service to Query</param>
        /// <returns>A <see cref="ServiceStartMode"/> value for the specified Service name</returns>
        public static ServiceStartMode GetServiceStartMode(string serviceName)
        {
            object objStartType =
                Registry.GetValue(ServiceHiveRegistryPath + serviceName, ServiceStartKey, null);

            int iStartType;
            if (objStartType is int)
                iStartType = (int)objStartType;
            else
                if (!int.TryParse(objStartType.ToString(), out iStartType))
                return ServiceStartMode.Disabled;

            switch (iStartType)
            {
                case 0: // Loaded by boot loader
                case 1: // Loaded by kernel init
                case 2: // Automatic
                    return ServiceStartMode.Automatic;
                case 3: // Manual
                    return ServiceStartMode.Manual;
                case 4: // Disabled
                default:
                    return ServiceStartMode.Disabled;
            }
        }

        /// <summary>
        /// Modifies the Windows Service Database and sets the Service Start Mode value for the specified Service name to the desired one
        /// </summary>
        /// <param name="serviceName">Name of the Service to modify</param>
        /// <param name="newMode">New Service Start mode for the Service</param>
        /// <returns>True if the call succeeds, otherwise false</returns>
        /// <remarks>This method waits one seconds after updating the Windows Service Database and then checks the updated value of the Service Start Mode. If everything matches, then True is returned.
        /// If the queried value differs from the one specified in the parameters, false will be returned instead.</remarks>
        public static bool SetServiceStartMode(string serviceName, ServiceStartMode newMode)
        {
            return SetServiceStartModeByPInvoke(serviceName, newMode);
        }

        /// <summary>
        /// Sets the current Service Start Mode for the specified Service name to the desired value by using a P/Invoke call to the System
        /// </summary>
        /// <param name="serviceName">Name of the Service to modify</param>
        /// <param name="newMode">Desired Service Start Mode</param>
        /// <returns>True if the operation succeeds, otherwise false</returns>
        private static bool SetServiceStartModeByPInvoke(string serviceName, ServiceStartMode newMode)
        {
            const uint SERVICE_NO_CHANGE = 0xFFFFFFFF;

            uint newValue;
            switch (newMode)
            {
                case ServiceStartMode.Automatic:
                    newValue = 0x02; // Automatic
                    break;
                case ServiceStartMode.Disabled:
                    newValue = 0x04; // Disabled
                    break;
                case ServiceStartMode.Manual:
                default:
                    newValue = 0x03; // Manual
                    break;
            }

            try
            {
                ServiceController controller = new ServiceController(serviceName);
                NativeMethods.ChangeServiceConfig(controller.ServiceHandle.DangerousGetHandle(), SERVICE_NO_CHANGE, newValue, SERVICE_NO_CHANGE, null, null, IntPtr.Zero, null, null, null, null);
            }
            catch (Exception)
            {
                return false;
            }

            System.Threading.Thread.Sleep(1000);

            return (GetServiceStartMode(serviceName) == newMode);
        }

        /// <summary>
        /// Modifies the Windows Registry hive and sets the Windows Update Agent service Start Mode value to the desired one
        /// </summary>
        /// <param name="serviceName">Name of the Service to modify</param>
        /// <param name="newMode">New Service Start mode for the specified Service name</param>
        /// <returns>True if the call succeeds, otherwise false</returns>
        /// <remarks>This method waits one seconds after updating the Windows Registry and then checks the updated value of the Service Start Mode. If everything matches, then True is returned.
        /// If the queried value differs from the one specified in the parameters, false will be returned instead.</remarks>
        private static bool SetServiceStartModeByReg(string serviceName, ServiceStartMode newMode)
        {
            int newValue;
            switch (newMode)
            {
                case ServiceStartMode.Automatic:
                    newValue = 0x02; // Automatic
                    break;
                case ServiceStartMode.Disabled:
                    newValue = 0x04; // Disabled
                    break;
                case ServiceStartMode.Manual:
                default:
                    newValue = 0x03; // Manual
                    break;
            }

            try
            {
                Registry.SetValue(ServiceHiveRegistryPath + serviceName, ServiceStartKey, newValue);
            }
            catch (Exception)
            {
                return false;
            }

            System.Threading.Thread.Sleep(1000);

            return (GetServiceStartMode(serviceName) == newMode);
        }
        /// <summary>
        /// Returns a value indicating if a Service with the specified name is installed on the system
        /// </summary>
        /// <param name="serviceName">Service Name to query from the Windows Service Database</param>
        /// <returns>True if a Service with the specified name is installed on the System, otherwise False</returns>
        public static bool Exists(string serviceName)
        {
            try
            {
                ServiceController service = new ServiceController(serviceName);
                service.Refresh(); // Prevent the C# compiler from omitting the previous line when optimizing the code
                return service.Status >= 0 || service.Status < 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns a value indicating if the specified Service is currently present on the System
        /// </summary>
        public bool IsPresent
        {
            get
            {
                if (string.IsNullOrWhiteSpace(BackingServiceName)) return false;

                return ServiceBase.Exists(BackingServiceName);
            }
        }
    }
}