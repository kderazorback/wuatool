﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuahelper.Services
{
    /// <summary>
    /// Manages the Windows Update Orchestation Service (USO) present on later versions of Windows 10
    /// </summary>
    public class UsoService : ServiceBase
    {
        // Static Properties
        /// <summary>
        /// Returns the Windows Update Orchestation Service (USO) Service name on the current platform
        /// </summary>
        public static string PlatformSpecificServiceName
        {
            get { return "usosvc"; }
        }
        /// <summary>
        /// Returns a value indicating if the Windows Update Orchestation Service (USO) Service is currently present on the System
        /// </summary>
        public static new bool IsPresent
        {
            get
            {
                if (string.IsNullOrWhiteSpace(PlatformSpecificServiceName)) return false;

                return ServiceBase.Exists(PlatformSpecificServiceName);
            }
        }

        // Constructors
        /// <summary>
        /// Creates a new Instance of this class that can be used to manage the Windows Update Orchestation Service (USO) Service
        /// </summary>
        public UsoService() : base(PlatformSpecificServiceName) { }

        // Static methods
        /// <summary>
        /// Queries the Windows Service Database and returns the Start Mode value for the Windows Update Orchestation Service (USO) Service
        /// </summary>
        /// <returns>A <see cref="ServiceStartMode"/> value for the Windows Update Orchestation Service (USO) Service</returns>
        public static ServiceStartMode GetServiceStartMode()
        {
            return ServiceBase.GetServiceStartMode(PlatformSpecificServiceName);
        }

        /// <summary>
        /// Modifies the Windows Service Database and sets the Service Start Mode value for the Windows Update Orchestation Service (USO) Service to the desired one
        /// </summary>
        /// <param name="newMode">New Service Start mode for the Windows Update Orchestation Service (USO) Service</param>
        /// <returns>True if the call succeeds, otherwise false</returns>
        /// <remarks>This method waits one seconds after updating the Windows Service Database and then checks the updated value of the Service Start Mode. If everything matches, then True is returned.
        /// If the queried value differs from the one specified in the parameters, false will be returned instead.</remarks>
        public static bool SetServiceStartMode(ServiceStartMode newMode)
        {
            return SetServiceStartMode(PlatformSpecificServiceName, newMode);
        }
    }
}
