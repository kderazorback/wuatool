﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuahelper.Services
{
    public class CustomService : ServiceBase
    {
        public CustomService(string identifier) : base(identifier)
        {
            BackingServiceName = identifier;
        }
    }
}
