﻿using System.ServiceProcess;

namespace com.razorsoftware.wuahelper.Services
{
    /// <summary>
    /// Manages the Background Intelligent Transfer Service (BITS) Service
    /// </summary>
    public class BitsService : ServiceBase
    {
        // Static Properties
        /// <summary>
        /// Returns the Background Intelligent Transfer Service (BITS) Service name on the current platform
        /// </summary>
        public static string PlatformSpecificServiceName
        {
            get { return "BITS"; }
        }
        /// <summary>
        /// Returns a value indicating if the Background Intelligent Transfer Service (BITS) Service is currently present on the System
        /// </summary>
        public static new bool IsPresent
        {
            get
            {
                if (string.IsNullOrWhiteSpace(PlatformSpecificServiceName)) return false;

                return ServiceBase.Exists(PlatformSpecificServiceName);
            }
        }

        // Constructors
        /// <summary>
        /// Creates a new Instance of this class that can be used to manage the Background Intelligent Transfer Service (BITS) Service
        /// </summary>
        public BitsService() : base(PlatformSpecificServiceName) { }

        // Static methods
        /// <summary>
        /// Queries the Windows Service Database and returns the Start Mode value for the Background Intelligent Transfer Service (BITS) Service
        /// </summary>
        /// <returns>A <see cref="ServiceStartMode"/> value for the Background Intelligent Transfer Service (BITS) Service</returns>
        public static ServiceStartMode GetServiceStartMode()
        {
            return ServiceBase.GetServiceStartMode(PlatformSpecificServiceName);
        }

        /// <summary>
        /// Modifies the Windows Service Database and sets the Service Start Mode value for the Background Intelligent Transfer Service (BITS) Service to the desired one
        /// </summary>
        /// <param name="newMode">New Service Start mode for the Background Intelligent Transfer Service (BITS) Service</param>
        /// <returns>True if the call succeeds, otherwise false</returns>
        /// <remarks>This method waits one seconds after updating the Windows Service Database and then checks the updated value of the Service Start Mode. If everything matches, then True is returned.
        /// If the queried value differs from the one specified in the parameters, false will be returned instead.</remarks>
        public static bool SetServiceStartMode(ServiceStartMode newMode)
        {
            return SetServiceStartMode(PlatformSpecificServiceName, newMode);
        }
    }
}
