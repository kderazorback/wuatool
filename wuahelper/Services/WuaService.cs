﻿using System;
using System.ServiceProcess;
using Microsoft.Win32;

namespace com.razorsoftware.wuahelper.Services
{
    /// <summary>
    /// Manages the Windows Update Agent (WUA) Service
    /// </summary>
    public class WuaService : ServiceBase
    {
        // Static Properties
        /// <summary>
        /// Returns the Windows Update Agent service name on the current platform
        /// </summary>
        public static string PlatformSpecificServiceName
        {
            get { return "wuauserv"; }
        }
        /// <summary>
        /// Returns the expected Windows Update Agent core Dll name on the current platform.
        /// The string resulting from this property is not based on any Query, it is just a cherry pick based on the current platform
        /// </summary>
        /// <remarks>This property is set as Protected for your own safety</remarks>
        protected static string ExpectedPlatformSpecificDllName
        {
            get { return "wuaueng.dll"; }
        }

        /// <summary>
        /// Returns a value indicating if the Windows Update Agent (WUA) Service is currently present on the System
        /// </summary>
        public static new bool IsPresent
        {
            get
            {
                if (string.IsNullOrWhiteSpace(PlatformSpecificServiceName)) return false;

                return ServiceBase.Exists(PlatformSpecificServiceName);
            }
        }


        // Constructors
        /// <summary>
        /// Creates a new Instance of this class that can be used to manage the Windows Update Agent (WUA) Service
        /// </summary>
        public WuaService() : base(PlatformSpecificServiceName) { }


        // Static methods
        /// <summary>
        /// Starts the Windows Update service and waits for its fully initialization
        /// </summary>
        /// <returns>A new instance of this class that can be used to manage an already started instance of the Windows Update Service</returns>
        public static WuaService StartNew()
        {
            WuaService instance = new WuaService();

            instance.Start();

            return instance;
        }

        /// <summary>
        /// Queries the Windows Service Database and returns the Start Mode value for the Windows Update Agent (WUA) Service
        /// </summary>
        /// <returns>A <see cref="ServiceStartMode"/> value for the Windows Update Agent (WUA) Service</returns>
        public static ServiceStartMode GetServiceStartMode()
        {
            return ServiceBase.GetServiceStartMode(PlatformSpecificServiceName);
        }

        /// <summary>
        /// Modifies the Windows Service Database and sets the Service Start Mode value for the Windows Update Agent (WUA) Service to the desired one
        /// </summary>
        /// <param name="newMode">New Service Start mode for the Windows Update Agent (WUA) Service</param>
        /// <returns>True if the call succeeds, otherwise false</returns>
        /// <remarks>This method waits one seconds after updating the Windows Service Database and then checks the updated value of the Service Start Mode. If everything matches, then True is returned.
        /// If the queried value differs from the one specified in the parameters, false will be returned instead.</remarks>
        public static bool SetServiceStartMode(ServiceStartMode newMode)
        {
            return SetServiceStartMode(PlatformSpecificServiceName, newMode);
        }

        /// <summary>
        /// Returns the full filename where the Windows Update Agent core Dll is located. This value is used by the Service host component to load the Windows Update Agent.
        /// </summary>
        /// <returns>An expanded path to the core Dll of the Windows Update Agent</returns>
        public static string GetWuaBinaryPath()
        {
            string dllName = GetWuaBinaryPathByScQuery();

            if (string.IsNullOrWhiteSpace(dllName))
                dllName = GetWuaBinaryPathHardcoded();

            return dllName;
        }

        /// <summary>
        /// Returns the Windows Update Agent core Dll name by looking at its service information on the Windows Registry Hive.
        /// </summary>
        /// <returns>An expanded path to the core Dll of the Windows Update Agent</returns>
        private static string GetWuaBinaryPathByScQuery()
        {
            object dllName = Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + PlatformSpecificServiceName + "\\Parameters", "ServiceDll", null);

            if (dllName == null) return null;

            string strDllName = dllName as string;

            if (string.IsNullOrWhiteSpace(strDllName))
                return null;

            return Environment.ExpandEnvironmentVariables(strDllName);
        }

        /// <summary>
        /// Returns the Windows Update Agent core Dll name by using hardcoded values.
        /// This value may not work on all platforms.
        /// </summary>
        /// <returns>An expanded path to the core Dll of the Windows Update Agent</returns>
        private static string GetWuaBinaryPathHardcoded()
        {
            return Environment.ExpandEnvironmentVariables("%systemroot%\\system32\\" + ExpectedPlatformSpecificDllName);
        }

        /// <summary>
        /// (Dangerous) Sets the windows Update Agent core Dll filename stored in the Windows Registry Hive to the specified value.
        /// The result of this operation can damage the system.
        /// </summary>
        /// <remarks><para>These path is used by the Windows Service Host (svchost.exe) to locate, launch and host the Windows Update Agent (WUA) main component.
        /// If the stored value gets modified, Windows Updates will not operate correctly anymore. It is recommended to not use this call at all.
        /// If this method is called with its parameter set to null, the method will try to restore the default value on the Windows Registry Hive
        /// that points to where the Windows Update Agent core dll is located in a typical environment (eg: %systemroot%\\system32\\wuaueng.dll).</para>
        /// If an empty string is specified as parameter, the Windows Service host will not have any dll to launch, therefore the Windows Update Agent (WUA) Service will be disabled.</remarks>
        public static void SetWuaBinaryPath(string newPath)
        {
            if (newPath == null)
                newPath = "%systemroot%\\system32\\" + ExpectedPlatformSpecificDllName;

            Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + PlatformSpecificServiceName + "\\Parameters", "ServiceDll", newPath, RegistryValueKind.ExpandString);
        }
    }
}
