﻿using WUApiLib;

namespace com.razorsoftware.wuahelper.WuaCom
{
    internal class WuaTaskMask : ISearchCompletedCallback, IDownloadProgressChangedCallback, IDownloadCompletedCallback, IInstallationProgressChangedCallback, IInstallationCompletedCallback
    {
        // Instance Fields
        protected readonly WuaTask Parent;

        // Constructor
        public WuaTaskMask(WuaTask parent)
        {
            Parent = parent;
        }


        // Interface methods
        void ISearchCompletedCallback.Invoke(ISearchJob searchJob, ISearchCompletedCallbackArgs callbackArgs)
        {
            Parent.SearchCompletedCallbackInvoke(searchJob, callbackArgs);
        }

        void IDownloadProgressChangedCallback.Invoke(IDownloadJob downloadJob,
            IDownloadProgressChangedCallbackArgs callbackArgs)
        {
            Parent.DownloadProgressChangedCallbackInvoke(downloadJob, callbackArgs);
        }

        void IDownloadCompletedCallback.Invoke(IDownloadJob downloadJob, IDownloadCompletedCallbackArgs callbackArgs)
        {
            Parent.DownloadCompletedCallbackInvoke(downloadJob, callbackArgs);
        }

        void IInstallationProgressChangedCallback.Invoke(IInstallationJob installationJob,
            IInstallationProgressChangedCallbackArgs callbackArgs)
        {
            Parent.InstallationProgressChangedCallbackInvoke(installationJob, callbackArgs);
        }

        void IInstallationCompletedCallback.Invoke(IInstallationJob installationJob,
            IInstallationCompletedCallbackArgs callbackArgs)
        {
            Parent.InstallationCompletedCallbackInvoke(installationJob, callbackArgs);
        }
    }
}
