﻿using System.Collections.Generic;
using WUApiLib;

namespace com.razorsoftware.wuahelper.WuaCom
{
    public class WuaInstallSessionResult
    {
        /// <summary>
        /// Stores the updates that tried to be Installed
        /// </summary>
        public WuaUpdateWrapper[] Updates { get; protected set; }
        /// <summary>
        /// Stores the Results produced by Windows Update while downloading the specified Updated
        /// </summary>
        public XmlTypeSerializer DownloadResults { get; protected set; }
        /// <summary>
        /// Stores the Results produced by Windows Update while installing the specified Updates
        /// </summary>
        public XmlTypeSerializer InstallResults { get; protected set; }

        internal WuaInstallSessionResult(UpdateCollectionClass updates, IDownloadResult downloadResults,
            IInstallationResult installResults)
        {
            List<WuaUpdateWrapper> wrappedUpdates = new List<WuaUpdateWrapper>();
            foreach (IUpdate update in updates)
                wrappedUpdates.Add(WuaUpdateWrapper.Wrap(update));

            Updates = wrappedUpdates.ToArray();

            DownloadResults = XmlTypeSerializer.WrapAround(downloadResults, typeof(IDownloadResult));
            InstallResults = XmlTypeSerializer.WrapAround(installResults, typeof(IInstallationResult));
        }
    }
}
