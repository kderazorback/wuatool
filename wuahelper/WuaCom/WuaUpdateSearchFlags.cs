﻿using System;

namespace com.razorsoftware.wuahelper.WuaCom
{
    [Flags]
    public enum WuaUpdateSearchFlags : byte
    {
        Hidden = 1,
        Software = 2,
        Driver = 4,
        Installed = 8,
        NotInstalled = 16
    }
}
