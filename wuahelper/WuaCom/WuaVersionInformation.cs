﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using com.razorsoftware.wuahelper.Services;
using Microsoft.Win32;
using WUApiLib;

namespace com.razorsoftware.wuahelper.WuaCom
{
    /// <summary>
    /// Provides information about a Windows Update Agent installation, 
    /// </summary>
    public class WuaVersionInformation
    {
        // Fields
        protected Thread BackgroundThread;


        // Properties
        public Version LatestVersion
        {
            get
            {
                Version v;
                if (Version.TryParse(LatestVersionString, out v))
                    return v;

                return null;
            }
        }

        public Version InstalledVersion
        {
            get
            {
                Version v;
                if (Version.TryParse(InstalledVersionString, out v))
                    return v;

                return null;
            }
        }

        public Version FileVersion { get; protected set; }

        public string LatestVersionString;

        public string InstalledVersionString;

        public string FileVersionString
        {
            get
            {
                if (FileVersion == null) return null;
                return FileVersion.ToString(4);
            }
        }

        public bool IsInstalled { get; protected set; }

        public bool IsWuaAvailable { get; protected set; }

        public Exception WuaExceptionInfo { get; protected set; }

        public bool IsUpdateAvailable { get; protected set; }


        // Delegates
        public delegate void ProgressUpdatedDelegate(WuaVersionInformation sender, WuaVersionInformationEventArgs args);


        // Events
        public event ProgressUpdatedDelegate ProgressUpdated;
        public event ProgressUpdatedDelegate BackgroundOperationCompleted;


        // Methods
        public void GetOnlineInfo()
        {

        }

        public void GetOnlineInfoAsync()
        {

        }

        public void GetLocalInfo()
        {
            LatestVersionString = null;
            InstalledVersionString = null;
            FileVersion = null;

            IsInstalled = false;

            OnProgressUpdated(new WuaVersionInformationEventArgs("Searching for WUA agent...", 0));

            // Get dll path
            string file = WuaService.GetWuaBinaryPath();

            if (string.IsNullOrWhiteSpace(file))
            {
                OnProgressUpdated(new WuaVersionInformationEventArgs("Agent primary key not found. Trying possible backup keys...", 0));

                string[] backupEntries =
                {
                    "BackupServiceDll", "_ServiceDll", "OriginalServiceDll", "__ServiceDll",
                    "DisabledServiceDll", "Backup_ServiceDll"
                };

                for (int i = 0; i < backupEntries.Length && string.IsNullOrWhiteSpace(file); i++)
                {
                    OnProgressUpdated(new WuaVersionInformationEventArgs(string.Format("Searching for WUA agent ({0})...", (i + 1)), (33.33f / backupEntries.Length) * (i + 1)));

                    // Missing Dll path on windows registry, try backup key
                    object dllName = Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" + WuaService.PlatformSpecificServiceName + "\\Parameters", backupEntries[i], null);

                    if (dllName == null) continue;

                    string strDllName = dllName as string;

                    if (string.IsNullOrWhiteSpace(strDllName))
                        continue;

                    file = Environment.ExpandEnvironmentVariables(strDllName);
                }
            }

            

            // Get dll version
            if (!string.IsNullOrWhiteSpace(file))
            {
                OnProgressUpdated(new WuaVersionInformationEventArgs("Reading DLL details...", 33.33f));

                FileInfo fi = new FileInfo(file);
                if (fi.Exists)
                {
                    // Get dll version
                    FileVersionInfo dllVer = FileVersionInfo.GetVersionInfo(fi.FullName);
                    FileVersion = new Version(dllVer.FileMajorPart, dllVer.FileMinorPart, dllVer.FileBuildPart,
                        dllVer.FilePrivatePart);
                }
            }

            OnProgressUpdated(new WuaVersionInformationEventArgs("Reading COM provider details...", 66.66f));

            // Get WUA version via COM
            try
            {
                IWindowsUpdateAgentInfo agentInfo = new WindowsUpdateAgentInfoClass();
                string infoObj = agentInfo.GetInfo(@"ProductVersionString") as string;

                if (string.IsNullOrWhiteSpace(infoObj))
                    throw new NullReferenceException("WUA Agent returned an empty string in response to IWindowsUpdateAgentInfo.GetInfo()");

                IsWuaAvailable = true;
                IsInstalled = true;
                WuaExceptionInfo = null;
                InstalledVersionString = infoObj;
            }
            catch (Exception ex)
            {
                OnProgressUpdated(new WuaVersionInformationEventArgs("Failed to get COM provider details. COM connection error.", 99.99f));
                IsWuaAvailable = false;
                WuaExceptionInfo = ex;   
            }

            OnBackgroundOperationCompleted(new WuaVersionInformationEventArgs("OK", 100));
        }

        public void GetLocalInfoAsync()
        {
            if (BackgroundThread != null && BackgroundThread.IsAlive)
                throw new InvalidOperationException("The object is already executing a background task");

            BackgroundThread = new Thread(GetLocalInfo);
            BackgroundThread.IsBackground = true;
            BackgroundThread.Name = "Background WUA Agent Version Check";

            BackgroundThread.Start();
        }


        // Method Invocator
        protected virtual void OnProgressUpdated(WuaVersionInformationEventArgs args)
        {
            ProgressUpdated?.Invoke(this, args);
        }

        protected virtual void OnBackgroundOperationCompleted(WuaVersionInformationEventArgs args)
        {
            BackgroundOperationCompleted?.Invoke(this, args);
        }
    }
}
