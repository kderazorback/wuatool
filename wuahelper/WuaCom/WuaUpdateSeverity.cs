﻿namespace com.razorsoftware.wuahelper.WuaCom
{
    /// <summary>
    /// Defines constants for the severity rating of the Microsoft Security Response Center (MSRC) bulletin that is associated with an update.
    /// </summary>
    public enum WuaUpdateSeverity : byte
    {
        /// <summary>
        /// The update does not have a severity rating.
        /// </summary>
        Unspecified = 0,
        /// <summary>
        /// The update fixes a problem whose exploitation is mitigated to a significant degree by factors such as default configuration, auditing, or difficulty of exploitation.
        /// </summary>
        Moderate,
        /// <summary>
        /// The update fixes a problem whose exploitation is extremely difficult, or whose impact is minimal.
        /// </summary>
        Low,
        /// <summary>
        /// The update fixes a problem whose exploitation could result in the compromise of the confidentiality, integrity, or availability of users' data, or of the integrity or availability of processing resources.
        /// </summary>
        Important,
        /// <summary>
        /// The update fixes a problem whose exploitation could allow for the propagation of an Internet worm without user action.
        /// </summary>
        Critical,
    }
}
