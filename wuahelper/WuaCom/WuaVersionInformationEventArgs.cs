﻿
namespace com.razorsoftware.wuahelper.WuaCom
{
    /// <summary>
    /// Stores information for events raised by <see cref="WuaVersionInformation"/> asynchronous methods
    /// </summary>
    public struct WuaVersionInformationEventArgs
    {
        // Fields
        public readonly string Message;
        public readonly float Progress;

        // Constructors
        public WuaVersionInformationEventArgs(string pMessage, float pProgress)
        {
            Message = pMessage;
            Progress = pProgress;
        }
    }
}
