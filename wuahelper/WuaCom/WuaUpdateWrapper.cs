﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Xml.Linq;
using WUApiLib;

namespace com.razorsoftware.wuahelper.WuaCom
{
    /// <summary>
    /// Stores information about a Windows Update Package
    /// </summary>
    public class WuaUpdateWrapper
    {
        // Back-end fields
        protected dynamic Element;

        // Cache fields
        protected string CachedId;
        protected string CachedInformationUrl;
        protected string CachedDownloadUrl;
        protected string CachedName;
        protected long CachedSize = -1;
        protected string CachedKbArticle;
        protected bool? CachedIsInstalled;
        protected bool? CachedIsDownloaded;
        protected WuaUpdateSeverity? CachedSeverity;

        /// <summary>
        /// Stores XML information for the Update
        /// </summary>
        public XmlTypeSerializer Update
        {
            get { return Element; }
        }

        /// <summary>
        /// Stores the URL to the information page for the Update
        /// </summary>
        public string InformationUrl
        {
            get
            {
                if (string.IsNullOrEmpty(CachedInformationUrl))
                    CachedInformationUrl = WuaSearch.GetInformationUrlFor(Id);

                return CachedInformationUrl;
            }
        }

        /// <summary>
        /// Stores the URL that can be used to download the Update
        /// </summary>
        public string DownloadUrl
        {
            get
            {
                if (CachedDownloadUrl == null)
                    CachedDownloadUrl = GetDownloadUrl();

                return CachedDownloadUrl;
            }
        }

        /// <summary>
        /// Stores the Update Identifier (UUID like) for the Update Package
        /// </summary>
        public string Id
        {
            get
            {
                if (string.IsNullOrEmpty(CachedId))
                    CachedId = Element.Identity.UpdateID["value"];

                return CachedId;
            }
        }

        /// <summary>
        /// Stores the Friendly name for the Update Package (actually not so friendly)
        /// </summary>
        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(CachedName))
                    CachedName = Element.Title["value"];

                return CachedName;
            }
        }

        /// <summary>
        /// Stores the Size of this Update in Bytes
        /// </summary>
        public long Size
        {
            get
            {
                if (CachedSize < 0)
                {
                    long size;
                    if (!long.TryParse(Element.MaxDownloadSize["value"], out size))
                        size = 0;

                    CachedSize = size;
                }


                return CachedSize;
            }
        }

        /// <summary>
        /// Stores the KB Patch Article number for this Update. This value is dependent on Microsoft Update terminology but usually is only a number, without the KB prefix
        /// </summary>
        public string KbArticle
        {
            get
            {
                if (string.IsNullOrEmpty(CachedKbArticle))
                {
                    XmlTypeSerializer node = Element.KBArticleIDs;
                    if (node != null && node.HasChilds)
                        CachedKbArticle = Element.KBArticleIDs.@string["value"];
                    else
                        CachedKbArticle = "";
                }

                return CachedKbArticle;
            }
        }

        /// <summary>
        /// Returns an URL that can be used to browse the KB Article for this Update
        /// </summary>
        public string KbArticleUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(KbArticle))
                    return "";

                return "http://support.microsoft.com/kb/" + KbArticle;
            }
        }

        /// <summary>
        /// Stores a value indicating if this Update is currently installed on the System
        /// </summary>
        public bool IsInstalled
        {
            get
            {
                if (CachedIsInstalled == null)
                {
                    bool result;
                    if (!bool.TryParse(Element.IsInstalled["value"], out result))
                        result = false;

                    CachedIsInstalled = result;
                }

                return CachedIsInstalled.Value;
            }
        }

        /// <summary>
        /// Stores a value indicating if this Update is a Critical Update
        /// </summary>
        public bool IsCritical
        {
            get { return Severity == WuaUpdateSeverity.Critical; }
        }

        /// <summary>
        /// Stores the Severity value for this Update
        /// </summary>
        public WuaUpdateSeverity Severity
        {
            get
            {
                if (CachedSeverity == null)
                {
                    string value = Element.MsrcSeverity["value"];

                    if (string.IsNullOrEmpty(value))
                    {
                        CachedSeverity = WuaUpdateSeverity.Unspecified;
                        return CachedSeverity.Value;
                    }

                    WuaUpdateSeverity severity;
                    if (Enum.TryParse(value, true, out severity))
                        CachedSeverity = severity;
                    else
                        CachedSeverity = WuaUpdateSeverity.Unspecified;
                }

                return CachedSeverity.Value;
            }
        }

        /// <summary>
        /// Returns a value indicating if this update has a KB Article assigned to it
        /// </summary>
        public bool HasKbArticle
        {
            get { return !string.IsNullOrWhiteSpace(KbArticle); }
        }

        /// <summary>
        /// Returns a value indicating if this update has an Update ID assigned to it
        /// </summary>
        public bool HasId
        {
            get { return !string.IsNullOrWhiteSpace(Id); }
        }

        /// <summary>
        /// Stores a value indicating if this update has Download information available
        /// </summary>
        public bool Downloadable
        {
            get { return !string.IsNullOrWhiteSpace(DownloadUrl); }
        }

        /// <summary>
        /// Stores a value indicating if this update is already Downloaded and cached by Windows Update
        /// </summary>
        public bool IsDownloaded
        {
            get
            {
                if (CachedIsDownloaded == null)
                {
                    bool result;
                    if (!bool.TryParse(Element.IsDownloaded["value"], out result))
                        result = false;

                    CachedIsDownloaded = result;
                }

                return CachedIsDownloaded.Value;
            }
        }

        /// <summary>
        /// Serializes all wrapped Update information to a formatted XML string
        /// </summary>
        /// <returns>An string with the XML formatted data</returns>
        public string ToXml()
        {
            return Element.Body.ToString(SaveOptions.None);
        }


    // Constructor
        protected WuaUpdateWrapper() { }


        // Static Methods
        /// <summary>
        /// Wraps an specified Update information object
        /// </summary>
        /// <param name="update">Update object to wrap</param>
        /// <returns>An instance of this class that wraps the supplied Update information object</returns>
        public static WuaUpdateWrapper Wrap(IUpdate update)
        {
            XmlTypeSerializer instance = XmlTypeSerializer.WrapAround(update, typeof(IUpdate));

            return new WuaUpdateWrapper()
            {
                Element = instance,
            };
        }

        // Methods
        /// <summary>
        /// Gets an URL that can be used to download the current UID
        /// </summary>
        /// <returns>An URL to download this UID</returns>
        public string GetDownloadUrl()
        {
            NameValueCollection postBody = WuaSearch.GetPostBodyForDownloadingUid(Id);
            byte[] response;
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Host", "www.catalog.update.microsoft.com");
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("Referer", "http://www.catalog.update.microsoft.com/DownloadDialog.aspx");

                response = client.UploadValues(WuaSearch.DownloadResolveUri, postBody);
            }

            if (response == null || response.Length < 1)
                throw new NullReferenceException("The server returned a null response.");

            string script = System.Text.Encoding.UTF8.GetString(response);

            int startIndex = script.IndexOf(WuaSearch.DownloadScriptMatchStr, StringComparison.OrdinalIgnoreCase);
            if (startIndex < 0)
                return ""; // Not Found

            startIndex += WuaSearch.DownloadScriptMatchStr.Length + 1;

            int endIndex = script.IndexOfAny(new char[] {'\'', '"'}, startIndex + 2);
            if (endIndex < 0)
                return ""; // Not Found

            string url = script.Substring(startIndex, endIndex - startIndex);

            return url;
        }
    }
}
