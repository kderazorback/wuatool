﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using com.razorsoftware.wuahelper.Services;
using WUApiLib;

namespace com.razorsoftware.wuahelper.WuaCom
{
    public class WuaTask : IDisposable
    {
        // Instance Fields
        public WuaService Service { get; protected set; }
        public Control Invocator { get; set; }
        protected UpdateCollectionClass SelectedUpdates { get; set; }
        public string SearchCriteria { get; set; }
        public string[] SelectedUids { get; set; }
        public string[] MissingUpdates { get; protected set; }

        protected IUpdateSession Session { get; set; }

        protected IUpdateSearcher Searcher { get; set; }
        protected ISearchResult SearchResult { get; set; }
        protected ISearchJob SearchJob { get; set; }

        protected IUpdateDownloader Downloader { get; set; }
        protected IDownloadResult DownloadResult { get; set; }
        protected IDownloadJob DownloadJob { get; set; }

        protected IUpdateInstaller Installer { get; set; }
        protected IInstallationResult InstallResult { get; set; }
        protected IInstallationJob InstallJob { get; set; }
        internal WuaTaskMask InterfaceLinker { get; set; }

        public bool CanSearch 
        {
            get { return true; }
        }

        public bool CanDownload
        {
            get { return (SearchResult != null); }
        }

        public bool CanInstall
        {
            get { return (DownloadResult != null); }
        }
        public bool Online { get; set; }


        // Delegates
        public delegate void SearchCompleteDelegate(
            WuaUpdateWrapper[] updates, string[] missingUpdates, XmlTypeSerializer result);
        public delegate void DownloadProgressUpdatedDelegate(WuaDownloadProgress progress);
        public delegate void DownloadCompleteDelegate(KeyValuePair<WuaUpdateWrapper, string>[] results, bool succeedded);
        public delegate void InstallationProgressUpdatedDelegate(WuaInstallProgress progress);
        public delegate void InstallationCompleteDelegate(KeyValuePair<WuaUpdateWrapper, string>[] results, bool succeedded);


        // Events
        public event SearchCompleteDelegate SearchComplete;
        public event DownloadProgressUpdatedDelegate DownloadProgressUpdated;
        public event DownloadCompleteDelegate DownloadComplete;
        public event InstallationProgressUpdatedDelegate InstallProgressUpdated;
        public event InstallationCompleteDelegate InstallationComplete;


        // Constructors
        internal WuaTask()
        {  }

        // Static Members
        public static WuaTask CreateForUpdates(params string[] ids)
        {
            WuaTask instance = new WuaTask();
            instance.Service = WuaService.StartNew();
            instance.SelectedUids = ids;
            instance.Session = new UpdateSessionClass();
            instance.Session.ClientApplicationID = WuaSearch.ClientApplicationId;

            instance.InterfaceLinker = new WuaTaskMask(instance);

            return instance;
        }


        // Instance Members
        public int SolveUpdates()
        {
            Searcher = Session.CreateUpdateSearcher();
            Searcher.ClientApplicationID = WuaSearch.ClientApplicationId;
            Searcher.Online = Online;
            SearchResult = Searcher.Search(SearchCriteria);

            return ParseSearchResults();
        }

        public int SolveUpdates(string searchCriteria)
        {
            SearchCriteria = searchCriteria;
            return SolveUpdates();
        }

        public int SolveUpdates(WuaUpdateSearchFlags flags)
        {
            SearchCriteria = WuaSearch.WuaSearchFlagsToString(flags);
            return SolveUpdates();
        }

        protected int ParseSearchResults()
        {
            if (SelectedUpdates != null)
                SelectedUpdates.Clear();

            if (SelectedUids == null || SelectedUids.Length < 1)
            {
                SelectedUpdates = new UpdateCollectionClass();
                foreach (IUpdate update in SearchResult.Updates)
                    SelectedUpdates.Add(update);

                return SelectedUpdates.Count;
            }

            List<string> missingUpdates = new List<string>();

            for (int i = 0; i < SelectedUids.Length; i++)
            {
                string updateId = SelectedUids[i].Replace("-", "");
                bool found = false;
                foreach (IUpdate update in SearchResult.Updates)
                {
                    if (string.Equals(updateId, update.Identity.UpdateID.Replace("-", "")))
                    {
                        SelectedUpdates.Add(update);
                        found = true;
                        break;
                    }
                }

                if (!found)
                    missingUpdates.Add(SelectedUids[i]);
            }


            MissingUpdates = missingUpdates.ToArray();

            return SearchResult.Updates.Count;
        }

        public string DownloadUpdates()
        {
            Downloader = Session.CreateUpdateDownloader();
            Downloader.ClientApplicationID = WuaSearch.ClientApplicationId;
            Downloader.IsForced = true;
            DownloadPriority priority;
            if (!Enum.TryParse<DownloadPriority>("dpExtraHigh", true, out priority))
                priority = DownloadPriority.dpHigh;
            //Downloader.Priority = DownloadPriority.dpHigh;
            Downloader.Updates = SelectedUpdates;
            DownloadResult = Downloader.Download();

            return DownloadResult.ResultCode.ToString();
        }

        public string InstallUpdates()
        {
            Installer = Session.CreateUpdateInstaller();
            Installer.ClientApplicationID = WuaSearch.ClientApplicationId;
            Installer.IsForced = true;
            Installer.Updates = SelectedUpdates;
            InstallResult = Installer.Install();

            return InstallResult.ResultCode.ToString();
        }

        public void SolveUpdatesAsync()
        {
            Searcher = Session.CreateUpdateSearcher();
            Searcher.ClientApplicationID = WuaSearch.ClientApplicationId;

            SearchJob = Searcher.BeginSearch(SearchCriteria, InterfaceLinker, null);
        }

        public void SolveUpdatesAsync(string searchCriteria)
        {
            SearchCriteria = searchCriteria;
            SolveUpdatesAsync();
        }

        public void SolveUpdatesAsync(WuaUpdateSearchFlags flags)
        {
            SearchCriteria = WuaSearch.WuaSearchFlagsToString(flags);
            SolveUpdatesAsync();
        }
            

        public void DownloadUpdatesAsync()
        {
            Downloader = Session.CreateUpdateDownloader();
            Downloader.ClientApplicationID = WuaSearch.ClientApplicationId;
            Downloader.Updates = SelectedUpdates;

            DownloadJob = Downloader.BeginDownload(InterfaceLinker, InterfaceLinker, null);
        }

        public void InstallUpdatesAsync()
        {
            Installer = Session.CreateUpdateInstaller();
            Installer.ClientApplicationID = WuaSearch.ClientApplicationId;
            Installer.Updates = SelectedUpdates;

            InstallJob = Installer.BeginInstall(InterfaceLinker, InterfaceLinker, null);
        }

        public void Dispose()
        {
            Service.Dispose();
            Service = null;
        }

        // Event Handlers
        internal void SearchCompletedCallbackInvoke(ISearchJob searchJob, ISearchCompletedCallbackArgs callbackArgs)
        {
            SearchResult = Searcher.EndSearch(searchJob);

            ParseSearchResults();

            if (SearchComplete == null)
                return;

            List<WuaUpdateWrapper> updates = new List<WuaUpdateWrapper>();

            foreach (IUpdate update in SearchResult.Updates)
                updates.Add(WuaUpdateWrapper.Wrap(update));

            OnSearchComplete(updates.ToArray(), MissingUpdates, XmlTypeSerializer.WrapAround(SearchResult, typeof(ISearchResult)));
        }

        internal void DownloadProgressChangedCallbackInvoke(IDownloadJob downloadJob, IDownloadProgressChangedCallbackArgs callbackArgs)
        {
            if (DownloadProgressUpdated == null)
                return;

            OnDownloadProgressUpdated(new WuaDownloadProgress(downloadJob.Updates, callbackArgs.Progress));
        }

        internal void DownloadCompletedCallbackInvoke(IDownloadJob downloadJob, IDownloadCompletedCallbackArgs callbackArgs)
        {
            DownloadResult = Downloader.EndDownload(downloadJob);

            if (DownloadComplete == null)
                return;

            List<KeyValuePair<WuaUpdateWrapper, string>> results = new List<KeyValuePair<WuaUpdateWrapper, string>>();

            for (int i = 0; i < downloadJob.Updates.Count; i++)
                results.Add(new KeyValuePair<WuaUpdateWrapper, string>(WuaUpdateWrapper.Wrap(downloadJob.Updates[i]), DownloadResult.GetUpdateResult(i).ResultCode.ToString()));

            OnDownloadComplete(results.ToArray(), DownloadResult.ResultCode == OperationResultCode.orcSucceeded);
        }

        internal void InstallationProgressChangedCallbackInvoke(IInstallationJob installationJob, IInstallationProgressChangedCallbackArgs callbackArgs)
        {
            if (InstallProgressUpdated == null)
                return;

            OnInstallProgressUpdated(new WuaInstallProgress(installationJob.Updates, callbackArgs.Progress));
        }

        internal void InstallationCompletedCallbackInvoke(IInstallationJob installationJob, IInstallationCompletedCallbackArgs callbackArgs)
        {
            InstallResult = Installer.EndInstall(installationJob);

            if (InstallationComplete == null)
                return;

            List<KeyValuePair<WuaUpdateWrapper, string>> results = new List<KeyValuePair<WuaUpdateWrapper, string>>();

            for (int i = 0; i < installationJob.Updates.Count; i++)
                results.Add(new KeyValuePair<WuaUpdateWrapper, string>(WuaUpdateWrapper.Wrap(installationJob.Updates[i]), InstallResult.GetUpdateResult(i).ResultCode.ToString()));

            OnInstallationComplete(results.ToArray(), InstallResult.ResultCode == OperationResultCode.orcSucceeded);
        }

        protected virtual void OnSearchComplete(WuaUpdateWrapper[] updates, string[] missingUpdates, XmlTypeSerializer result)
        {
            if (Invocator != null && Invocator.InvokeRequired)
            {
                Delegate d = new SearchCompleteDelegate(OnSearchComplete);
                Invocator.Invoke(d, new object[] {updates, missingUpdates, result});

                return;
            }

            SearchComplete?.Invoke(updates, missingUpdates, result);
        }

        protected virtual void OnDownloadProgressUpdated(WuaDownloadProgress progress)
        {
            if (Invocator != null && Invocator.InvokeRequired)
            {
                Delegate d = new DownloadProgressUpdatedDelegate(OnDownloadProgressUpdated);
                Invocator.Invoke(d, new object[] { progress });

                return;
            }

            DownloadProgressUpdated?.Invoke(progress);
        }

        protected virtual void OnDownloadComplete(KeyValuePair<WuaUpdateWrapper, string>[] results, bool succeedded)
        {
            if (Invocator != null && Invocator.InvokeRequired)
            {
                Delegate d = new DownloadCompleteDelegate(OnDownloadComplete);
                Invocator.Invoke(d, new object[] { results, succeedded });

                return;
            }

            DownloadComplete?.Invoke(results, succeedded);
        }

        protected virtual void OnInstallProgressUpdated(WuaInstallProgress progress)
        {
            if (Invocator != null && Invocator.InvokeRequired)
            {
                Delegate d = new InstallationProgressUpdatedDelegate(OnInstallProgressUpdated);
                Invocator.Invoke(d, new object[] { progress });

                return;
            }

            InstallProgressUpdated?.Invoke(progress);
        }

        protected virtual void OnInstallationComplete(KeyValuePair<WuaUpdateWrapper, string>[] results, bool succeedded)
        {
            if (Invocator != null && Invocator.InvokeRequired)
            {
                Delegate d = new InstallationCompleteDelegate(OnInstallationComplete);
                Invocator.Invoke(d, new object[] { results, succeedded });

                return;
            }

            InstallationComplete?.Invoke(results, succeedded);
        }
    }
}
