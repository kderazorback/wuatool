﻿using System.Collections.Generic;
using WUApiLib;

namespace com.razorsoftware.wuahelper.WuaCom
{
    public class WuaInstallProgress
    {
        // Fields
        protected IInstallationProgress InstallProgress;
        protected XmlTypeSerializer CachedXmlInstallProgress;

        // Properties
        public WuaUpdateWrapper[] Updates { get; protected set; }

        public XmlTypeSerializer Progress
        {
            get
            {
                if (CachedXmlInstallProgress == null)
                    CachedXmlInstallProgress = XmlTypeSerializer.WrapAround(InstallProgress, typeof(IInstallationProgress));

                return CachedXmlInstallProgress;
            }
        }

        public float PercentComplete
        {
            get { return InstallProgress.PercentComplete; }
        }

        public float UpdatePercentComplete
        {
            get { return InstallProgress.CurrentUpdatePercentComplete; }
        }

        public int UpdateIndex { get { return InstallProgress.CurrentUpdateIndex; } }

        public WuaUpdateWrapper CurrentUpdate { get { return Updates[UpdateIndex]; } }

        // Constructor
        internal WuaInstallProgress(IUpdateCollection updates, IInstallationProgress progress)
        {
            List<WuaUpdateWrapper> upd = new List<WuaUpdateWrapper>();
            foreach (IUpdate update in updates)
                upd.Add(WuaUpdateWrapper.Wrap(update));

            Updates = upd.ToArray();
            InstallProgress = progress;
        }
    }
}
