﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using com.razorsoftware.wuahelper.Services;
using WUApiLib;

namespace com.razorsoftware.wuahelper.WuaCom
{
    public class WuaSearch
    {
        // Static Fields
        /// <summary>
        /// Stores the Client Identifier used to communicate with Windows Update
        /// </summary>
        public static string ClientApplicationId { get; set; } = "com.razorsoftware.WUATool";

        // Static Methods
        /// <summary>
        /// Returns an URL to the information site for the specified Update ID
        /// </summary>
        /// <param name="uid">Target update ID</param>
        /// <returns>An URL to a site with information about the specified Update</returns>
        public static string GetInformationUrlFor(string uid)
        {
            return "http://www.catalog.update.microsoft.com/ScopedViewInline.aspx?updateid=" + uid;
        }

        /// <summary>
        /// Returns a blob that must be used as a body in the POST request to resolve the Download URL for the specified Update ID
        /// </summary>
        /// <param name="uid">Target update ID</param>
        /// <returns>A POST body that must be sent to the Windows Update Catalog for resolving the Download URL</returns>
        internal static NameValueCollection GetPostBodyForDownloadingUid(string uid)
        {
            return new NameValueCollection()
            {
                    { "updateIDs", "[{\"size\":0,\"languages\":\"\",\"uidInfo\":\"" + uid + "\",\"updateID\":\"" + uid + "\"}]" },
                    { "updateIDsBlockedForImport", "" },
                    { "wsusApiPresent", "" },
                    { "contentImport", "" },
                    { "sku", "" },
                    { "serverName", "" },
                    { "ssl", "" },
                    { "portNumber", "" },
                    { "version", "" },
            };  
        }

        /// <summary>
        /// Stores the URL that must be used to retrieve a UID Download Link via POST Request
        /// </summary>
        public static string DownloadResolveUri { get; set; } = @"http://www.catalog.update.microsoft.com/DownloadDialog.aspx";

        /// <summary>
        /// Stores the Match string used to identify and extract UID Download URLs from HTTP Post Responses
        /// </summary>
        public static string DownloadScriptMatchStr { get; set; } = @"downloadInformation[0].files[0].url = ";

        /// <summary>
        /// Searches for updates on the system via Windows Update and using the specified flags
        /// </summary>
        /// <param name="flags">Which Updates to return</param>
        /// <returns>A list of Updates that matches the supplied flags</returns>
        public static WuaUpdateWrapper[] GetUpdates(WuaUpdateSearchFlags flags)
        {
            string filter = WuaSearchFlagsToString(flags);

            List<WuaUpdateWrapper> output;
            using (WuaService manager = WuaService.StartNew())
            {
                // Get available updates
                UpdateSessionClass uSession = new UpdateSessionClass();
                IUpdateSearcher uSearcher = uSession.CreateUpdateSearcher();
                uSearcher.ClientApplicationID = ClientApplicationId;
                ISearchResult uResult = uSearcher.Search(filter);

                output = new List<WuaUpdateWrapper>();
                foreach (IUpdate update in uResult.Updates)
                    output.Add(WuaUpdateWrapper.Wrap(update));

                uResult = null;
                uSearcher = null;
                uSession = null;
            }

            return output.ToArray();
        }

        public static WuaInstallSessionResult InstallUpdates(params string[] uid)
        {
            UpdateCollectionClass targetUpdates = new UpdateCollectionClass();
            IDownloadResult dResult;
            IInstallationResult iResult;
            WuaInstallSessionResult output;

            using (WuaService manager = WuaService.StartNew())
            {
                UpdateSessionClass uSession = new UpdateSessionClass();
                uSession.ClientApplicationID = ClientApplicationId;
                IUpdateInstaller uInstaller = uSession.CreateUpdateInstaller();
                IUpdateDownloader uDownloader = uSession.CreateUpdateDownloader();
                IUpdateSearcher uSearcher = uSession.CreateUpdateSearcher();
                ISearchResult sResult = uSearcher.Search("IsInstalled=0");


                // Search Updates
                foreach (string id in uid)
                {
                    string simpleId = id.Replace("-", "");
                    bool found = false;
                    foreach (IUpdate update in sResult.Updates)
                    {
                        if (string.Equals(update.Identity.UpdateID.Replace("-", ""), simpleId,
                            StringComparison.OrdinalIgnoreCase))
                        {
                            targetUpdates.Add(update);
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                        throw new KeyNotFoundException(String.Format("The specified update with id \"{0}\" cannot be found.", id));
                }

                if (targetUpdates.Count != uid.Length)
                    throw new ArgumentException("There are no updates to install.");

                // Download Updates
                uDownloader.Updates = targetUpdates;
                uDownloader.ClientApplicationID = ClientApplicationId;
                uDownloader.IsForced = true;
                DownloadPriority priority;
                if (!Enum.TryParse<DownloadPriority>("dpExtraHigh", true, out priority)) // Required since W7 and earlier doesn't have a dpExtraHigh value on the enum
                    priority = DownloadPriority.dpHigh;
                uDownloader.Priority = priority;
                dResult = uDownloader.Download();

                // Install Updates
                uInstaller.Updates = targetUpdates;
                uInstaller.ClientApplicationID = ClientApplicationId;
                uInstaller.IsForced = true;
                iResult = uInstaller.Install();

                output = new WuaInstallSessionResult(targetUpdates, dResult, iResult);
            }

            return output;
        }

        public static string WuaSearchFlagsToString(WuaUpdateSearchFlags flags)
        {
            StringBuilder str = new StringBuilder();

            if (flags.HasFlag(WuaUpdateSearchFlags.Driver))
                str.Append("Type='Driver'");

            if (flags.HasFlag(WuaUpdateSearchFlags.Software))
            {
                if (str.Length > 0)
                    str.Append(" or ");
                str.Append("Type='Software'");
            }

            if (flags.HasFlag(WuaUpdateSearchFlags.Hidden))
            {
                if (str.Length > 0)
                    str.Append(" or ");
                str.Append("IsHidden=1");
            }

            if (flags.HasFlag(WuaUpdateSearchFlags.Installed))
            {
                if (str.Length > 0)
                    str.Append(" or ");
                str.Append("IsInstalled=1");
            }

            if (flags.HasFlag(WuaUpdateSearchFlags.NotInstalled))
            {
                if (str.Length > 0)
                    str.Append(" or ");
                str.Append("IsInstalled=0");
            }

            if (str.Length == 0)
                str.Append("IsInstalled=0 and Type='Software'");

            return str.ToString();
        }
    }
}
