﻿using System.Collections.Generic;
using WUApiLib;

namespace com.razorsoftware.wuahelper.WuaCom
{
    /// <summary>
    /// Stores information about a Background Update Download that is in Progress
    /// </summary>
    public class WuaDownloadProgress
    {
        // Fields
        protected IDownloadProgress DownloadProgress;
        protected XmlTypeSerializer CachedXmlDownloadProgress;

        // Properties
        public WuaUpdateWrapper[] Updates { get; protected set; }

        public XmlTypeSerializer Progress
        {
            get
            {
                if (CachedXmlDownloadProgress == null)
                    CachedXmlDownloadProgress = XmlTypeSerializer.WrapAround(DownloadProgress, typeof(IDownloadProgress));

                return CachedXmlDownloadProgress;
            }
        }

        public float PercentComplete
        {
            get
            {
                if (DownloadProgress.TotalBytesToDownload < 1)
                    return 0;

                return (float)((DownloadProgress.TotalBytesDownloaded*100)/
                       DownloadProgress.TotalBytesToDownload);
            }
        }

        public float UpdatePercentComplete
        {
            get
            {
                return (float)((DownloadProgress.CurrentUpdateBytesDownloaded*100)/DownloadProgress.CurrentUpdateBytesToDownload);
            }
        }

        public int UpdateIndex { get { return DownloadProgress.CurrentUpdateIndex; } }

        public WuaUpdateWrapper CurrentUpdate { get { return Updates[UpdateIndex]; } }

        public long UpdateBytesDownloaded { get { return (long) DownloadProgress.CurrentUpdateBytesDownloaded; } }
        public long UpdateTotalBytes { get { return (long)DownloadProgress.CurrentUpdateBytesToDownload; } }

        public long BytesDownloaded { get { return (long)DownloadProgress.TotalBytesDownloaded; } }
        public long TotalBytes { get { return (long)DownloadProgress.TotalBytesToDownload; } }

        // Constructor
        internal WuaDownloadProgress(IUpdateCollection updates, IDownloadProgress progress)
        {
            List<WuaUpdateWrapper> upd = new List<WuaUpdateWrapper>();
            foreach (IUpdate update in updates)
                upd.Add(WuaUpdateWrapper.Wrap(update));

            Updates = upd.ToArray();
            DownloadProgress = progress;
        }
    }
}
