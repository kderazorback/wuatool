﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;

namespace com.razorsoftware.wuahelper.Downloads
{
    /// <summary>
    /// Provides support for downloading data via HTTP/HTTPS with continue/retry support
    /// </summary>
    /// <remarks>
    /// <para>This implementation downloads resources from HTTP/HTTPS hosts in chunks with Retry/Resume support.</para>
    /// <para>If this class owns the underlying output stream (passed an string to its constructor), 
    /// seeking operations will be performed on it when calling <see cref="DownloadBase.Start()"/> or <see cref="DownloadBase.Resume(long)"/> methods.
    /// <example><code>
    /// Start(0x123); // Will seek to the start of the stream and download only 0x123 bytes from the host.
    /// Start(); // Will seek to the start of the stream and download the full content from the host
    /// Resume(0xAB, 0x123); // Will seek to offset 0xAB on the output stream and download only 0x123 bytes from the host
    /// </code></example></para>
    /// <para>If <see cref="DownloadBase.TruncateStream"/> is enabled, and the class owns the underlying stream,
    /// The stream size will be truncated to the last byte downloaded from the host.
    /// <example><code>
    /// // TruncateStream is TRUE
    /// // Starting with a file that is 0xFFF in size.
    /// TruncateStream = true;
    /// Resume(0xAB, 0x123); // Download only 0x123 bytes starting from offset 0xAB.
    /// Dispose(); // Close the underlying stream and truncate it. It will be now 0x1CE bytes in size (0xAB + 0x123)
    /// </code>
    /// <code>
    /// // TruncateStream is FALSE
    /// // Starting with a file that is 0xFFF in size.
    /// TruncateStream = false;
    /// Resume(0xAB, 0x123); // Download only 0x123 bytes starting from offset 0xAB.
    /// Dispose(); // Close the underlying stream and don't truncate it. It will still be 0xFFF bytes in size
    /// </code></example></para>
    /// <para>If the class don't owns the underlying stream (by passing an stream to its constructor), no seeking operations will be carried out.
    /// Download will take place at the current stream position. Regardless if <see cref="DownloadBase.Start()"/> or <see cref="DownloadBase.Resume(long)"/> methods are used.</para>
    /// <para>When downloading a resource, the class will try to download the file <see cref="WebRetryAttempts"/> times.
    ///  If a try fails, it will automatically resume from the last byte received and try to continue from there.</para>
    /// </remarks>
    public class HttpDownload : DownloadBase
    {
        // Properties
        /// <summary>
        /// If this class owns the Stream (a filename was passed to the constructor) then this property will store the <see cref="FileInfo"/> instance that references to the output file
        /// </summary>
        public FileInfo File { get; protected set; }

        /// <summary>
        /// Stores the Source URI from where the content will be downloaded
        /// </summary>
        public Uri SourceUri { get; protected set; }

        /// <summary>
        /// Stores the last <see cref="HttpWebResponse"/> information received by the host as a result of a <see cref="HttpWebRequest"/> object being sent.
        /// If this value is null, then either no request has been sent, or the host has not answered a pending request yet
        /// </summary>
        public HttpWebResponse LastResponseHeader { get; protected set; }

        /// <summary>
        /// Stores the amount of bytes that have been already downloaded
        /// </summary>
        public override long DownloadedBytes
        {
            get
            {
                if (Resumable)
                {
                    if (ContentLength < 0) return Stream.Position;

                    if (Stream.Position > ContentLength) return 0; // Invalid downloaded value

                    return Stream.Position;
                }

                return 0; // Not resumable
            }
        }

        /// <summary>
        /// Stores the amount of time in milliseconds this class should wait for a download operation, before timing out
        /// </summary>
        public int WebTimeoutMs { get; set; } = 15000;

        /// <summary>
        /// Stores how much attempts to download a file should be made by this instance before giving up if errors occurs
        /// </summary>
        public uint WebRetryAttempts { get; set; } = 3;

        /// <summary>
        /// Stores the amount of bytes that are still required to download
        /// </summary>
        public override long RemainingBytes
        {
            get
            {
                if (ContentLength < 0) return -1;

                if (ContentLength < DownloadedBytes) return ContentLength;

                return (ContentLength - DownloadedBytes);
            }
        }

        /// <summary>
        /// Stores the UserAgent string that will be sent to the target host on every HTTP Request header. If this value is null, no UserAgent head entry will be sent at all.
        /// </summary>
        public string UserAgentString { get; set; }

        /// <summary>
        /// Stores the Cookies that will be sent to the target host on every HTTP Request header
        /// </summary>
        public CookieCollection Cookies { get; protected set; } = new CookieCollection();

        /// <summary>
        /// Stores a list of additional head entries that will be sent to the target host on every HTTP Request header.
        /// Headers are a KeyValuePair of string type, where the first item is the Key name, and the second one its value.
        /// </summary>
        public Dictionary<string, string> Headers { get; protected set; } = new Dictionary<string, string>();


        // Delegates
        /// <summary>
        /// Defines a delegate that can subscribe to the <see cref="StatusCodeReceived"/> event.
        /// </summary>
        /// <param name="args">Status of this class at the exact moment the event was generated</param>
        /// <param name="code">Status code received from the remote host</param>
        public delegate void StatusCodeReceivedDelegate(HttpDownloadEventArgs args, HttpStatusCode code);

        /// <summary>
        /// Defines a delegate that can subscribe to the <see cref="DataReceived"/> event.
        /// </summary>
        /// <param name="args">Status of this class at the exact moment the event was generated</param>
        /// <param name="buffer">RAW data received from host. This buffer is of a fixed size</param>
        /// <param name="receivedBytes">Amount of bytes received from the host, use this to locate the last data byte received on the buffer.</param>
        /// <param name="msSinceLastUpdate">Milliseconds that passed since the last time the class received data from the host</param>
        public new delegate void DataReceivedDelegate(
            HttpDownloadEventArgs args, byte[] buffer, long receivedBytes, long msSinceLastUpdate);

        /// <summary>
        /// Defines a delegate that can subscribe to the <see cref="ConnectionException"/> event.
        /// </summary>
        /// <param name="args">Status of this class at the exact moment the event was generated</param>
        /// <param name="ex">Exception that raised on the download process</param>
        public new delegate void ConnectionExceptionDelegate(HttpDownloadEventArgs args, Exception ex);

        /// <summary>
        /// Defines a delegate that can subscribe to the <see cref="DownloadComplete"/> event.
        /// </summary>
        /// <param name="args">Status of this class at the exact moment the event was generated</param>
        /// <param name="completed">True if the file has fully downloaded. If there are still remaining bytes to download, this will be False</param>
        public new delegate void DownloadCompleteDelegate(HttpDownloadEventArgs args, bool completed);


        // Events
        public event StatusCodeReceivedDelegate StatusCodeReceived;
        public new event DataReceivedDelegate DataReceived;
        public new event ConnectionExceptionDelegate ConnectionException;
        public new event DownloadCompleteDelegate DownloadComplete;
        

        // Event Invocator
        /// <summary>
        /// Invokes the <see cref="StatusCodeReceived"/> event with the provided arguments
        /// </summary>
        /// <param name="args">Current state of this class</param>
        /// <param name="code">Status code received</param>
        protected virtual void OnStatusCodeReceived(HttpDownloadEventArgs args, HttpStatusCode code)
        {
            StatusCodeReceived?.Invoke(args, code);
        }

        /// <summary>
        /// Invokes the <see cref="DataReceived"/> event with the provided arguments
        /// </summary>
        /// <param name="args">Current state of this class</param>
        /// <param name="buffer">Buffer received from the host</param>
        /// <param name="receivedBytes">Amount of bytes received from the host and stored in buffer</param>
        /// <param name="msSinceLastUpdate">Milliseconds since the last time data was received from the host</param>
        protected virtual void OnDataReceived(HttpDownloadEventArgs args, byte[] buffer, long receivedBytes, long msSinceLastUpdate)
        {
            base.OnDataReceived(this, buffer, receivedBytes, msSinceLastUpdate);
            DataReceived?.Invoke(args, buffer, receivedBytes, msSinceLastUpdate);
        }

        /// <summary>
        /// Invokes the <see cref="ConnectionException"/> event with the provided arguments
        /// </summary>
        /// <param name="args">Current state of this class</param>
        /// <param name="ex">Exception raised during the download process</param>
        protected virtual void OnConnectionException(HttpDownloadEventArgs args, Exception ex)
        {
            ConnectionException?.Invoke(args, ex);
            base.OnConnectionException(this, ex);
        }

        /// <summary>
        /// Invokes the <see cref="DownloadComplete"/> event with the provided arguments
        /// </summary>
        /// <param name="args">Current state of this class</param>
        /// <param name="completed">If the download has been fully completed</param>
        protected virtual void OnDownloadComplete(HttpDownloadEventArgs args, bool completed)
        {
            DownloadComplete?.Invoke(args, completed);
            base.OnDownloadComplete(this, completed);
        }


        // Constructors
        /// <summary>
        /// Creates a new instance of this class without performing any Initialization operation
        /// </summary>
        /// <param name="name">Indicates the name of this Instance (purely cosmetic)</param>
        protected HttpDownload(string name) :base(name) { }

        /// <summary>
        /// Creates a new instance of this class by specifying a download URL.
        /// </summary>
        /// <param name="url">Source URL of the content to download</param>
        /// <param name="name">Indicates the name of this Instance (purely cosmetic)</param>
        /// <param name="outStream">Output stream where the downloaded content will be written to</param>
        public HttpDownload(string name, string url, Stream outStream) :base(name)
        {
            Stream = outStream;
            Source = url;
            OwnsStream = false;

            Initialize();
        }

        /// <summary>
        /// Creates a new instance of this class by specifying a download URL and a target filename on disk where the content will be saved to
        /// </summary>
        /// <param name="url">Source URL of the content to download</param>
        /// <param name="name">Indicates the name of this Instance (purely cosmetic)</param>
        /// <param name="filename">Target Filename where the content will be downloaded to</param>
        public HttpDownload(string name, string url, string filename) :base(name)
        {
            Source = url;
            FileInfo fi = new FileInfo(filename);

            Initialize();

            if (fi.Directory != null && !fi.Directory.Exists)
                fi.Directory.Create();

            File = fi;
            Stream = new FileStream(fi.FullName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            OwnsStream = true;
        }


        // Methods (protected)
        /// <summary>
        /// Initializes the current instance of <see cref="HttpDownload"/>
        /// </summary>
        protected void Initialize()
        {
            try
            {
                SourceUri = new Uri(Source);
            }
            catch (Exception ex)
            {
                throw new UriFormatException("Failed to parse Source URL for resource download. Check inner exception for details.", ex);
            }
        }

        /// <summary>
        /// Starts the download operation using the specified starting offset and length
        /// </summary>
        /// <param name="startOffset">Optional starting offset for the download operation. The content will be downloaded starting from the this offset in bytes.
        /// If null is specified, then the entire content will be downloaded</param>
        /// <param name="length">Amount of data to download from the host. Use null to download the entire content</param>
        /// <returns>the amount of bytes downloaded to the output stream from the source URL</returns>
        protected override long StartDownload(long? startOffset, long? length)
        {
            long pStartOffset = (startOffset.HasValue ? startOffset.Value : 0);
            long pSize = (length.HasValue ? length.Value - 1 : ContentLength); // Apply value correction of -1 when converting from Length parameter to a EndOffset
            IsAborted = false;
            IsDownloading = true;

            if (pSize < 0)
                pSize = 0;

            HttpWebRequest request = GetBasicHttpRequestObject(false);

            if (pStartOffset > 0 || length.HasValue)
            {
                if (length.HasValue)
                    request.AddRange(pStartOffset, pStartOffset + pSize);
                else
                    request.AddRange(pStartOffset);
            }

            HttpWebResponse response = null;
            int triesLeft = (int)WebRetryAttempts;

            byte[] buffer = new byte[1024 * 1024 * 4]; // 4 MiB buffer
            long downloadedBytes = 0;

            Stopwatch sw = new Stopwatch();
            bool noMoreContent = false;

            do
            {
                try
                {
                    if (triesLeft < 1 || IsAborted)
                        break; // No more retrying

                    triesLeft--;

                    // Get Response from host
                    request.Timeout = 20000;
                    response = (HttpWebResponse)request.GetResponse();
                    LastResponseHeader = response; // Update last response

                    OnStatusCodeReceived(new HttpDownloadEventArgs(this, triesLeft), response.StatusCode); // Call the StatusCodeReceived event

                    if (response.StatusCode == HttpStatusCode.NoContent || response.StatusCode == HttpStatusCode.RequestedRangeNotSatisfiable)
                    {
                        // Target resource is empty
                        break;
                    }

                    // Get resource stream
                    Stream responseStream = response.GetResponseStream();

                    if (responseStream == Stream.Null)
                    {
                        response.Dispose();
                        continue;
                    }

                    sw.Restart();

                    do
                    {
                        if (IsAborted)
                            break;

                        // Loop until we cannot read any more bytes

                        int receivedbytes = responseStream.Read(buffer, 0, buffer.Length); // Connection exceptions can happen here

                        if (receivedbytes < 1)
                        {
                            Thread.Sleep(250);
                            continue; // Check if this line jumps to the condition or to the loop start. Must jump to the condition.
                        }

                        OnDataReceived(new HttpDownloadEventArgs(this, triesLeft), buffer, receivedbytes, sw.ElapsedMilliseconds); // Call the DataReceived event

                        // Write to the output stream
                        Stream.Write(buffer, 0, receivedbytes);
                        downloadedBytes += receivedbytes;

                        sw.Restart();

                        if (DownloadedBytes + pStartOffset >= pSize && (ContentLength > 0 || length.HasValue))
                        {
                            noMoreContent = true;
                            break;
                        }
                    } while (sw.ElapsedMilliseconds < WebTimeoutMs);

                    if (noMoreContent)
                        break;
                }
                catch (Exception ex)
                {
                    OnConnectionException(new HttpDownloadEventArgs(this, triesLeft), ex);  // Call the ConnectionException event
                }
                finally
                {
                    if (response != null) // Close the response stream before exiting the try...catch
                        response.Close();

                    // Prepare the next request object
                    if (!noMoreContent && triesLeft > 0)
                    {
                        request = GetBasicHttpRequestObject(false);
                        // Continue from where we left
                        if (length.HasValue)
                            request.AddRange(DownloadedBytes + pStartOffset, pStartOffset + pSize);
                        else
                            request.AddRange(DownloadedBytes + pStartOffset);
                    }
                }
            } while (Resumable && !IsAborted);

            sw.Stop();
            IsDownloading = false;
            OnDownloadComplete(new HttpDownloadEventArgs(this, triesLeft), (RemainingBytes > 0));  // Call the DownloadComplete event

            if (response != null) // Close the response stream before exiting
                response.Close();

            return downloadedBytes;
        }

        /// <summary>
        /// Generates a new <see cref="HttpWebRequest"/> object and initializes it with basic settings from this instance.
        /// </summary>
        /// <param name="isHeadOnly">A value indicating if the Request will be used to retrieve only the HEAD part of the resource (true) or the entire resource (false).</param>
        /// <returns>A new <see cref="HttpWebRequest"/> object that can be used to request data from the target host where the resource is located</returns>
        protected HttpWebRequest GetBasicHttpRequestObject(bool isHeadOnly)
        {
            HttpWebRequest request = WebRequest.CreateHttp(SourceUri.AbsoluteUri);

            // Setup WebRequest
            request.Host = SourceUri.Host;
            request.AutomaticDecompression = DecompressionMethods.GZip;
            if (Cookies != null && Cookies.Count > 0)
            {
                request.CookieContainer = new CookieContainer(Cookies.Count);
                request.CookieContainer.Add(Cookies);
            }
            request.KeepAlive = !isHeadOnly;
            request.UserAgent = UserAgentString;
            request.AllowAutoRedirect = true;

            // Append extra headers if any
            if (Headers != null && Headers.Count > 0)
            {
                foreach (KeyValuePair<string, string> entry in Headers)
                {
                    try
                    {
                        request.Headers.Add(entry.Key, entry.Value);
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidDataException("Cannot add key \"" + entry + "\" to HTTP Request Header. Check inner exception for details.", ex);
                    }
                }
            }

            // Setup HTTP Method
            request.Method = (isHeadOnly ? "HEAD" : "GET");

            return request;
        }
        

        // Methods (public)
        /// <summary>
        /// Gets content information from the source URL, like Size, Type, etc.
        /// </summary>
        /// <exception cref="UriFormatException">Thrown when the Source URL cannot be properly identified</exception>
        /// <exception cref="WebException">Thrown when there was an error communicating with the target host. InnerException may provide more specific info about the error</exception>
        /// <returns>True if the operation succeeded, otherwise False</returns>
        public override bool GetInfo()
        {
            HttpWebRequest request = GetBasicHttpRequestObject(true);
            HttpWebResponse response;
            InfoQueried = true;

            try
            {
                //request.AddRange(0, 1); // Request partial resource DATA, to check if the host supports partial messages
                request.Timeout = 7000;
                response = request.GetResponse() as HttpWebResponse;

                if (response == null)
                    throw new InvalidCastException(
                        "The response object returned by the .NET Framework was not of the expected type.");

                // Check returned Status Code
                switch (response.StatusCode)
                {
                    // Redirection codes (currently not handled by this method. Expected to be handled internally by HttpWebRequest object)
                    case HttpStatusCode.MultipleChoices:
                    case HttpStatusCode.MovedPermanently:
                    case HttpStatusCode.Found:
                    case HttpStatusCode.SeeOther:
                    case HttpStatusCode.UseProxy:
                    case HttpStatusCode.Unused:
                    case HttpStatusCode.TemporaryRedirect:
                        throw new WebException("The server returned an unexpected redirection status code. " + response.StatusCode.ToString("N0") + " " + response.StatusDescription, null, WebExceptionStatus.ConnectFailure, response);

                    // Not supported
                    case HttpStatusCode.SwitchingProtocols:
                    case HttpStatusCode.Created:
                    case HttpStatusCode.Accepted:
                    case HttpStatusCode.NotModified:
                        throw new WebException("The server returned an status code that is not supported or expected by this library. " + response.StatusCode.ToString("N0") + " " + response.StatusDescription, null, WebExceptionStatus.ProtocolError, response);

                    // Server Error
                    case HttpStatusCode.RequestTimeout:
                    case HttpStatusCode.Conflict:
                    case HttpStatusCode.InternalServerError:
                    case HttpStatusCode.BadGateway:
                    case HttpStatusCode.ServiceUnavailable:
                    case HttpStatusCode.GatewayTimeout:
                        throw new WebException("The server experienced an Internal Error and cannot fulfill the request. " + response.StatusCode.ToString("N0") + " " + response.StatusDescription, null, WebExceptionStatus.ConnectFailure, response);


                    // OK to proceed. Partial content requests supported
                    case HttpStatusCode.PartialContent:
                    case HttpStatusCode.RequestedRangeNotSatisfiable: // Empty Resource?
                        Downloadable = true;
                        Resumable = true;
                        ContentLength = response.ContentLength; // If missing, it will be -1 anyways
                        break;

                    // OK to proceed. Partial content requests are NOT supported
                    case HttpStatusCode.Continue:
                    case HttpStatusCode.OK:
                    case HttpStatusCode.ResetContent:
                    case HttpStatusCode.NonAuthoritativeInformation:
                        Downloadable = true;
                        Resumable =
                            (string.Equals("bytes", response.GetResponseHeader("Accept-Ranges").Trim(),
                                StringComparison.OrdinalIgnoreCase));
                        ContentLength = response.ContentLength; // If missing, it will be -1 anyways
                        break;


                    // Invalid URI
                    case HttpStatusCode.NoContent:
                    case HttpStatusCode.BadRequest:
                    case HttpStatusCode.Unauthorized:
                    case HttpStatusCode.PaymentRequired:
                    case HttpStatusCode.Forbidden:
                    case HttpStatusCode.NotFound:
                    case HttpStatusCode.MethodNotAllowed:
                    case HttpStatusCode.NotAcceptable:
                    case HttpStatusCode.ProxyAuthenticationRequired:
                    case HttpStatusCode.Gone:
                    case HttpStatusCode.LengthRequired:
                    case HttpStatusCode.PreconditionFailed:
                    case HttpStatusCode.RequestEntityTooLarge:
                    case HttpStatusCode.RequestUriTooLong:
                    case HttpStatusCode.UnsupportedMediaType:
                    case HttpStatusCode.ExpectationFailed:
                    case HttpStatusCode.UpgradeRequired:
                    case HttpStatusCode.NotImplemented:
                    case HttpStatusCode.HttpVersionNotSupported:
                        throw new WebException("The source URI is invalid and cannot be processed by the target host. " + response.StatusCode.ToString("N0") + " " + response.StatusDescription, null, WebExceptionStatus.ConnectFailure, response);

                    default:
                        throw new KeyNotFoundException("The Status Code returned by the host cannot be recognized. " + response.StatusCode);
                }
            }
            catch (WebException ex)
            {
                // Process a Web communication exception
                Downloadable = false;
                Resumable = false;
                ContentLength = -1;
                LastResponseHeader = ex.Response as HttpWebResponse;
                return false;
            }
            catch (Exception ex)
            {
                throw new WebException("Failed to communicate with the target host. Check inner exception for details.", ex);
            }


            // Set last response header
            LastResponseHeader = response;
            return true;
        }
    }
}
