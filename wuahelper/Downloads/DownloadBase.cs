using System;
using System.IO;
using System.Threading;

namespace com.razorsoftware.wuahelper.Downloads
{
    /// <summary>
    /// Provides a base class for objects that download remote resources
    /// </summary>
    public abstract class DownloadBase : IDisposable
    {
        // Fields
        protected Thread BackgroundThread;


        // Properties
        /// <summary>
        /// Indicates if the resource download can be resumed giving the current status of the host, and the local cache
        /// </summary>
        public bool Resumable { get; protected set; }

        /// <summary>
        /// Indicates if the resource can be downloaded from the host at this time
        /// </summary>
        public bool Downloadable { get; protected set; }

        /// <summary>
        /// Stores the output stream where the resource will be downloaded
        /// </summary>
        public Stream Stream { get; protected set; }

        /// <summary>
        /// Stores the RAW Source string from where the content will be downloaded as specified when initializing this instance.
        /// </summary>
        public string Source { get; protected set; }

        /// <summary>
        /// Stores the total size of the content referenced by the source host
        /// </summary>
        public long ContentLength { get; protected set; }

        /// <summary>
        /// Stores the amount of bytes that have been already downloaded
        /// </summary>
        public abstract long DownloadedBytes { get; }

        /// <summary>
        /// Indicates if this instance should truncate the output stream to the downloaded size when disposed
        /// </summary>
        public bool TruncateStream { get; set; } = true;

        /// <summary>
        /// Stores the amount of bytes that are still required to download
        /// </summary>
        public abstract long RemainingBytes { get; }

        /// <summary>
        /// Indicates if this instance owns the Stream instance and should dispose it too when this instance is disposed
        /// </summary>
        protected bool OwnsStream { get; set; }

        /// <summary>
        /// Defines if the active Download operation should be aborted. This value is actually reset when a new Download operation is initiated
        /// </summary>
        public bool IsAborted { get; set; }

        /// <summary>
        /// Returns the percent of completion for the download of this resource.
        /// This value is a product of the <see cref="RemainingBytes"/>. This will reflect overall content completion regardless if the object is downloading only a part of it
        /// </summary>
        /// <remarks>If <see cref="ContentLength"/> is unknown, the returned value will be Zero</remarks>
        public float PercentComplete
        {
            get
            {
                if (ContentLength < 0) return 0;

                float result = (DownloadedBytes*100.0f)/ContentLength;


                if (result < 0)
                    result = 0;
                if (result > 100.0f)
                    result = 100.0f;

                return result;
            }
        }

        /// <summary>
        /// Returns a value indicating if this instance is currently downloading from the host
        /// </summary>
        public bool IsDownloading { get; protected set; }

        /// <summary>
        /// Stores a value indicating the amount of bytes downloads in the last second (this value is not averaged)
        /// </summary>
        public float TransferRate { get; protected set; }
        /// <summary>
        /// Stores the name associated with this instance. This is purely cosmetic
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Indicates if <see cref="GetInfo"/> has been already called for this instance
        /// </summary>
        public bool InfoQueried { get; protected set; }


        // Constructor
        protected DownloadBase(string name)
        {
            Name = name;
        }


        // Delegates
        /// <summary>
        /// Defines a delegate that can subscribe to the <see cref="DataReceived"/> event.
        /// </summary>
        /// <param name="sender">Object that generated the event</param>
        /// <param name="buffer">RAW data received from host. This buffer is of a fixed size</param>
        /// <param name="receivedBytes">Amount of bytes received from the host, use this to locate the last data byte received on the buffer.</param>
        /// <param name="msSinceLastUpdate">Milliseconds that passed since the last time the class received data from the host</param>
        public delegate void DataReceivedDelegate(
            DownloadBase sender, byte[] buffer, long receivedBytes, long msSinceLastUpdate);

        /// <summary>
        /// Defines a delegate that can subscribe to the <see cref="ConnectionException"/> event.
        /// </summary>
        /// <param name="sender">Object that generated the event</param>
        /// <param name="ex">Exception that raised on the download process</param>
        public delegate void ConnectionExceptionDelegate(DownloadBase sender, Exception ex);

        /// <summary>
        /// Defines a delegate that can subscribe to the <see cref="DownloadComplete"/> event.
        /// </summary>
        /// <param name="sender">Object that generated the event</param>
        /// <param name="completed">True if the file has fully downloaded. If there are still remaining bytes to download, this will be False</param>
        public delegate void DownloadCompleteDelegate(DownloadBase sender, bool completed);


        // Events
        public virtual event DataReceivedDelegate DataReceived;
        public virtual event ConnectionExceptionDelegate ConnectionException;
        public virtual event DownloadCompleteDelegate DownloadComplete;


        // Event Invocator
        /// <summary>
        /// Invokes the <see cref="DataReceived"/> event with the provided arguments
        /// </summary>
        /// <param name="sender">Current state of this class</param>
        /// <param name="buffer">Buffer received from the host</param>
        /// <param name="receivedBytes">Amount of bytes received from the host and stored in buffer</param>
        /// <param name="msSinceLastUpdate">Milliseconds since the last time data was received from the host</param>
        protected void OnDataReceived(DownloadBase sender, byte[] buffer, long receivedBytes, long msSinceLastUpdate)
        {
            // Update Transfer rate
            TransferRate = (receivedBytes/msSinceLastUpdate)*1000;

            DataReceived?.Invoke(sender, buffer, receivedBytes, msSinceLastUpdate);
        }

        /// <summary>
        /// Invokes the <see cref="ConnectionException"/> event with the provided arguments
        /// </summary>
        /// <param name="sender">Object that generates the event</param>
        /// <param name="ex">Exception raised during the download process</param>
        protected void OnConnectionException(DownloadBase sender, Exception ex)
        {
            ConnectionException?.Invoke(sender, ex);
        }

        /// <summary>
        /// Invokes the <see cref="DownloadComplete"/> event with the provided arguments
        /// </summary>
        /// <param name="sender">Object that generates the event</param>
        /// <param name="completed">If the download has been fully completed</param>
        protected void OnDownloadComplete(DownloadBase sender, bool completed)
        {
            DownloadComplete?.Invoke(sender, completed);
        }


        // Methods
        /// <summary>
        /// Starts the download operation from the beginning.
        /// </summary>
        /// <returns>The amount of bytes downloaded to the output stream from the source host</returns>
        public long Start()

        {
            if (IsDownloading) throw new InvalidOperationException("This instances is already downloading from the host");

            if (OwnsStream)
                Stream.Seek(0, SeekOrigin.Begin);

            return StartDownload(null, null);
        }
        /// <summary>
        /// Starts the download operation from the beginning and downloads only the specified amount of bytes from the host
        /// </summary>
        /// <param name="size">Amount of bytes to download from the host</param>
        /// <returns>The amount of bytes actually downloaded from the host</returns>
        /// <remarks>Ensure that the value specified as the <paramref name="size"/> parameter is actually valid and smaller or equal to the content length.
        /// Specifying a value greater than the content size will lead to the library to try to download more that the content actually have, and eventually report an incomplete download</remarks>
        public long Start(long size)
        {
            if (IsDownloading) throw new InvalidOperationException("This instances is already downloading from the host");

            if (OwnsStream)
                Stream.Seek(0, SeekOrigin.Begin);

            return StartDownload(null, size);
        }

        /// <summary>
        /// Starts the download operation using the specified starting offset and length
        /// </summary>
        /// <param name="startOffset">Optional starting offset for the download operation. The content will be downloaded starting from the this offset in bytes.
        /// If null is specified, then the entire content will be downloaded</param>
        /// <param name="length">Amount of data to download from the host. Use null to download the entire content</param>
        /// <returns>the amount of bytes downloaded to the output stream from the source URL</returns>
        protected abstract long StartDownload(long? startOffset, long? length);

        /// <summary>
        /// Resumes a download operation from the specified offset.
        /// </summary>
        /// <param name="offset">Starting point from where the download operation will be resumed</param>
        /// <returns>The amount of bytes from the specified starting point, that where downloaded to the output stream from the source host</returns>
        public long Resume(long offset)
        {
            if (IsDownloading) throw new InvalidOperationException("This instances is already downloading from the host");

            if (OwnsStream)
                Stream.Seek(offset, SeekOrigin.Begin);

            return StartDownload(offset, null);
        }

        /// <summary>
        /// Resumes a download operation from the specified offset and downloads only the specified amount of bytes from the host
        /// </summary>
        /// <param name="offset">Starting point from where the download operation will be resumed</param>
        /// <param name="size">The amount of bytes actually downloaded from the host</param>
        /// <returns>Ensure that the value specified as the <paramref name="size"/> parameter is actually valid and its resulting sum to <paramref name="offset"/> is smaller or equal to the content length.
        /// Specifying a value that sums to a greater value than the content size will lead to the library to try to download more that the content actually have, and eventually report an incomplete download</returns>
        public long Resume(long offset, long size)
        {
            if (IsDownloading) throw new InvalidOperationException("This instances is already downloading from the host");

            if (OwnsStream)
                Stream.Seek(offset, SeekOrigin.Begin);

            return StartDownload(offset, size);
        }

        /// <summary>
        /// Gets content information from the source host, like Size, Type, etc.
        /// </summary>
        /// <returns>True if the operation succeeded, otherwise False</returns>
        public abstract bool GetInfo();


        // Methods (Async)
        /// <summary>
        /// Starts the download operation asynchronously from the beginning.
        /// </summary>
        public void StartAsync()
        {
            if (IsDownloading) throw new InvalidOperationException("This instances is already downloading from the host");

            if (BackgroundThread != null && BackgroundThread.IsAlive)
                BackgroundThread.Join(5000);

            BackgroundThread = new Thread(() => { StartDownload(null, null); });
            BackgroundThread.Name = "Background downloader";
            BackgroundThread.IsBackground = true;
            BackgroundThread.Priority = ThreadPriority.Lowest;

            BackgroundThread.Start();
        }

        /// <summary>
        /// Starts the download operation asynchronously from the beginning and downloads only the specified amount of bytes from the host
        /// </summary>
        /// <param name="size">Amount of bytes to download from the host</param>
        /// <remarks>Ensure that the value specified as the <paramref name="size"/> parameter is actually valid and smaller or equal to the content length.
        /// Specifying a value greater than the content size will lead to the library to try to download more that the content actually have, and eventually report an incomplete download</remarks>
        public void StartAsync(long size)
        {
            if (IsDownloading) throw new InvalidOperationException("This instances is already downloading from the host");

            if (BackgroundThread.IsAlive)
                BackgroundThread.Join(5000);

            BackgroundThread = new Thread(() => { StartDownload(null, size); });
            BackgroundThread.Name = "Background downloader";
            BackgroundThread.IsBackground = true;
            BackgroundThread.Priority = ThreadPriority.Lowest;

            BackgroundThread.Start();
        }
        /// <summary>
        /// Resumes a download operation asynchronously from the specified offset.
        /// </summary>
        /// <param name="offset">Starting point from where the download operation will be resumed</param>
        public void ResumeAsync(long offset)
        {
            if (IsDownloading) throw new InvalidOperationException("This instances is already downloading from the host");

            if (BackgroundThread.IsAlive)
                BackgroundThread.Join(5000);

            BackgroundThread = new Thread(() => { StartDownload(offset, null); });
            BackgroundThread.Name = "Background downloader";
            BackgroundThread.IsBackground = true;
            BackgroundThread.Priority = ThreadPriority.Lowest;

            BackgroundThread.Start();
        }

        /// <summary>
        /// Resumes a download operation asynchronously from the specified offset and downloads only the specified amount of bytes from the host
        /// </summary>
        /// <param name="offset">Starting point from where the download operation will be resumed</param>
        /// <param name="size">The amount of bytes actually downloaded from the host</param>
        public void ResumeAsync(long offset, long size)
        {
            if (IsDownloading) throw new InvalidOperationException("This instances is already downloading from the host");

            if (BackgroundThread.IsAlive)
                BackgroundThread.Join(5000);

            BackgroundThread = new Thread(() => { StartDownload(offset, size); });
            BackgroundThread.Name = "Background downloader";
            BackgroundThread.IsBackground = true;
            BackgroundThread.Priority = ThreadPriority.Lowest;

            BackgroundThread.Start();
        }

        public void Dispose()
        {
            if (TruncateStream)
                Stream.SetLength(Stream.Position);

            if (OwnsStream)
                Stream.Dispose();

            Stream = Stream.Null;
        }
    }
}