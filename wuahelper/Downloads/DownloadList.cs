﻿using System;
using System.Collections.Generic;


namespace com.razorsoftware.wuahelper.Downloads
{
    /// <summary>
    /// Manages multiple resource downloads using a download List that can perform multiple transfers in parallel
    /// </summary>
    public class DownloadList : IDisposable
    {
        // Fields
        /// <summary>
        /// Stores the list where all downloads are stored (pending and completed)
        /// </summary>
        protected List<DownloadBase> List = new List<DownloadBase>();
        /// <summary>
        /// Indicates if the download pointer should be used as a shortcut to select the next pending download when an slot becomes available, instead of looping through all indexes of the list for a pending download.
        /// This value is set to true initially and when a download has been added to the List that is before the current DownloadPointer.
        /// </summary>
        protected bool DownloadPointerInvalidated = true;

        /// <summary>
        /// Counter for the amount of currently active downloads
        /// </summary>
        protected int ActiveDownloadsCounter;
        
        
        // Properties
        /// <summary>
        /// Returns a value indicating if this object is currently downloading any resource
        /// </summary>
        public bool IsDownloading { get; protected set; }

        /// <summary>
        /// Returns an array of items that stores indexes for all currently active downloads
        /// </summary>
        public int[] ActiveDownloadIndexes
        {
            get
            {
                List<int> indices = new List<int>();

                for (int i = 0; i < List.Count; i++)
                {
                    if (List[i].IsDownloading)
                        indices.Add(i);
                }

                return indices.ToArray();
            }
        }
        /// <summary>
        /// Returns an array of objects that stores all currently active downloads
        /// </summary>
        public DownloadBase[] ActiveDownloads
        {
            get
            {
                List<DownloadBase> downloads = new List<DownloadBase>();

                for (int i = 0; i < List.Count; i++)
                {
                    if (List[i].IsDownloading)
                        downloads.Add(List[i]);
                }

                return downloads.ToArray();
            }
        }

        /// <summary>
        /// Defines the amount of Downloads that will be active in parallel
        /// </summary>
        public int ParallelDownloads { get; set; } = 3;

        /// <summary>
        /// Returns the Overall transfer rate, that is the sum of each individual transfer rate for all active downloads
        /// </summary>
        public float OverallTransferRate
        {
            get
            {
                DownloadBase[] downloads = ActiveDownloads;
                float transferRate = 0;
                foreach (DownloadBase d in downloads)
                    transferRate += d.TransferRate;

                return transferRate;
            }
        }

        /// <summary>
        /// Returns the percent of completion for all active downloads (sum)
        /// </summary>
        public float PercentComplete
        {
            get
            {
                DownloadBase[] downloads = ActiveDownloads;
                float percent = 0;
                foreach (DownloadBase d in downloads)
                    percent += d.PercentComplete;

                percent /= downloads.Length;

                return percent;
            }
        }
        /// <summary>
        /// Returns the amount of Downloads on the List (pending and completed)
        /// </summary>
        public int Count => List.Count; 
        /// <summary>
        /// Returns the download pointer, this is the location of the last started download
        /// </summary>
        public int DownloadPointer { get; protected set; }
        /// <summary>
        /// Indicates if the Download list is currently stopping its active downloads
        /// </summary>
        public bool IsStopping { get; protected set; }
        /// <summary>
        /// Stores the total amount of bytes that needs to be downloaded
        /// </summary>
        public long TotalByteCount { get; protected set; }
        /// <summary>
        /// Stores the total amount of bytes that were already downloaded
        /// </summary>
        public long DownloadedBytes { get; protected set; }


        // Delegates
        public delegate void DownloadStatusChangedDelegate(DownloadList sender, DownloadBase download);

        public delegate void DownloadExceptionDelegate(DownloadList sender, DownloadBase download, Exception ex);


        // Events
        /// <summary>
        /// Notifies event subscribers that an specified download has been started
        /// </summary>
        public event DownloadStatusChangedDelegate DownloadStarted;
        /// <summary>
        /// Notifies event subscribers that an specified download has been stopped (either is has finished or has been aborted)
        /// </summary>
        public event DownloadStatusChangedDelegate DownloadStopped;
        /// <summary>
        /// Notifies event subscribers that an specified download has updated its progress information
        /// </summary>
        public event DownloadStatusChangedDelegate DownloadProgressUpdated;
        /// <summary>
        /// Notifies event subscribers that an specified download has encountered an unexpected exception. This doesn't necessarily means the download has been canceled.
        /// Always check <see cref="DownloadBase.IsDownloading"/>.
        /// </summary>
        public event DownloadExceptionDelegate DownloadException;


        // Event invocator
        protected virtual void OnDownloadStarted(DownloadBase download)
        {
            DownloadStarted?.Invoke(this, download);
        }

        protected virtual void OnDownloadStopped(DownloadBase download)
        {
            DownloadStopped?.Invoke(this, download);
        }

        protected virtual void OnDownloadProgressUpdated(DownloadBase download)
        {
            DownloadProgressUpdated?.Invoke(this, download);
        }

        protected virtual void OnDownloadException(DownloadBase download, Exception ex)
        {
            DownloadException?.Invoke(this, download, ex);
        }


        // Methods
        /// <summary>
        /// Starts all downloads on the list
        /// </summary>
        /// <returns>The amount of downloads that are currently pending</returns>
        public int Start()
        {
            DownloadPointer = 0;
            DownloadPointerInvalidated = true;
            IsStopping = false;
            IsDownloading = true;

            long totalBytes = 0;
            long totalBytesDownloaded = 0;
            for (int i = 0; i < List.Count; i++)
            {
                totalBytes += List[i].ContentLength;
                totalBytesDownloaded += List[i].DownloadedBytes;
            }

            TotalByteCount = totalBytes;
            DownloadedBytes = totalBytesDownloaded;

            int pending = 0;
            for (int i = 0; i < List.Count; i++)
            {
                if (IsDownloadCandidate(i))
                    pending++;
            }

            StartNextDownloads();

            return pending;
        }

        /// <summary>
        /// Stops all downloads currently active. And optionally specifies if the active downloads should be aborted too.
        /// </summary>
        /// <param name="immediately">True to abort all currently active downloads, False to stop after the currently active downloads finishes</param>
        public void Stop(bool immediately)
        {
            IsStopping = true;

            if (!immediately)
                return;

            foreach (DownloadBase d in List)
            {
                if (d.IsDownloading)
                    d.IsAborted = true;
            }
        }

        /// <summary>
        /// Adds a new download at the end of the list
        /// </summary>
        /// <param name="download">Target download that will be added to the list</param>
        /// <returns>The index at which the download was added</returns>
        public int Add(DownloadBase download)
        {
            if (download == null)
                throw new ArgumentNullException(nameof(download), "Download task cannot be null");

            int index = List.Count;
            Insert(index, download);
            return index;
        }

        private void Download_DownloadComplete(DownloadBase sender, bool completed)
        {
            ActiveDownloadsCounter--;
            if (ActiveDownloadsCounter < 0)
                ActiveDownloadsCounter = 0;

            OnDownloadStopped(sender);

            StartNextDownloads();
        }

        private void Download_DataReceived(DownloadBase sender, byte[] buffer, long receivedBytes, long msSinceLastUpdate)
        {
            DownloadedBytes += receivedBytes;
            OnDownloadProgressUpdated(sender);
        }

        private void Download_ConnectionException(DownloadBase sender, Exception ex)
        {
            OnDownloadException(sender, ex);
        }

        /// <summary>
        /// Insert a new download on the list at the specified index
        /// </summary>
        /// <param name="atIndex">Index where to insert the new download. If this index is Zero, the item will be inserted at the beginning on the List,
        /// if this index is equal or greater than the current amount of items on the List, the download will be inserted at the end of the List</param>
        /// <param name="download">Target download that will be added to the List</param>
        /// <remarks>If the List is currently downloading, and the download is inserted before the Download Pointer, 
        /// the download will be started immediately after any currently active download finished</remarks>
        public void Insert(int atIndex, DownloadBase download)
        {
            if (download == null)
                throw new ArgumentNullException(nameof(download), "Download task cannot be null");

            if (atIndex < 0)
                throw new ArgumentException("The Index of the new download task cannot be smaller than zero.");

            download.ConnectionException += Download_ConnectionException;
            download.DataReceived += Download_DataReceived;
            download.DownloadComplete += Download_DownloadComplete;
            TotalByteCount += download.ContentLength;

            if (atIndex >= List.Count)
            {
                List.Add(download);
                return;
            }

            if (atIndex <= DownloadPointer)
            {
                DownloadPointer++;
                DownloadPointerInvalidated = true; // DownloadPointer no longer points to the last download started on the list
            }

            List.Insert(atIndex, download);
        }

        /// <summary>
        /// Removes a download from the list located at the specified index. And additionally, aborts the removed download
        /// </summary>
        /// <param name="index">Index of the download that will be removed from the List</param>
        /// <returns>The Download object that was actually removed. This download will be already aborted.</returns>
        /// <exception cref="IndexOutOfRangeException">Thrown when the specified index is out of bounds</exception>
        public DownloadBase RemoveAt(int index)
        {
            if (index < 0 || index >= List.Count)
                throw new IndexOutOfRangeException("The specified index cannot be found in the collection.");

            if (index == DownloadPointer)
                DownloadPointer = 0;
            else if (DownloadPointer < index)
                DownloadPointer--;

            DownloadBase removedDownload = List[index];

            removedDownload.ConnectionException += Download_ConnectionException;
            removedDownload.DataReceived += Download_DataReceived;
            removedDownload.DownloadComplete += Download_DownloadComplete;

            List.RemoveAt(index);

            long totalBytes = 0;
            long totalBytesDownloaded = 0;
            for (int i = 0; i < List.Count; i++)
            {
                totalBytes += List[i].ContentLength;
                totalBytesDownloaded += List[i].DownloadedBytes;
            }

            TotalByteCount = totalBytes;
            DownloadedBytes = totalBytesDownloaded;


            removedDownload.IsAborted = true;
            return removedDownload;
        }

        /// <summary>
        /// Stops all downloads, disposes them and also disposes this instance
        /// </summary>
        public void Dispose()
        {
            DownloadBase[] downloads = List.ToArray();
            List.Clear();
            IsStopping = true;
            TotalByteCount = 0;

            foreach (DownloadBase d in downloads)
            {
                if (d == null)
                    continue;

                d.ConnectionException -= Download_ConnectionException;
                d.DataReceived -= Download_DataReceived;
                d.DownloadComplete -= Download_DownloadComplete;
                d.IsAborted = true;

            }

            IsDownloading = false;
            DownloadPointer = -1;
        }

        /// <summary>
        /// Starts a new batch of downloads until the available slots become filled
        /// </summary>
        protected void StartNextDownloads()
        {
            if (!IsStopping)
                IsDownloading = true;
            else
            {
                if (ActiveDownloadsCounter > 0)
                    IsDownloading = true;
                else
                {
                    IsDownloading = false;
                    IsStopping = false;
                }

                return;
            }

            while (ActiveDownloadsCounter < ParallelDownloads)
            {
                if (DownloadPointerInvalidated)
                {
                    // Search the first pending download on the list
                    bool found = false;
                    for (int i = 0; i < List.Count; i++)
                    {
                        if (IsDownloadCandidate(i))
                        {
                            DownloadPointer = i;
                            DownloadPointerInvalidated = false;
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        // End of list
                        if (ActiveDownloadsCounter < 1)
                            IsDownloading = false;

                        return;
                    }
                }
                else
                    DownloadPointer++; // Advance pointer

                if (DownloadPointer >= List.Count)
                {
                    // End of list
                    if (ActiveDownloadsCounter < 1)
                        IsDownloading = false;

                    return;
                }

                // Get the next download on the list
                DownloadBase download = List[DownloadPointer];
                ActiveDownloadsCounter++; // Increment download counter
                download.StartAsync(); // Start the download
                OnDownloadStarted(download); // Notify subscribers
            }
        }

        protected bool IsDownloadCandidate(int index)
        {
            if (index < 0 || index >= List.Count)
                throw new ArgumentOutOfRangeException(nameof(index), "The specified index is outside of bounds.");

            if (!List[index].InfoQueried)
                List[index].GetInfo();

            return (List[index].RemainingBytes > 0 && List[index].Downloadable && !List[index].IsAborted);
        }

        /// <summary>
        /// Removes all download from the list (pending, and completed).
        /// </summary>
        public void Clear()
        {
            while (Count > 0)
                RemoveAt(0);
        }


        // Indexers
        /// <summary>
        /// Returns the download located at the specified index on the List
        /// </summary>
        /// <param name="index">Index of the downloads that will be returned</param>
        /// <returns>the download at the specified index on the List</returns>
        /// <exception cref="IndexOutOfRangeException">Thrown when the specified index is out of bounds</exception>
        public DownloadBase this[int index]
        {
            get
            {
                if (index < 0 || index >= List.Count)
                    throw new IndexOutOfRangeException("The specified index cannot be found in the collection.");

                return List[index];
            }
        }
    }
}
