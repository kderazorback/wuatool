﻿
namespace com.razorsoftware.wuahelper.Downloads
{
    /// <summary>
    /// Provides common arguments for events on the <see cref="HttpDownload"/> class.
    /// <para>This structure stores the status of a <see cref="HttpDownload"/> class at an exact moment in time</para>
    /// </summary>
    public struct HttpDownloadEventArgs
    {
        // Fields
        /// <summary>
        /// <see cref="HttpDownload"/> Instance that generated the event
        /// </summary>
        public readonly HttpDownload Instance;
        /// <summary>
        /// Amount of bytes downloaded at the exact moment when the event was generated
        /// </summary>
        public readonly long DownloadedBytes;
        /// <summary>
        /// Amount of bytes remaining at the exact moment when the event was generated
        /// </summary>
        public readonly long RemainingBytes;
        /// <summary>
        /// Length of the content at the exact moment when the event was generated
        /// </summary>
        public readonly long ContentLength;
        /// <summary>
        /// Indicates if an Abort was requested at the exact moment when the event was generated
        /// </summary>
        public readonly bool Aborted;
        /// <summary>
        /// Amount of retires left at the exact moment when the event was generated
        /// </summary>
        public readonly int TriesLeft;
        /// <summary>
        /// Percent of completion for the resource download.
        /// </summary>
        public readonly float PercentComplete;

        // Constructors
        /// <summary>
        /// Creates a new instance of this class with the provided values
        /// </summary>
        /// <param name="pInstance"><see cref="HttpDownload"/> Instance</param>
        /// <param name="pDownloaded">Amount of bytes downloaded at this exact moment</param>
        /// <param name="pRemaining">Amount of bytes remaining at this exact moment</param>
        /// <param name="pLength">Length of the resource</param>
        /// <param name="pAborted">If the operation is aborted right now</param>
        /// <param name="pTries">Tries remaining at this exact moment</param>
        /// <param name="pPercentComplete">Overall download progress in percentiles</param>
        public HttpDownloadEventArgs(HttpDownload pInstance, long pDownloaded, long pRemaining, long pLength,
            bool pAborted, int pTries, float pPercentComplete)
        {
            Instance = pInstance;
            DownloadedBytes = pDownloaded;
            RemainingBytes = pRemaining;
            ContentLength = pLength;
            Aborted = pAborted;
            TriesLeft = pTries;
            PercentComplete = pPercentComplete;
        }

        /// <summary>
        /// Creates a new instance of this class using the provided instance, and extracting all missing information from it
        /// </summary>
        /// <param name="pInstance"><see cref="HttpDownload"/> Instance</param>
        /// <param name="pTriesLeft">Tries remaining at this exact moment</param>
        public HttpDownloadEventArgs(HttpDownload pInstance, int pTriesLeft)
        {
            Instance = pInstance;
            DownloadedBytes = pInstance.DownloadedBytes;
            RemainingBytes = pInstance.RemainingBytes;
            ContentLength = pInstance.ContentLength;
            Aborted = pInstance.IsAborted;
            TriesLeft = pTriesLeft;
            PercentComplete = pInstance.PercentComplete;
        }
    }
}
