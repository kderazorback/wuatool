﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace com.razorsoftware.wuahelper.WuaBlocking
{
    public static class RegistryBlocking
    {
        private const string RegKey1 = "SOFTWARE\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU";
        private const string RegValue1 = "AUOptions";
        //private const string RegValue2 = "NoAutoUpdate";

        public static bool IsBlocked
        {
            get
            {
                try
                {
                    using (RegistryKey rootKey = Registry.LocalMachine.OpenSubKey(RegKey1))
                    {
                        object value = rootKey.GetValue(RegValue1, null);
                        if (value == null)
                            return false;

                        int ival = (int) value;

                        if (ival != 2)
                            return false;

                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static void Unblock()
        {
            using (RegistryKey rootHive = Registry.LocalMachine)
            {
                using (RegistryKey containerKey = rootHive.CreateSubKey(RegKey1, RegistryKeyPermissionCheck.Default, RegistryOptions.None))
                {
                    if (containerKey == null)
                        throw new NullReferenceException("The requested registry key cannot be opened.");

                    containerKey.DeleteValue(RegValue1, false);
                }
            }
        }

        public static void Block()
        {
            using (RegistryKey rootHive = Registry.LocalMachine)
            {
                using (RegistryKey containerKey = rootHive.CreateSubKey(RegKey1, RegistryKeyPermissionCheck.Default, RegistryOptions.None))
                {
                    if (containerKey == null)
                        throw new NullReferenceException("The requested registry key cannot be created.");

                    containerKey.SetValue(RegValue1, 2, RegistryValueKind.DWord); // AUOptions = 2 --> Notify of download and installation.
                }
            }
        }
    }
}
