﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using com.razorsoftware.wuahelper.GpoManipulation;
using Microsoft.Win32;
using Microsoft.Win32.SafeHandles;

namespace com.razorsoftware.wuahelper.WuaBlocking
{
    public class PolicyBlocking
    {
        private const string GpoPath1 = "HKLM\\Software\\Policies\\Microsoft\\Windows\\WindowsUpdate\\AU!NoAutoUpdate";

        private volatile Exception _innerThreadException = null;
        private volatile bool _innerThreadResult = false;

        public bool GetBlockStatus()
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    _innerThreadException = null;
                    _innerThreadResult = false;
                    _innerThreadResult = Inner_GetBlockStatus();
                }
                catch (Exception e)
                {
                    _innerThreadException = e;
                    _innerThreadResult = false;
                }
            });
            thread.SetApartmentState(ApartmentState.STA);

            thread.Start();
            thread.Join(10000);

            if (_innerThreadException != null)
                throw _innerThreadException;

            return _innerThreadResult;
        }

        public void Block()
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    _innerThreadException = null;
                     Inner_SetBlocking(1);
                }
                catch (Exception e)
                {
                    _innerThreadException = e;
                }
            });
            thread.SetApartmentState(ApartmentState.STA);

            thread.Start();
            thread.Join(10000);

            if (_innerThreadException != null)
                throw _innerThreadException;
        }

        public void Unblock()
        {
            Thread thread = new Thread(() =>
            {
                try
                {
                    _innerThreadException = null;
                    Inner_UnSetGpo();
                }
                catch (Exception e)
                {
                    _innerThreadException = e;
                }
            });
            thread.SetApartmentState(ApartmentState.STA);

            thread.Start();
            thread.Join(10000);

            if (_innerThreadException != null)
                throw _innerThreadException;
        }

        private bool Inner_GetBlockStatus()
        {
            GPClass c = new GPClass();
            IGroupPolicyObject gpo = c as IGroupPolicyObject;

            uint result = gpo.OpenLocalMachineGPO(GPO_OPEN.GPO_OPEN_LOAD_REGISTRY);

            if (result != 0) // S_OK
                throw new InvalidOperationException("The specified GPO object cannot be opened");

            if (gpo.GetRegistryKey(GPO_SECTION.GPO_SECTION_MACHINE, out IntPtr handle) != 0)
               throw new InvalidOperationException("Cannot retrieve target RegistryKey for the specified GPO.");

            SafeRegistryHandle safeHandle = new SafeRegistryHandle(handle, true);
            RegistryKey regKey = RegistryKey.FromHandle(safeHandle, RegistryView.Default);

            string valueName = new string(' ', 512);
            GPO_SECTION section = GPO_SECTION.GPO_SECTION_MACHINE;

            DumpGpoTree(".\\hklm_gpoTree.txt", regKey);

            string subkeyName = GpoUtils.ParseGpoKey(GpoPath1, out valueName, out section);
            RegistryKey subKey = regKey.OpenSubKey(subkeyName);

            if (subKey == null)
            {
                regKey.Close();
                return false;
            }

            object value = subKey.GetValue(valueName, null);

            subKey.Close();
            regKey.Close();

            if (value == null)
                return false;

            return (string.Equals(value.ToString(), "1", StringComparison.Ordinal));
        }

        private void Inner_SetBlocking(uint value)
        {
            GPClass c = new GPClass();
            IGroupPolicyObject gpo = c as IGroupPolicyObject;

            uint result = gpo.OpenLocalMachineGPO(GPO_OPEN.GPO_OPEN_LOAD_REGISTRY);

            if (result != 0) // S_OK
                throw new InvalidOperationException("The specified GPO object cannot be opened");

            if (gpo.GetRegistryKey(GPO_SECTION.GPO_SECTION_MACHINE, out IntPtr handle) != 0)
                throw new InvalidOperationException("Cannot retrieve target RegistryKey for the specified GPO.");

            SafeRegistryHandle safeHandle = new SafeRegistryHandle(handle, true);
            RegistryKey regKey = RegistryKey.FromHandle(safeHandle, RegistryView.Default);

            string valueName = new string(' ', 512);
            GPO_SECTION section = GPO_SECTION.GPO_SECTION_MACHINE;

            string subkeyName = GpoUtils.ParseGpoKey(GpoPath1, out valueName, out section);
            RegistryKey subKey = regKey.OpenSubKey(subkeyName, true);

            if (subKey == null)
                subKey = regKey.CreateSubKey(subkeyName, RegistryKeyPermissionCheck.Default, RegistryOptions.None);

            subKey.SetValue(valueName, value, RegistryValueKind.DWord);

            subKey.Close();
            regKey.Close();

            if (gpo.Save(true, // Save Machine policy
                    true, // ADD operation
                    GuidStore.PolSnapInGuid, // Internal Snap-In processing .pol files
                    GuidStore.LocalAppGuid) // Local App GUID
                != 0)
                throw new InvalidOperationException("Cannot save machine GPO store.");
        }

        private void Inner_UnSetGpo()
        {
            GPClass c = new GPClass();
            IGroupPolicyObject gpo = c as IGroupPolicyObject;

            uint result = gpo.OpenLocalMachineGPO(GPO_OPEN.GPO_OPEN_LOAD_REGISTRY);

            if (result != 0) // S_OK
                throw new InvalidOperationException("The specified GPO object cannot be opened");

            if (gpo.GetRegistryKey(GPO_SECTION.GPO_SECTION_MACHINE, out IntPtr handle) != 0)
                throw new InvalidOperationException("Cannot retrieve target RegistryKey for the specified GPO.");

            SafeRegistryHandle safeHandle = new SafeRegistryHandle(handle, true);
            RegistryKey regKey = RegistryKey.FromHandle(safeHandle, RegistryView.Default);

            string valueName = new string(' ', 512);
            GPO_SECTION section = GPO_SECTION.GPO_SECTION_MACHINE;

            string subkeyName = GpoUtils.ParseGpoKey(GpoPath1, out valueName, out section);

            RegistryKey subkey = regKey.OpenSubKey(subkeyName, true);

            if (subkey != null)
            {
                subkey.DeleteValue(valueName, false);
                subkey.Close();
            }

            regKey.Close();

            gpo.Save(true, // Save Machine policy
                true, // ADD operation
                GuidStore.PolSnapInGuid, // Internal Snap-In processing .pol files
                GuidStore.LocalAppGuid); // Local App GUID
        }

        private void DumpGpoTree(string filename, RegistryKey initialKey)
        {
            using (System.IO.FileStream fs = new System.IO.FileStream(filename, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None))
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fs))
                {
                    Stack<KeyValuePair<RegistryKey, int>> items = new Stack<KeyValuePair<RegistryKey, int>>(1024);
                    items.Push(new KeyValuePair<RegistryKey, int>(initialKey, 0));

                    while (items.Count > 0)
                    {
                        KeyValuePair<RegistryKey, int> pair = items.Pop();

                        string[] valueNames = pair.Key.GetValueNames();

                        foreach (string val in valueNames)
                        {
                            writer.Write(new string('\t', pair.Value));
                            writer.Write("- ");
                            writer.Write(val);
                            writer.Write(" ===> ");

                            try
                            {
                                writer.WriteLine(pair.Key.GetValue(val).ToString());
                            }
                            catch (Exception e)
                            {
                                writer.WriteLine("EXCEPTION: " + e.Message);
                            }
                        }

                        string[] subKeyNames = pair.Key.GetSubKeyNames();

                        if (subKeyNames != null && subKeyNames.Length > 0)
                        {
                            writer.Write(new string('\t', pair.Value));
                            writer.Write("+ ");
                            if (string.IsNullOrWhiteSpace(pair.Key.Name) && pair.Key == initialKey)
                                writer.WriteLine("ROOT");
                            else
                                writer.WriteLine(pair.Key.Name);

                            Stack<string> reverseStack = new Stack<string>(subKeyNames.Length);
                            foreach (string str in subKeyNames)
                                reverseStack.Push(str);

                            while (reverseStack.Count > 0)
                            {
                                try
                                {
                                    RegistryKey sk = pair.Key.OpenSubKey(reverseStack.Pop());

                                    if (sk != null)
                                        items.Push(new KeyValuePair<RegistryKey, int>(sk, pair.Value + 1));
                                }
                                catch (Exception)
                                {
                                    // Ignore
                                }
                            }
                        }

                        if (pair.Key != initialKey)
                            pair.Key.Close();
                    }
                }
            }
        }
    }
}
