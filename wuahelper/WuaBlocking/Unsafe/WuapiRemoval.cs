﻿using com.razorsoftware.wuahelper.Services;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuahelper.WuaBlocking.Unsafe
{
    /// <summary>
    /// Manages the complete removal of the Windows Update API on the system
    /// </summary>
    public class WuapiRemoval
    {
        public static string WuaLibraryName => "wuaueng.dll";
        public static string[] ApiDependencies = { "wuapi.dll", "wuapihost.exe", "wuauclt.exe", "wuaueng.dll" };
        public static string[] DependantServices = { "InstallService", "LicenseManager", "usosvc", "wuauserv", "sedsvc", "BITS" };

        public string PlaceholderPath = "wuaplaceholder.exe";

        private string RegistryKeyPath => "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
        private string RegistryKeyName => "WuaToolNotify";


        public static string System32Path
        {
            get
            {
                return Environment.ExpandEnvironmentVariables("%SystemRoot%\\System32");
            }
        }
        public static string WuapiBackupPath
        {
            get
            {
                DirectoryInfo di = new DirectoryInfo(Path.Combine(System32Path, "wuatool_wuapibackup"));
                if (!di.Exists)
                    di.Create();

                return di.FullName;
            }
        }

        public static bool IsApiPresent
        {
            get
            {
                FileInfo fi = new FileInfo(Path.Combine(System32Path, WuaLibraryName));
                return fi.Exists && fi.Length > 0;
            }
        }

        public bool RemoveApi()
        {
            bool output = false;
            using (FileStream fs = new FileStream(".\\wuapiremoval.log", FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                using (StreamWriter log = new StreamWriter(fs))
                {
                    log.WriteLine("Removing Windows Update API from the local system {0} at {1}...", Environment.MachineName, DateTime.Now.ToString("dd/mm/yyyy hh:mm:ss"));
                    log.WriteLine("Running as SID: {0} ({1})", WindowsIdentity.GetCurrent().User, WindowsIdentity.GetCurrent().Name);
                    try
                    {
                        __RemoveApi(log);
                        output = true;
                    }
                    catch (Exception ex)
                    {
                        log.WriteLine("An internal exception occurred. Cannot remove Windows Update API.");
                        while (ex != null)
                        {
                            log.WriteLine(ex.GetType().Name);
                            log.WriteLine(ex.Message);
                            ex = ex.InnerException;
                        }
                    }
                    log.WriteLine("Log closed.");
                }
            }

            return output;
        }

        private void __RemoveApi(StreamWriter log)
        {
            WindowsIdentity principal = WindowsIdentity.GetCurrent();
            string machineName = Environment.MachineName;

            log.WriteLine("Resolving user SID...");
            if (principal.Name.Length <= machineName.Length || !String.Equals(principal.Name.Substring(0, machineName.Length + 1), machineName + "\\", StringComparison.OrdinalIgnoreCase))
                throw new InvalidOperationException(string.Format("The current logged-in user is not a Local user. UserPrincipal: {0} MachineName: {1}", principal.Name, machineName));

            log.WriteLine("User is a valid local user for the machine {0}. ({1})", machineName, principal.Name);

            string[] accountSegments = principal.Name.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);

            NTAccount account;

            if (accountSegments.Length == 1)
                account = new NTAccount(accountSegments[0]);
            else
                account = new NTAccount(accountSegments[0], accountSegments[1]);

            log.WriteLine("Stopping {0} services that may be using the Windows Update API...");
            foreach (string svcName in DependantServices)
            {
                if (ServiceBase.Exists(svcName))
                {
                    ServiceBase svc = new CustomService(svcName);
                    if (svc.Status != System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        log.WriteLine("Service {0} found. Stoppin...", svcName);
                        svc.Stop();
                    }
                }
                else
                    log.WriteLine("Service {0} NOT found. Skipping...", svcName);
            }

            FileInfo fisrc = new FileInfo(Path.Combine(System32Path, WuaLibraryName));
            FileInfo fitarget = new FileInfo(Path.Combine(WuapiBackupPath, WuaLibraryName));
            if (!fisrc.Exists)
                throw new FileNotFoundException(string.Format("The main library file {0} cannot be located on the system. Expected path: {1}", WuaLibraryName, fisrc.FullName));
            if (fitarget.Exists)
            {
                log.WriteLine("Target backup file for {0} ({1}) already exists. Removing it...", WuaLibraryName, fitarget);
                fitarget.Delete();
            }

            log.WriteLine("Taking ownership of main library {0}...", WuaLibraryName);
            SetFileOwner(fisrc.FullName, account);
            MoveOutFile(log, fisrc, fitarget);

            log.WriteLine("Processing {0} WUA components...", ApiDependencies.Length);
            foreach (string f in ApiDependencies)
            {
                log.WriteLine("Taking ownership of dependency: {0} ...", f);
                fisrc = new FileInfo(Path.Combine(System32Path, f));

                if (!fisrc.Exists)
                {
                    log.WriteLine("Dependency file {0} does not exists. Skipping it...", fisrc.FullName);
                    continue;
                }
                fitarget = new FileInfo(Path.Combine(WuapiBackupPath, f));
                SetFileOwner(fisrc.FullName, account);
                MoveOutFile(log, fisrc, fitarget);
            }
        }

        private static void MoveOutFile(StreamWriter log, FileInfo source, FileInfo target)
        {
            log.WriteLine("Moving out main library...");
            log.WriteLine("\t\t{0} -> {1}", source.FullName, target.FullName);
            File.Move(source.FullName, target.FullName);
            source.Refresh();
            if (source.Exists)
                throw new IOException(String.Format("Cannot confirm file move operation for file {0}. The file still exists after executing the command.", source.FullName));

            log.WriteLine("Move operation confirmed for file {0}", source.Name);
        }

        private static void SetFileOwner(string file, NTAccount account)
        {
            System.Diagnostics.Process p = System.Diagnostics.Process.Start("takeown", string.Format("/f \"{0}\"", file.Replace("\"", "")));
            p.WaitForExit();

            if (p.ExitCode > 0)
                throw new UnauthorizedAccessException(String.Format("Cannot take ownership of file {0}. Shell error.", file));

            System.Security.AccessControl.FileSecurity acl = File.GetAccessControl(file);
            acl.SetAccessRuleProtection(false, false);
            acl.ResetAccessRule(new System.Security.AccessControl.FileSystemAccessRule(account, System.Security.AccessControl.FileSystemRights.FullControl, System.Security.AccessControl.AccessControlType.Allow));
            acl.SetAccessRule(new System.Security.AccessControl.FileSystemAccessRule(account, System.Security.AccessControl.FileSystemRights.TakeOwnership, System.Security.AccessControl.AccessControlType.Allow));
            File.SetAccessControl(file, acl);
        }

        public void InstallPlaceholder()
        {
            FileInfo fi = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), PlaceholderPath));
            if (!fi.Exists)
                throw new FileNotFoundException("Cannot find the required WUA placeholder app. " + fi.FullName);

            FileInfo targetPath = new FileInfo(Path.Combine(System32Path, "wuauclt.exe"));

            fi.CopyTo(targetPath.FullName);
            fi.CopyTo(Path.Combine(System32Path, WuaLibraryName));

            RegistryKey key = Registry.LocalMachine.OpenSubKey(RegistryKeyPath, true);
            key.SetValue(RegistryKeyName, targetPath.FullName);

            System.Diagnostics.Process.Start(targetPath.FullName);
        }
    }
}
