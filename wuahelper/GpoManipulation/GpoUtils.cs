﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuahelper.GpoManipulation
{
    internal static class GpoUtils
    {
        internal static string ParseGpoKey(string keyPath, out string value, out GPO_SECTION section)
        {
            string[] split = keyPath.Split('!');
            string key = split[0];
            string hive = key.Substring(0, key.IndexOf('\\'));
            key = key.Substring(key.IndexOf('\\') + 1);

            value = split[1];

            if (hive.Equals("HKLM", StringComparison.OrdinalIgnoreCase)
                || hive.Equals("HKEY_LOCAL_MACHINE", StringComparison.OrdinalIgnoreCase))
            {
                section = GPO_SECTION.GPO_SECTION_MACHINE;
            }
            else
            {
                section = GPO_SECTION.GPO_SECTION_USER;
            }

            return key;
        }
    }
}
