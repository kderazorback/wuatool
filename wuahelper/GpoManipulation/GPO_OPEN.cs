﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuahelper.GpoManipulation
{
    internal enum GPO_OPEN : uint
    {
        /// <summary>
        /// Load the registry files
        /// </summary>
        GPO_OPEN_LOAD_REGISTRY = 0x00000001,
        /// <summary>
        /// Open the GPO as read only
        /// </summary>
        GPO_OPEN_READ_ONLY = 0x00000002,
    }
}
