﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuahelper.GpoManipulation
{
    internal enum GPO_SECTION : uint
    {
        /// <summary>
        /// Root Section
        /// </summary>
        GPO_SECTION_ROOT = 0,
        /// <summary>
        /// User Section
        /// </summary>
        GPO_SECTION_USER = 1,
        /// <summary>
        /// Machine Section
        /// </summary>
        GPO_SECTION_MACHINE = 2,
    }
}
