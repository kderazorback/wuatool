﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuahelper.GpoManipulation
{
    internal static class GuidStore
    {
        public static Guid PolSnapInGuid
        {
            get
            {
                return new Guid(0x35378EAC, 0x683F, 0x11D2, 0xA8, 0x9A, 0x00, 0xC0, 0x4F, 0xBB, 0xCF, 0xA2);
            }
        }

        public static Guid LocalAppGuid
        {
            get
            {
                object[] attribs = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(GuidAttribute), true);

                if (attribs == null || attribs.Length < 1)
                    return Guid.NewGuid();

                GuidAttribute attrib = attribs[0] as GuidAttribute;

                if (attrib == null)
                    return Guid.NewGuid();

                return new Guid(attrib.Value);
            }
        }
    }
}
