﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuahelper.GpoManipulation
{
    [Flags]
    internal enum GPO_OPTION_DISABLE : uint
    {
        /// <summary>
        /// The user portion of this GPO is disabled
        /// </summary>
        GPO_OPTION_DISABLE_USER = 0x00000001,
        /// <summary>
        /// The machine portion of this GPO is disabled
        /// </summary>
        GPO_OPTION_DISABLE_MACHINE = 0x00000002,
    }
}
