﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace com.razorsoftware.wuahelper.GpoManipulation
{
    // COM Interop
    [ComImport, Guid("EA502723-A23D-11d1-A7D3-0000F87571E3"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface IGroupPolicyObject
    {
        // Constructor
        /// <summary>
        /// The New method creates a new GPO in the Active Directory with the specified display name. The method opens the GPO using the OpenDSGPO method.
        /// </summary>
        /// <param name="pszDomainName">Specifies the Active Directory path of the object to create. If the path specifies a domain controller, the GPO is created on that DC. Otherwise, the system will select a DC on the caller's behalf.</param>
        /// <param name="pszDisplayName">Specifies the display name of the object to create.</param>
        /// <param name="dwFlags">Specifies whether or not the registry information should be loaded for the GPO. This parameter can be one of the <see cref="GPO_OPEN"/> values.</param>
        /// <returns>If the method succeeds, the return value is S_OK. Otherwise, the method returns one of the COM error codes defined in the Platform SDK header file WinError.h.</returns>
        uint New([MarshalAs(UnmanagedType.LPWStr)] string pszDomainName, [MarshalAs(UnmanagedType.LPWStr)] string pszDisplayName, GPO_OPEN dwFlags);


        // Open methods
        /// <summary>
        /// The OpenDSGPO method opens the specified GPO and optionally loads the registry information.
        /// </summary>
        /// <param name="pszPath">Specifies the Active Directory path of the object to open. If the path specifies a domain controller, the GPO is created on that DC. Otherwise, the system will select a DC on the caller's behalf.</param>
        /// <param name="dwFlags">Specifies whether or not the registry information should be loaded for the GPO. This parameter can be one of the <see cref="GPO_OPEN"/> values.</param>
        /// <returns>If the method succeeds, the return value is S_OK. Otherwise, the method returns one of the COM error codes defined in the Platform SDK header file WinError.h.</returns>
        uint OpenDSGPO([MarshalAs(UnmanagedType.LPWStr)] string pszPath, GPO_OPEN dwFlags);
        /// <summary>
        /// The OpenLocalMachineGPO method opens the default GPO for the computer and optionally loads the registry information.
        /// </summary>
        /// <param name="dwFlags">Specifies whether or not the registry information should be loaded for the GPO. This parameter can be one of the <see cref="GPO_OPEN"/> values.</param>
        /// <returns>If the method succeeds, the return value is S_OK. Otherwise, the method returns one of the COM error codes defined in the Platform SDK header file WinError.h.</returns>
        uint OpenLocalMachineGPO(GPO_OPEN dwFlags);
        uint OpenRemoteMachineGPO([MarshalAs(UnmanagedType.LPWStr)] string computerName, uint flags); // UNUSED


        // Save methods
        /// <summary>
        /// The Save method saves the specified registry policy settings to disk and updates the revision number of the GPO.
        /// </summary>
        /// <param name="bMachine">Specifies the registry policy settings to be saved. If this parameter is TRUE, the computer policy settings are saved. Otherwise, the user policy settings are saved.</param>
        /// <param name="bAdd">Specifies whether this is an add or delete operation. If this parameter is FALSE, the last policy setting for the specified extension pGuidExtension is removed. In all other cases, this parameter is TRUE.</param>
        /// <param name="pGuidExtension">Specifies the GUID or unique name of the snap-in extension that will process policy. If the GPO is to be processed by the snap-in that processes .pol files, you must specify the REGISTRY_EXTENSION_GUID value.</param>
        /// <param name="pGuid">Specifies the GUID that identifies the MMC snap-in used to edit this policy. The snap-in can be a Microsoft snap-in or a third-party snap-in.</param>
        /// <returns>If the method succeeds, the return value is S_OK. Otherwise, the method returns one of the COM error codes defined in the Platform SDK header file WinError.h.</returns>
        uint Save([MarshalAs(UnmanagedType.Bool)] bool bMachine, [MarshalAs(UnmanagedType.Bool)] bool bAdd, [MarshalAs(UnmanagedType.LPStruct)] Guid pGuidExtension, [MarshalAs(UnmanagedType.LPStruct)] Guid pGuid);


        // Delete methods
        /// <summary>
        /// The Delete method deletes the GPO.
        /// </summary>
        /// <returns>If the function succeeds, the return value is S_OK. Otherwise, the function returns one of the COM error codes defined in the Platform SDK header file WinError.h.</returns>
        uint Delete();


        // Operational methods
        uint GetName([MarshalAs(UnmanagedType.LPWStr)] StringBuilder name, int maxLength); // UNUSED: For Active Directory policy objects, the method returns a GUID. For a local GPO, the method returns the string "Local". For remote objects, GetName returns the computer name.
        /// <summary>
        /// The GetDisplayName method retrieves the display name for the GPO.
        /// </summary>
        /// <param name="pszName">Pointer to a buffer that receives the display name.</param>
        /// <param name="cchMaxLength">Specifies the size, in characters, of the pszName buffer.</param>
        /// <returns>If the method succeeds, the return value is S_OK. Otherwise, the method returns one of the COM error codes defined in the Platform SDK header file WinError.h.</returns>
        uint GetDisplayName([MarshalAs(UnmanagedType.LPWStr)] string pszName, int cchMaxLength);
        uint SetDisplayName([MarshalAs(UnmanagedType.LPWStr)] string name); // UNUSED
        uint GetPath([MarshalAs(UnmanagedType.LPWStr)] StringBuilder path, int maxPath); // UNUSED
        uint GetDSPath(uint section, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder path, int maxPath); // UNUSED
        uint GetFileSysPath(uint section, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder path, int maxPath); // UNUSED
        /// <summary>
        /// The GetRegistryKey method retrieves a handle to the root of the registry key for the specified GPO section.
        /// </summary>
        /// <param name="dwSection">Specifies the GPO section. This parameter can be one of the values of <see cref="GPO_SECTION"/> enumeration.</param>
        /// <param name="hKey">Receives a handle to the registry key. This handle is opened with all access rights</param>
        /// <returns>If the method succeeds, the return value is S_OK. Otherwise, the method returns one of the COM error codes defined in the Platform SDK header file WinError.h. If the registry information is not loaded, the method returns E_FAIL.</returns>
        uint GetRegistryKey(GPO_SECTION dwSection, out IntPtr hKey);
        /// <summary>
        /// The GetOptions method retrieves the options for the GPO.
        /// </summary>
        /// <param name="dwOptions">Receives the options. This parameter can be one or more of the values defined on <see cref="GPO_OPTION_DISABLE"/>.</param>
        /// <returns>If the method succeeds, the return value is S_OK. Otherwise, the method returns one of the COM error codes defined in the Platform SDK header file WinError.h.</returns>
        uint GetOptions(out GPO_OPTION_DISABLE dwOptions);
        /// <summary>
        /// The SetOptions method sets the options for the GPO.
        /// </summary>
        /// <param name="dwOptions">Specifies the new option values.</param>
        /// <param name="dwMask">Specifies the options to change.</param>
        /// <returns>If the method succeeds, the return value is S_OK. Otherwise, the method returns one of the COM error codes defined in the Platform SDK header file WinError.h.</returns>
        uint SetOptions(GPO_OPTION_DISABLE dwOptions, GPO_OPTION_DISABLE dwMask);
        uint GetType(out IntPtr gpoType); // UNUSED
        uint GetMachineName([MarshalAs(UnmanagedType.LPWStr)] StringBuilder name, int maxLength); // UNUSED
        uint GetPropertySheetPages(out IntPtr pages); // UNUSED
    }
}
