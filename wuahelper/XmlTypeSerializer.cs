﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace com.razorsoftware.wuahelper
{
    /// <summary>
    /// Provides methods for Serializing or Deserializing objects from/to XML scripts
    /// </summary>
    public class XmlTypeSerializer : DynamicObject
    {
        // Instance fields
        protected XElement Element;
        protected XElement[] CachedChildElements;
        protected XAttribute[] BackendAttributes;
        protected TextReader Source;


        // Constructors
        /// <summary>
        /// Creates a new instance of this class by using the supplied XElement
        /// </summary>
        /// <param name="element">Element from where the new class will be created</param>
        protected XmlTypeSerializer(XElement element)
        { Element = element; }


        // Static methods
        /// <summary>
        /// Serializes an specified object of the given type to a Custom XML tree
        /// </summary>
        /// <param name="obj">Object that will be Serialized</param>
        /// <param name="t">Type of the object to Serialize</param>
        /// <param name="indentTabs">Amount of indentation to add to each Line</param>
        /// <returns>An string with the serialized XML object</returns>
        public static string SerializeObj(object obj, Type t, int indentTabs = 0)
        {
            if (obj == null) return "";

            StringBuilder output = new StringBuilder();

            // Check if Type is string or assignable from String
            if (typeof(String).IsAssignableFrom(t))
            {
                output.Append('\t', indentTabs);
                output.AppendLine(string.Format("<string value=\"{0}\"/>", SanitizeString(obj)));
                return output.ToString();
            }


            // Check if Type is a collection of or assignable from IEnumerable/ICollection<String>
            if (typeof(ICollection<string>).IsAssignableFrom(t) || typeof(IEnumerable<string>).IsAssignableFrom(t))
            {
                // String collection
                dynamic coll = obj as ICollection<string>;
                if (coll == null) coll = obj as IEnumerable<string>;

                if (coll != null)
                {
                    foreach (string val in coll)
                    {
                        output.Append('\t', indentTabs);
                        output.Append(SerializeObj(val, typeof(String), indentTabs + 1)); // There is a \n already at the end
                    }

                    return output.ToString();
                }
            }

            // Check if Type is a collection or enumeration
            if (typeof(ICollection).IsAssignableFrom(t) || typeof(IEnumerable).IsAssignableFrom(t))
            {
                dynamic coll = obj as ICollection;
                if (coll == null) coll = obj as IEnumerable;

                if (coll != null)
                {
                    // Find interface member subtype
                    Type[] interfaceTypes = t.GetInterfaces();
                    Type subType = typeof(object);

                    foreach (Type it in interfaceTypes)
                    {
                        PropertyInfo itprop = it.GetProperty("Item",
                            BindingFlags.IgnoreReturn | BindingFlags.Public | BindingFlags.Instance);

                        if (itprop != null)
                        {
                            subType = itprop.PropertyType;
                            break;
                        }
                    }

                    foreach (object val in coll)
                        output.Append(SerializeObj(val, subType, indentTabs));  // There is a \n already at the end

                    return output.ToString();
                }
            }

            // Check if Type is an Enumeration
            if (t.IsEnum)
            {
                Enum e = (Enum)obj;

                output.Append('\t', indentTabs);
                output.AppendLine(string.Format("<enum type=\"{0}\" value=\"{1}\"/>", t.Name, e));
                return output.ToString();
            }

            // Write Properties
            PropertyInfo[] properties = t.GetProperties();

            foreach (PropertyInfo p in properties)
            {
                StringBuilder str = new StringBuilder();

                try
                {
                    MemberInfo[] toStringMember = p.PropertyType.GetMember("ToString", BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreReturn);
                    bool stringType = typeof(String).IsAssignableFrom(p.PropertyType);

                    if (!stringType && (toStringMember == null || toStringMember.Length < 1))
                    {
                        // Object
                        str.Append('\t', indentTabs);
                        string content = SerializeObj(p.GetValue(obj), p.PropertyType, indentTabs + 1);
                        if (string.IsNullOrWhiteSpace(content))
                            str.AppendLine(string.Format("<member name=\"{0}\" type=\"{1}\"/>", p.Name,
                                p.PropertyType.Name));
                        else
                        {
                            str.AppendLine(string.Format("<member name=\"{0}\" type=\"{1}\">", p.Name,
                                p.PropertyType.Name));
                            str.Append(content);
                            str.Append('\t', indentTabs);
                            str.AppendLine("</member>");
                        }
                    }
                    else
                    {
                        // String parse-able object
                        str.Append('\t', indentTabs);
                        str.AppendLine(string.Format("<member name=\"{0}\" type=\"{1}\" value=\"{2}\"/>", p.Name, p.PropertyType.Name, SanitizeString(p.GetValue(obj))));
                    }
                }
                catch (Exception)
                {
                    str.Clear();
                    str.Append('\t', indentTabs);
                    str.AppendLine(string.Format("<!-- member name=\"{0}\" type=\"{1}\" cannot be retrieved. -->", p.Name, p.PropertyType.Name));
                }

                output.Append(str);
            }

            return output.ToString();
        }

        /// <summary>
        /// Deserializes the specified XML data into a dynamic object
        /// </summary>
        /// <param name="data">XML data to Deserialize</param>
        /// <returns>An instance of this class that can be used to dynamically access the deserialized data</returns>
        public static XmlTypeSerializer DeserializeObj(string data)
        {
            XmlTypeSerializer instance = new XmlTypeSerializer(null);

            TextReader reader = new StringReader(data);
            instance.Source = reader;

            instance.Element = XElement.Load(reader);

            return instance;
        }

        /// <summary>
        /// Removes invalid characters that tend to cause issues on the XML Parser, by escaping them
        /// </summary>
        /// <param name="str">Source string to sanitize or escape</param>
        /// <returns>An escaped copy of the source string that can be used safely by the XML Parser</returns>
        private static string SanitizeString(string str)
        {
            str = str.Replace("&", "&amp;");
            str = str.Replace("<", "&lt;");
            str = str.Replace(">", "&gt;");
            str = str.Replace("\"", "&quot;");
            str = str.Replace("'", "&apos;");

            return str;
        }

        /// <summary>
        /// Removes invalid characters that tend to cause issues on the XML Parser, by escaping them
        /// </summary>
        /// <param name="obj">Source object to sanitize or escape. The resulting string is obtained by calling object.ToString()</param>
        /// <returns>An escaped copy of the source string that can be used safely by the XML Parser</returns>
        private static string SanitizeString(object obj)
        {
            if (obj == null)
                return "";

            return SanitizeString(obj.ToString());
        }

        public static XmlTypeSerializer WrapAround(object obj, Type t)
        {
            return DeserializeObj("<root>" + Environment.NewLine + SerializeObj(obj, t, 1) + "</root>");
        }

        // Properties
        /// <summary>
        /// Stores the Dynamic body of the Deserialized XML Type
        /// </summary>
        public XElement Body
        {
            get { return Element; }
        }

        public XElement[] Childs
        {
            get
            {
                if (CachedChildElements == null)
                {
                    List<XElement> elements = new List<XElement>();

                    foreach (XElement item in Element.Elements())
                        elements.Add(item);

                    CachedChildElements = elements.ToArray();
                }

                return CachedChildElements;
            }
        }

        /// <summary>
        /// Returns a value indicating if the current node has Children
        /// </summary>
        public bool HasChilds
        {
            get { return Element.HasElements; }
        }

        /// <summary>
        /// Returns a value indicating if the current node has Attributes
        /// </summary>
        public bool HasAttributes
        {
            get { return Element.HasAttributes; }
        }


        // Methods
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (Element == null)
                goto ReturnFalse;

            XElement sub = Element.Element(binder.Name);

            if (sub == null)
            {
                foreach (XElement child in Childs)
                {
                    XAttribute attr = child.Attribute("name");
                    if (attr != null && string.Equals(attr.Value, binder.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        sub = child;
                        break;
                    }
                }

                if (sub == null)
                    goto ReturnFalse;
            }

            result = new XmlTypeSerializer(sub);
            return true;


            ReturnFalse: // Return false
            result = null;
            return false;
        }

        public override string ToString()
        {
            if (Element == null)
                return "";
            
            return Element.Value;
        }

        // Indexers
        public string this[string attr]
        {
            get
            {
                if (Element == null)
                    return null;

                XAttribute attrValue = Element.Attribute(attr);
                if (attrValue == null)
                    return null;
                
                return attrValue.Value;
            }
        }

        public string this[int index]
        {
            get
            {
                if (BackendAttributes == null)
                    BackendAttributes = Element.Attributes().ToArray();

                return BackendAttributes[index].Value;
            }
        }
    }
}
